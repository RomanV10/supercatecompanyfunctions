module.exports = {
entitiesStructure$v1:function () {
        return [
            {
                name: "arrivyClient",
                fields: {
                    arrivyId: FieldBuilder.makeStructure({array: false, type: "bigint"}),
                    clientId: FieldBuilder.makeStructure({array: false, type: "integer"}),
                }
            },
            {
                name: "arrivyRequest",
                fields: {
                    arrivyId: FieldBuilder.makeStructure({array: false, type: "bigint"}),
                    requestId: FieldBuilder.makeStructure({array: false, type: "integer"}),
					statusForArrivy: FieldBuilder.makeStructure({array: false, type: "integer"}),

				}
            }
        ]
    },
syncRequest$v1:function ({request, oldRequest = {},  arrivySettings = {}, arrivyRequestStatutes ={}, constants} = {}) {
	if (arrivySettings.enabled) {
		this.companyData.arrivyInstance.setConnectionData(arrivySettings.connectionData);

		if (request.status < constants.request.STATUS_NUMBER.PAID) {
			return this.deleteRequest(request.id);
		}

		let payments = _.get(request, 'payments.rows', []);
		if (request.status >= constants.request.STATUS_NUMBER.DELIVERY) {
			return this.sendClient(request.client, arrivySettings.notificationSettings)
				.then(res => {
					if (res.arrivyId) {
						let pickupRequest = _.clone(request);
						pickupRequest.arrivyStatus = constants.request.STATUS_NUMBER.PICKUP;

						let deliveryRequest = _.clone(request);
						deliveryRequest.arrivyStatus = constants.request.STATUS_NUMBER.DELIVERY;

						return this.sendRequests([pickupRequest, deliveryRequest], res.arrivyId, arrivySettings.notificationSettings, arrivyRequestStatutes, constants)
					}
					throw 'ARRIVY_ADD_REQUEST_NO_CLIENT';
				})
		}
		else {
			return Promise.resolve([]);
		}
	}

	return Promise.resolve([]);

},
sendRequests:function(requests = [], arrivyClientId, notificationSettings = {}, arrivyRequestStatutes, constants) {
	let requestsForLink = [];
	return this.companyData.entityInstance.arrivyRequest.getMultiply({filter:{requestId:_.map(requests, 'id')}})
		.then(requestsArrivy => {
			return Promise.all(requests.map(request => {
				let statusForArrivy = request.arrivyStatus || 0;

				let requestForArrivy = this.doTask('prepareRequest')(request, arrivyClientId, notificationSettings, arrivyRequestStatutes, constants);

				let requestArrivy = _.find(requestsArrivy.rows, {requestId:request.id, statusForArrivy});
				if (requestArrivy) {
					return this.companyData.arrivyInstance.updateTask(requestForArrivy, requestArrivy.arrivyId)
						.then(() => {
							requestsForLink.push(requestArrivy.arrivyId);
							return requestArrivy;
						})
				}
				else {
					return this.companyData.arrivyInstance.sendTask(requestForArrivy)
						.then(arrivyId => {
							if (arrivyId) {
								requestsForLink.push(arrivyId);
								return this.companyData.entityInstance.arrivyRequest.save({arrivyId, requestId:request.id, statusForArrivy})
							}
							throw 'ARRIVY_SAVE_REQUEST_NO_ARRIVY_ID';
						})
				}
			}));
		})
		.then(res => {
			if (requestsForLink.length > 1) {
				let parentArrivyId = requestsForLink[0];
				requestsForLink.shift();
				Promise.all(requestsForLink.map(id => {
					return this.companyData.arrivyInstance.createTasksLink(parentArrivyId, {sibling_task_id:id})
				}))
			}
			return res;
		})
},
deleteRequest:function(requestId) {
	return this.companyData.entityInstance.arrivyRequest.getMultiply({filter:{requestId:requestId}})
		.then(requestsArrivy => {
			if (requestsArrivy.count) {
				return Promise.all(requestsArrivy.rows.map(requestArrivy => {
					return this.companyData.arrivyInstance.deleteTask(requestArrivy.arrivyId)
						.then(() => {
							return this.companyData.entityInstance.arrivyRequest.remove(requestArrivy.id);
						})
				}))
			}

			return 'NO_REQUEST_ID';
		})
},
sendClient:function (client, notificationSettings = {}) {
        return this.companyData.entityInstance.arrivyClient.get({filter:{clientId:client.id}})
            .then(res => {
				let clientForArrivy = this.prepareClient(client, notificationSettings);
				return this.companyData.arrivyInstance.sendClient(clientForArrivy, res.arrivyId)
					.then(arrivyId => {
						if (arrivyId) {
							return this.companyData.entityInstance.arrivyClient.save({arrivyId, clientId:client.id}, res.id)
						}
						throw 'ARRIVY_SAVE_CLIENT_NO_ARRIVY_ID';
					})
            })
    },
prepareClient:function (client, notificationSettings) {
        let defaultNotifications = {
            'sms': false,
            'email': false
        };
        return {
            //'external_id': client.id,
            'first_name': client.firstName,
            'last_name': client.lastName,
            'mobile_number': client.phone,
            'email': client.email,
            'notifications': JSON.stringify(notificationSettings || defaultNotifications)
        };

    },
prepareRequest_crate$v1:function(request, arrivyClientId, notificationSettings, arrivyRequestStatutes, constants) {
	let startTime;
	let endTime;
	let title;

	let address = {
		customer_address_line_1: '',
		customer_address_line_2: '',
		customer_city: '',
		customer_state: '',
		customer_zipcode: '',
	};

	switch (request.arrivyStatus) {
		case constants.request.STATUS_NUMBER.PICKUP:
			title = `Request #${request.id} - PIC`;
			if (request.pickupTimeWindowStart === request.pickupTimeWindowEnd) request.pickupTimeWindowEnd++;

			startTime = moment.unix(request.pickupTimeWindowStart).tz(this.companyData.timeZone.name).format('YYYY-MM-DDTHH:mm:ssZZ');  //'2020-06-18T14:30:00-05:00',
			endTime = moment.unix(request.pickupTimeWindowEnd).tz(this.companyData.timeZone.name).format('YYYY-MM-DDTHH:mm:ssZZ');  //'2020-06-18T14:30:00-05:00',

			address = {
				customer_address_line_1: _.get(request, 'pickupAddress.address', ''),
				customer_address_line_2: _.get(request, 'pickupAddress.apt', ''),
				customer_city: _.get(request, 'pickupAddress.city', ''),
				customer_state: _.get(request, 'pickupAddress.state', ''),
				customer_zipcode: _.get(request, 'pickupAddress.postal', ''),
			};

			break;
		case constants.request.STATUS_NUMBER.DELIVERY:
		default:
			title = `Request #${request.id} - DEL`;
			address = {
				customer_address_line_1: _.get(request, 'deliveryAddress.address', ''),
				customer_address_line_2: _.get(request, 'deliveryAddress.apt', ''),
				customer_city: _.get(request, 'deliveryAddress.city', ''),
				customer_state: _.get(request, 'deliveryAddress.state', ''),
				customer_zipcode: _.get(request, 'deliveryAddress.postal', ''),
			};

			if (request.deliveryTimeWindowStart === request.deliveryTimeWindowEnd) request.deliveryTimeWindowEnd++;

			startTime = moment.unix(request.deliveryTimeWindowStart).tz(this.companyData.timeZone.name).format('YYYY-MM-DDTHH:mm:ssZZ');  //'2020-06-18T14:30:00-05:00',
			endTime = moment.unix(request.deliveryTimeWindowEnd).tz(this.companyData.timeZone.name).format('YYYY-MM-DDTHH:mm:ssZZ');  //'2020-06-18T14:30:00-05:00',
			break;
	}

	let result = {
		customer_address_line_1: _.get(request, 'deliveryAddress.address', ''),
		customer_address_line_2: _.get(request, 'deliveryAddress.apt', ''),
		customer_city: _.get(request, 'deliveryAddress.city', ''),
		customer_state: _.get(request, 'deliveryAddress.state', ''),
		customer_zipcode: _.get(request, 'deliveryAddress.postal', ''),

		customer_company_name: '',
		customer_country: 'USA',
		customer_email: request.client.email,
		customer_exact_location: '',
		customer_first_name: request.client.firstName,
		customer_id: arrivyClientId,
		customer_last_name: request.client.lastName,
		customer_notes: _.get(request, 'notes.client'),
		customer_phone: request.client.phone,

		enable_time_window_display: false,
		notifications: notificationSettings.notifications || {email: false, sms: false},

		start_datetime: startTime,
		start_datetime_timezone: this.companyData.timeZone.name,

		end_datetime: endTime,
		end_datetime_timezone: this.companyData.timeZone.name,

		time_window_start: 0,
		title: title,
		source: 'Dashboard',
		unscheduled: false,
		use_assignee_color: false,
		is_linked: true,
		task_without_time: false,
		template_type: 'TASK',
		manipulate_route: true,
		number_of_workers_required: 0,
		details: this.getDescription$v1(request)
	};

	if (arrivyRequestStatutes && arrivyRequestStatutes.length) {
		let arrivyStatus = _.find(arrivyRequestStatutes, {requestStatusId:+request.arrivyStatus});
		if (arrivyStatus) result.template = arrivyStatus.arrivyStatusId;
	}

	Object.assign(result, address);

	return result;

},
getDescription$v1:function (request) {
	return `
${request.client.firstName} ${request.client.lastName}
${request.client.phone || ''}
${request.client.email || ''}
${this.wrapLabel('Package:')} ${_.get(request, 'packages.mainPackage.name', '')} ${_.get(request, 'packages.mainPackage.size', '')} crates included. 
${_.get(request, 'packages.mainPackage.rollingCarts', '')} rolling carts.${_.get(request, 'packages.mainPackage.usagePeriod', '')} weeks of usage. \n
${this.getAdditionalPackages(request)}
${this.getExtraServices(request)}
${this.getTaxes(request)}   
${this.getLongLocation$v1(request)}
${this.wrapLabel('Receipt Details:')}
${this.wrapLabel('Order:')} #${request.id}
${this.wrapLabel('Package:')} ${_.get(request, 'packages.mainPackage.name', '')} ${_.get(request, 'packages.mainPackage.size', '')} crates: ${_.get(request, 'packages.mainPackage.total', '')}\$
${this.wrapLabel('Total:')} ${_.get(request, 'totalWithDiscount', 0)}\$
`
},
getAdditionalPackages:function (request) {
	let result = '';
	if (_.get(request, 'packages.additionalPackages')) {
		result += `${this.wrapLabel('Supllies:')} \n`;
		request.packages.additionalPackages.forEach(getAdditionalPackage => {
			result += `${getAdditionalPackage.name} - qt:${getAdditionalPackage.quantity} total:${getAdditionalPackage.total} \n`;
		});
	}

	return result;
},
getExtraServices:function (request) {
	let result = '';

	if (_.get(request, 'services.autoServices')) {
		let label = `${this.wrapLabel('Services:')} \n`;
		request.services.autoServices.forEach(service => {
			if (!service.disabled) result += `${service.name} ${service.total} \n`;
		});

		if (result !== '') result = label + result;
	}

	if (_.get(request, 'services.customServices')) {
		request.services.customServices.forEach(service => {
			result += `${service.name} ${service.total} \n`;
		});
	}

	return result;
},
getTaxes:function (request) {
	if (_.get(request, 'taxes.total')) return `${this.wrapLabel('Tax:')} ${request.taxes.total}$`;
	return '';

},
wrapLabel:function (label) {
	return `${label}`;
},
getLongLocation$v1:function (request) {
let movingFrom = `${this.wrapLabel('Drop off:')} 
${_.get(request, 'deliveryAddress.address', '')}, ${_.get(request, 'deliveryAddress.city', '')}, ${_.get(request, 'deliveryAddress.state', '')} 
${_.get(request, 'deliveryAddress.postal', '')}, ${this.wrapLabel('Apt:')} ${_.get(request, 'deliveryAddress.apt', '')}
${this.wrapLabel('Estimated Time:')}  ${this.getEstimatedTime$v1(request, 0)}

`;
let movingTo = `${this.wrapLabel('Pickup:')} 
${_.get(request, 'pickupAddress.address', '')}, ${_.get(request, 'pickupAddress.city', '')}, ${_.get(request, 'pickupAddress.state', '')} 
${_.get(request, 'pickupAddress.postal', '')}, ${this.wrapLabel('Apt:')} ${_.get(request, 'pickupAddress.apt', '')}
${this.wrapLabel('Estimated Time:')} ${this.getEstimatedTime$v1(request, 3)}
`
	return `${movingFrom}${movingTo}`;
},
getEstimatedTime$v1:function (request, status) {
	let startTime = 0;
	let endTime = 0;
	let time = '';

	switch (status) {
		case 0:
		case 1:
		case 2:
		default:
			startTime = request.deliveryTimeWindowStart;
			endTime = request.deliveryTimeWindowEnd;
			break;
		case 3:
		case 4:
			startTime = request.pickupTimeWindowStart;
			endTime = request.pickupTimeWindowEnd;
			break;
	}

	if (startTime && endTime) {
		time  = `${moment.unix(startTime).tz(this.companyData.timeZone.name).format('h:mm')} - ${moment.unix(endTime).tz(this.companyData.timeZone.name).format('h:mm')}`;
	}
	return time;
},
prepareRequest_moving$v1:function(request, arrivyClientId, notificationSettings, constants) {
	return {
		additional_addresses: [
			{
				"complete_address": "",
				"title": "To address",
				"address_line_1": _.get(request, 'pickupAddress.address', ''),
				"address_line_2":  _.get(request, 'pickupAddress.apt', ''),
				"city": _.get(request, 'deliveryAddress.city', ''),
				"state": _.get(request, 'deliveryAddress.state', ''),
				"country": "USA",
				"zipcode":  _.get(request, 'deliveryAddress.postal', ''),
				"exact_location": null
			}],
		customer_address_line_1: _.get(request, 'deliveryAddress.address', ''),
		customer_address_line_2: _.get(request, 'deliveryAddress.apt', ''),
		customer_city: _.get(request, 'deliveryAddress.city', ''),
		customer_state: _.get(request, 'deliveryAddress.state', ''),
		customer_zipcode: _.get(request, 'deliveryAddress.postal', ''),

		customer_company_name: '',
		customer_country: 'USA',
		customer_email: request.client.email,
		customer_exact_location: '',
		customer_first_name: request.client.firstName,
		customer_id: arrivyClientId,
		customer_last_name: request.client.lastName,
		customer_notes: _.get(request, 'notes.client'),
		customer_phone: request.client.phone,

		enable_time_window_display: false,
		notifications: notificationSettings.notifications || {email: false, sms: false},

		end_datetime: moment.unix(request.pickupTimeWindowEnd).tz(this.companyData.timeZone.name).format('YYYY-MM-DDTHH:mm:ssZZ'),
		end_datetime_timezone: this.companyData.timeZone.name,

		start_datetime: moment.unix(request.deliveryTimeWindowStart).tz(this.companyData.timeZone.name).format('YYYY-MM-DDTHH:mm:ssZZ'),  //'2020-06-18T14:30:00-05:00',
		start_datetime_timezone: this.companyData.timeZone.name,

		time_window_start: 0,
		title: `Request #${request.id}`,
		source: 'Dashboard',
		unscheduled: false,
		use_assignee_color: false,
		is_linked: false,
		task_without_time: false,
		template_type: 'TASK',
		manipulate_route: true,
		number_of_workers_required: 0,
	};
},
getAllTemplatesStatuses$v1:function(settings) {
	if (_.get(settings, 'arrivySettings.enabled')) {
		this.companyData.arrivyInstance.setConnectionData(settings.arrivySettings.connectionData);
		return this.companyData.arrivyInstance.getAllTemplates()
			.then(res => {
				return _.filter(res, {type: "TASK"});
			})
	}

	return [];
},

};