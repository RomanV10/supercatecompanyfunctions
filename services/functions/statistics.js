module.exports = {
entitiesStructure$v1:function () {
	return [
		{
			name: 'Visits',
			fields: {
				total: FieldBuilder.makeStructure({ array: false, type: 'integer', defaultValue:0 }),
				type: FieldBuilder.makeStructure({ array: false, type: 'integer'}),
			},
		},
	];
},
getVisits$v1:function (data) {
	return this.companyData.entityInstance.getEntityMultiply('Visits', data);
},
saveVisit$v1:function (data) {
	return this.companyData.entityInstance.save('Visits', { total: 1 });
},

};