module.exports = {
createRequest$v1:function (request) {
		if (!request.manager) request.manager = 1;
		if (!request.source) request.source = this.systemData.source;
		return this.doTask('saveRequest')(request);
	},
getRequestsAll$v1:function (request) {
		return Promise.resolve()
			.then(() => this.services.request('getRequestsAll', {}));
	},
getFrontComponents$v1:function (request) {
		return this.services.request('getFrontComponents', {});
	},
getUser$v1:function (request) {
		return this.services.request('getUser', request);
	},
createDraftRequest$v1:function (request) {
	let newClient;
		return this.createUser$v1({
			firstName: 'User',
			lastName: `Draftovi4${Date.now()}`,
			email: `draft@${Date.now()}.dr`,
			status: 1,
			role: [2],
		})
			.then(client => {
				newClient = client;
				return this.doTask('getDraftRequestFields')();
			})
			.then(defaultValues => {
				Object.assign(defaultValues, {client: newClient.id});

				return this.doTask('saveRequest')(defaultValues);
			});
	},
searchRequests$v1:function (conditions) {
		return this.services.request('searchRequests', conditions);
	},
getRequest$v1:function (request) {
		return this.services.request('getRequest', request)
			.then(this.doTask('loadRequestParts'));
	},
getVariable$v1:function (request) {
		// {name: string, version: number}
		return this.services.request('getVariable', request);
	},
saveVariable$v1:function (variableInfo) {
		return this.services.request('saveVariable', variableInfo);
	},
getManyVariables$v1:function (request) {
		return this.services.request('getManyVariables', request)
			.then(this.makeOldVariableFormat$v1);
	},
getCalculatorSettings$v1:function (request) {

		if (request && request.id) {
			return this.services.request('getRequestVariables', request);
		}

		return this.getManyVariables$v1([
		  {name: 'packagesInfo'}, {name: 'servicesInfo'}, {name: 'discountSettings'}, {name: 'zonesSettings'},
		]);

	},
calculateRequest$v1:function (request) {

	let calculatorSettings;
	let iterationCounter = 0;
	
	let calculateFields = (request, calculatorSettings, constantsSettings) => {
		iterationCounter += 1;
		let newRequest = _.cloneDeep(request);
		return this.doTask('sideCalculationsBeforeMain')({
					request: newRequest,
					calculatorSettings
				})
				.then(() => {
					return this.services.calculator('calculate', {
						request: newRequest,
						calculatorSettings,
						constantsSettings
					})
				})
				.then((calculatedRequest) => {
					Object.assign(newRequest, calculatedRequest);
					return this.doTask('sideCalculationsAfterMain')(						
						newRequest,
						request,
						calculatorSettings
					)
				})				
				.then(() => {
					//let diff = commonsLibs.objectsDiff(newRequest, request);
					//let needAnotherIteration = !!Object.keys(diff).length;
					let needAnotherIteration = !_.isEqual(newRequest, request);
					if (needAnotherIteration) {
						if (iterationCounter > 10) {
							throw new Error('Max calculator iteration count exceeded');
						}
						return calculateFields(newRequest, calculatorSettings, constantsSettings);
					}
					return newRequest;
				});
	};
	
	let constantsSettings = {};
	return this.doTask('getConstants')()
		.then(res => {
		   constantsSettings = res;
			return this.getCalculatorSettings$v1(request);
		})
		.then(res => {
		
			calculatorSettings = res;
			
			return calculateFields(request, calculatorSettings, constantsSettings);
			
		});
},
calculatePackages$v1:function (request) {
		return this.getCalculatorSettings$v1(request)
			.then(calculatorSettings => this.services.calculator('calculatePackages', {
				request,
				calculatorSettings
			}));
	},
calculateServices$v1:function (request) {
		return this.getCalculatorSettings$v1(request)
			.then(calculatorSettings => this.services.calculator('calculateServices', {
				request,
				calculatorSettings
			}));
	},
calculateCustomServices$v1:function (request) {
		return this.getCalculatorSettings$v1(request)
			.then(calculatorSettings => this.services.calculator('calculateCustomServices', {
				request,
				calculatorSettings
			}));
	},
autocompleteAddress$v1:function (geoRequest) {

		return this.getManyVariables$v1([{name: 'zonesSettings'}])
			.then((variables) => {
				let areas = variables.zonesSettings || [];
				return this.services.location('autocompleteAddress', {
					request: geoRequest,
					areas
				});
			});
	},
geocodeAddress$v1:function (request) {
		return this.services.location('geocodeAddress', request);
	},
saveEntity$v1:function (entityInfo) {
		return this.services.request('saveEntity', entityInfo);
	},
getTemplatesSettings$v1:function (request) {
		return this.services.emailer('getTemplatesSettings');
	},
saveEmailerTemplate$v1:function (templateData) {
		return this.services.emailer('saveEntity', {
			entityName: 'Template',
			entityData: templateData
		});
	},
removeEmailerFolder$v1:function (folderData) {
		return this.services.emailer('removeEntity', {
			entityName: 'TemplateFolder',
			entityData: folderData
		});
	},
removeEmailerTemplate$v1:function (templateData) {
		return this.services.emailer('removeEntity', {
			entityName: 'Template',
			entityData: templateData
		});
	},
parseTemplates$v1:function (data) {
	return this.getEmailParserData$v1(data)
		.then(async parserData => {
			return Promise.all(data.templates.map(async template => {
				let key = _.get(template, 'event.key');
				switch (key) {
					case 'monthStatistic':
						parserData.statistic = await this.getStatisticForPeriod('month');
						break;
					case 'yearStatistic':
						parserData.statistic = await this.getStatisticForPeriod('year');
						break;
				}
				return parserData
			}));
		})
		.then(parserData => {
			return this.services.emailer('parseTemplates', {
				templates: data.templates,
				parserData:parserData[0],
				source: data.source
			});
		})
	},
getDraftRequestFields$v1:function (request) {
		return {
			deliveryDate: moment.tz(this.companyData.timeZone.name).add(2, 'days').startOf('day').unix(),
			deliveryTime: moment.tz(this.companyData.timeZone.name).add(2, 'days').startOf('day').add(8, 'hours').unix(),
			deliveryTimeWindowStart: moment.tz(this.companyData.timeZone.name).add(2, 'days').startOf('day').add(8, 'hours').unix(),
			deliveryTimeWindowEnd: moment.tz(this.companyData.timeZone.name).add(2, 'days').startOf('day').add(12, 'hours').unix(),

			pickupDate: moment.tz(this.companyData.timeZone.name).add(6, 'days').startOf('day').unix(),
			pickupTime: moment.tz(this.companyData.timeZone.name).add(6, 'days').startOf('day').add(15, 'hours').unix(),
			pickupTimeWindowStart: moment.tz(this.companyData.timeZone.name).add(6, 'days').startOf('day').add(15, 'hours').unix(),

			pickupTimeWindowEnd: moment.tz(this.companyData.timeZone.name).add(6, 'days').startOf('day').add(18, 'hours').unix(),

			deliveryWorkTime: 15,
			pickupWorkTime: 15,
			deliveryCrewSize: 2,
			pickupCrewSize: 2,
			status: 0,
			type: 0,
			deliveryAddress: {},
			pickupAddress: {},
			packages: {},
			services: {},
			payments: {},
			discount: {},
			notes: {},
			isDraft: true,
			manager: 1,
			source: 'dashboard',
		};
	},
getEmailParserData$v1:function (emailData) {
		let promises = [];
		let gettingRequest;
		if (emailData.existingRequest) {
			gettingRequest = Promise.resolve(emailData.existingRequest);
		} else if (emailData.requestId) {
			gettingRequest = this.getRequest$v1({id: emailData.requestId});
		} else {
			let draftFields = this.getDraftRequestFields$v1();

			let promises = [];
			promises.push(this.calculateRequest$v1(draftFields));
			promises.push(this.getUser$v1({id: draftFields.client}));
			promises.push(this.getUser$v1({id: draftFields.client}));

			gettingRequest = Promise.all(promises)
				.then(resolves => {
					resolves[0].client = resolves[1];
					resolves[0].manager = resolves[2];
					return resolves[0];
				});
		}

		promises.push(gettingRequest);
		promises.push(this.getManyVariables$v1([
			{name: 'companyInfo'}, {name: 'companyLogoUrl'}, {name: 'loginCoverUrl'}
		]));

		promises.push(Promise.resolve(emailData.message || {}));
		promises.push(Promise.resolve(emailData.statistic || {}));

		return Promise.all(promises)
			.then(resolves => ({
					request: resolves[0],
					company: resolves[1],
					message: resolves[2],
					statistic: resolves[3],
				})
			);
	},
saveEmailerFolder$v1:function (folderData) {
		return this.services.emailer('saveEntity', {
			entityName: 'TemplateFolder',
			entityData: folderData
		});
	},
sendTemplates$v1:function ({templates, source, requestId, destination}) {
	let parserData;
	let emailDevSettings = {};
	return this.getEmailParserData$v1({requestId})
		.then(res => {
			parserData = res;
			return Promise.all(templates.map(async template => {
				let key = _.get(template, 'event.key');
				switch (key) {
					case 'monthStatistic':
						parserData.statistic = await this.getStatisticForPeriod('month');
						break;
					case 'yearStatistic':
						parserData.statistic = await this.getStatisticForPeriod('year');
						break;
				}
				return parserData
			}))
		})
		.then(res => {
			return this.getVariable$v1({name: 'emailDevSettings'});
		})
		.then(res => {
			emailDevSettings = _.get(res, 'value', {});
			return this.getVariable$v1({name: 'smtpSettings'});
		})
		.then((res = {}) => {
			let smtpSettings = _.get(res, 'value');
			if (smtpSettings) {
				return this.services.emailer('sendTemplates', {
					templates,
					parserData,
					source,
					destination,
					smtpSettings,
					emailDevSettings
				});
			} else {
				this.saveLog$v1({
					type: 'email',
					title: 'No email settings',
					body: {
						type: 'text',
						value: 'No email settings'
					},
					entityType: 'request',
					entityId: requestId,
				});

				throw 'No email settings';
			}
		})
		.then(res => {
			res.forEach(emailInfo => {
				emailInfo.mailerRes.map(emailRes => {
					if (emailRes) {
						this.saveLog$v1({
							type: 'email',
							title: `Email Was Sent to ${emailRes.accepted.join(
								', ')}. Subject:${emailInfo.template.subject}`,
							body: {
								type: 'html',
								value: emailInfo.template.body
							},
							entityType: 'request',
							entityId: requestId,
							mailId: emailRes.mailId
						});
					}

				});

			});

			return res;
		});
	},
calculatePaymentsWithDerivations$v1:function (request) {
		return this.getCalculatorSettings$v1(request)
			.then(calculatorSettings => this.services.calculator('calculatePaymentsWithDerivations', {
				request,
				calculatorSettings
			}));
	},
getEmptyRequest$v1:function (request) {
		let emptyFields = this.getDraftRequestFields$v1(request);
		for (let field in emptyFields) {
			emptyFields[field] = null;
		}
		return this.calculateRequest$v1(emptyFields);
	},
getOldMessages$v1:function (request) {
		return this.services.message('getMessages', request);
	},
addMessage$v1:function (request) {
		if (request.body && !_.isEmpty(request.body) && request.body != '') {
			return this.services.message('saveMessage', request)
				.then(res => {
					this.services.gate('socketSendToRequest', {
						requestId: request.requestId,
						event: 'addMessage',
						data: res
					});
					sendNotificationToManager.call(this, res.rows[0]);
					let getRequestPromise = this.getRequest$v1({id: request.requestId});

					if (commonsLibs.Permissions.isAdmin(this.systemData.authorizedUser)) {
						getRequestPromise
							.then(res => {
								this.triggerEmailerEvent$v1({
									request: res,
									eventName: 'managerAddMessage',
									message: request
								});
							});
					}
					else {
						getRequestPromise
							.then(res => {
								this.triggerEmailerEvent$v1({
									request: res,
									eventName: 'clientAddMessage',
									message: request
								});
							});
					}
					return res;
				});
		} else {
			return Promise.reject('Message cannot be empty');
		}

		function sendNotificationToManager(message) {
			this.getRequest$v1({id: message.requestId})
				.then(res => {

					if (res.manager.id !== message.authorId) {
						this.services.gate('socketSendToUser', {
							userId: res.manager.id,
							event: 'addMessageNotification',
							data: {
								title: 'new message',
								data: message,
							}
						});
					}

					this.saveLog$v1({
						type: 'message',
						title: 'Added message from ' + (res.manager.id === message.authorId
																  ? 'manager' : 'client'),
						body: {
							type: 'text',
							value: message.body
						},
						entityType: 'request',
						entityId: message.requestId,
					});

				});
		}
	},
getLogs$v1:function (request = {}) {
		request.pagination = {itemsOnPage: 99999};
		return this.services.log('getLogs', request);
	},
saveLog$v1:function (payload) {
		return this.services.log('saveLog', payload);
	},
saveRequest$v1:function(requestFront, additionalParams = {}) {

		let requestFromDB;
		let requestWithSafeUpdates;
		let calculatedRequest;
		let savedRequest;
		let isNewRequest = !requestFront.id;

		let checkRequestExist = isNewRequest										
										? this.services.request('saveRequest', {
												value: {client: requestFront.client}
											})
											.then(res => {requestFront.id = res.id;})
										: Promise.resolve();

		return checkRequestExist
			.then(() => {
					return this.getRequest$v1({id: requestFront.id});
				})
			.then(res => {
				requestFromDB = res;
				return this.doTask('filterRequestFieldsByPermission')(requestFront);
			})
			.then(safeUpdates => {
				requestWithSafeUpdates = Object.assign({}, requestFromDB, safeUpdates);
				return this.doTask('savingRequestSideEffectsBeforeCalc')(requestWithSafeUpdates);
			})
			.then(sideEffectsBeforeCalcRes => {
				return this.doTask('calculateRequest')(requestWithSafeUpdates);
			})
			.then(_calculatedRequest => {
				calculatedRequest = _calculatedRequest;
				return this.doTask('savingRequestSideEffectsAfterCalc')(calculatedRequest, requestFromDB);
			})
			.then(sideEffectsAfterCalcRes => {

				return this.services.request('saveRequest', {
					value: calculatedRequest
				});

			})
			.then(_savedRequest => {
				savedRequest = _savedRequest;
				Object.assign(calculatedRequest, savedRequest);
				let oldRequestValue = isNewRequest ? {} : requestFromDB;
				return this.doTask('savingRequestSideEffectsAfterSave')(calculatedRequest, oldRequestValue, additionalParams);
			})
			.then(sideEffectsAfterSaveRes => {
				return calculatedRequest;
			});
	},
updateParklotJobsForRequest:async function (request) {

	return this.services.parklot('updateJobsForRequest', _.clone(request))
		.then(parklotResponse => {
			if (parklotResponse.isChanged) {
				this.saveLog$v1({
					type: 'parklotChanged',
					title: 'Parklot Changed',
					entityType: 'request',
					entityId: request.id,
					body: {
						type: 'json',
						value: parklotResponse,
					},
				});
			}
			return parklotResponse;
		});
},
saveRequestEmail:function (savedRequest, oldRequest) {
		let promises = [];
		let CONST = {};
		return this.doTask('getConstants')(['request'])
		.then(res => {
			CONST = res;
			if (_.isEmpty(oldRequest)) {
				return this.triggerEmailerEvent$v1({
					request: savedRequest,
					eventName: 'requestCreated'
				});
			} else {
				if (oldRequest.status !== CONST.request.STATUS_NUMBER.DELIVERY
					&& savedRequest.status === CONST.request.STATUS_NUMBER.DELIVERY) {
					promises.push(this.triggerEmailerEvent$v1({
						request: savedRequest,
						eventName: 'requestDeliveryStatus'
					}));
				}

				if (oldRequest.deliveryDate !== savedRequest.deliveryDate) {
					promises.push(this.services.emailer('removeSendedEmailLog', {
						requestId: savedRequest.id,
						event: 'deliveryReminder5'
					}));
				}

				if (oldRequest.pickupDate !== savedRequest.pickupDate) {
					promises.push(this.services.emailer('removeSendedEmailLog', {
						requestId: savedRequest.id,
						event: 'pickupReminder1'
					}));
					promises.push(this.services.emailer('removeSendedEmailLog', {
						requestId: savedRequest.id,
						event: 'pickupReminder5'
					}));
				}
			}

			return Promise.all(promises);
		})

	},
saveRequestLog:function (_savedRequest, _oldRequest) {

		let savedRequest = _.cloneDeep(_savedRequest);
		let oldRequest = _.cloneDeep(_oldRequest);
		let CONST = {};
		return this.doTask('getConstants')(['request'])
			.then(res => {
				CONST = res;
				if (_.isEmpty(oldRequest)) {
					return this.saveLog$v1({
						type: 'requestCreated',
						title: 'Request Created',
						body: {
							type: 'json',
							value: savedRequest
						},
						entityType: 'request',
						entityId: savedRequest.id,
					});

				} else {
					let logType = 'requestChanged';
					let logName = 'Request was Changed';

					if (oldRequest.status !== CONST.request.STATUS_NUMBER.COMPLETE
						&& savedRequest.status === CONST.request.STATUS_NUMBER.COMPLETE) {
						logType = 'requestComplete';
						logName = 'Request Completed!';
					}

					delete savedRequest.savedVariables;
					delete oldRequest.savedVariables;
					delete savedRequest.payments.rows;
					delete oldRequest.payments.rows;

					return this.saveLog$v1({
						type: logType,
						title: logName,
						entityType: 'request',
						entityId: savedRequest.id,
						forDiff: {
							oldObj: oldRequest,
							newObj: savedRequest,
						}
					});
				}
			})

	},
triggerEmailerEvent$v1:function ({request, requestId, message, eventName, smtpSettings, tokenParams, statistic}) {
	let parserData;
	let emailParserParams = {message, statistic};
	if (request) emailParserParams.existingRequest = request;
	if (requestId) emailParserParams.requestId = requestId;
	let emailDevSettings = {};
	return this.getVariable$v1({name: 'emailDevSettings'})
		.then(res => {
			emailDevSettings = _.get(res, 'value', {});
			return this.getEmailParserData$v1(emailParserParams);
		})
		.then(async res => {
			parserData = res;
			if (_.isEmptyPhp(smtpSettings)) {
				let smtpSettingsVariable = await this.getVariable$v1({name: 'smtpSettings'});
				smtpSettings = _.get(smtpSettingsVariable, 'value');
			}

			return Promise.resolve(smtpSettings);
		})
		.then(smtpSettings => {
			if (smtpSettings) {
				return this.services.emailer('triggerEvent', {
					eventName,
					parserData,
					source: !_.isEmptyPhp(smtpSettings.from) ? smtpSettings.from : 'system',
					smtpSettings,
					tokenParams,
					emailDevSettings
				});
			} else {
				if(_.get(parserData, 'request.id')) {
					this.saveLog$v1({
						type: 'email',
						title: 'No email settings',
						body: {
							type: 'text',
							value: 'No email settings'
						},
						entityType: 'request',
						entityId: parserData.request.id,
					});
				}

				throw 'No email settings';
			}

		})
		.then(res => {
			if(_.get(parserData, 'request.id')) {
				res.forEach(emailInfo => {
					emailInfo.mailerRes.map(emailRes => {
						this.saveLog$v1({
							type: 'email',
							title: 'Email Was Sent to ' + emailRes.accepted.join(
								', ') + `. Subject:${emailInfo.template.subject}`,
							body: {
								type: 'html',
								value: emailInfo.template.body
							},
							entityType: 'request',
							entityId: parserData.request.id,
							mailId: emailRes.mailId
						});
					});
				});
			}

			return res;
		});
	},
filterRequest$v1:function (request, params) {
		if (request.payments) delete request.payments.rows;
		delete request.parklotJobs;
		return request;
	},
saveUser$v1:function ({user, sourceRequest, sourcePlace}) {
	let oldUser;
	let savedUser;
	return this.getUser$v1({id: user.id})
		.then(userFromDB => {
			oldUser = userFromDB;
			return this.services.request('saveUser', user);
		})
		.then(res => {
			savedUser = res;
			return this.doTask('searchRequests')({filter:{$op:{or:[{client: savedUser.id}, {manager: savedUser.id}]}}});
		})
		.then(requests => {
			Promise.all(requests.rows.map(request => {
				let isManager = _.get(request, 'manager.id') === savedUser.id;
				let isClient = _.get(request, 'client.id') === savedUser.id;

				this.saveRequestToSolr$v1(request);
				this.sendRequestToGoogleCalendar$v1(request);
				this.syncRequestToArrivy$v1(request);
				this.saveLog$v1({
					type: 'user',
					title: `${isClient ? 'Client' : isManager ? 'Manager': 'User'} was changed`,
					entityType: 'request',
					entityId: request.id,
					body: {
						type: 'text',
						value: `Changed from ${sourcePlace} in Request#${request.id}`
					},
					forDiff: {
						oldObj: oldUser,
						newObj: savedUser,
					}
				});

				return request;
			}))
				.catch(err => console.log(err));

			if (commonsLibs.Permissions.isAllowedAccessToDashboard(savedUser)) {
				this.shareGoogleCalendar$v1(savedUser, oldUser);
			}

			return savedUser;

		})
	},
sendDeliveryReminder5Days$v1:function () {
		let startTime = moment.tz(this.companyData.timeZone.name).add(4, 'd').unix();
		let endTime = moment.tz(this.companyData.timeZone.name).add(5, 'd').unix();

		let event = 'deliveryReminder5';
		let CONST = {};
		return this.doTask('getConstants')(['request'])
		.then(res => {
			CONST = res;
			return this.services.emailer('getSendedEmails', {event})

		})
		.then(res => {
				let filter = {
					deliveryDate: {
						op: 'BETWEEN',
						data: [startTime, endTime]
					},
					status: CONST.request.STATUS_NUMBER.DELIVERY
				};
				if (res && !_.isEmpty(res.rows)) {
					let ids = [];
					res.rows.map(row => {
						if (row.requestId) return ids.push(row.requestId);
					});
					ids = _.uniq(ids);
					filter.id = {
						op: 'NOT IN',
						data: ids
					};
				}
				return this.searchRequests$v1({filter});
			})
			.then(async res => {
				if (res && !_.isEmpty(res.rows)) {
					let smtpSettingsVariable = await this.getVariable$v1({name: 'smtpSettings'});
					let smtpSettings = _.get(smtpSettingsVariable, 'value');

					return Promise.all(res.rows.map(request => {
						return this.triggerEmailerEvent$v1({
							request,
							eventName: event,
							smtpSettings
						});
					}));
				} else {
					return Promise.resolve(`No request`);
				}
			});
	},
sendDeliveryReminder1Day$v1:function () {
		let startTime = moment.tz(this.companyData.timeZone.name).unix();
		let endTime = moment.tz(this.companyData.timeZone.name).add(1, 'd').unix();

		let event = 'deliveryReminder2';
		let CONST = {};
		return this.doTask('getConstants')(['request'])
			.then(res => {
				CONST = res;
				return this.services.emailer('getSendedEmails', {event})
			})
			.then(res => {
				let filter = {
					deliveryDate: {
						op: 'BETWEEN',
						data: [startTime, endTime]
					},
					status: CONST.request.STATUS_NUMBER.DELIVERY
				};
				if (res && !_.isEmpty(res.rows)) {
					let ids = [];
					res.rows.map(row => {
						if (row.requestId) return ids.push(row.requestId);
					});
					ids = _.uniq(ids);
					filter.id = {
						op: 'NOT IN',
						data: ids
					};
				}
				return this.searchRequests$v1({filter});
			})
			.then(async res => {
				if (res && !_.isEmpty(res.rows)) {
					let smtpSettingsVariable = await this.getVariable$v1({name: 'smtpSettings'});
					let smtpSettings = _.get(smtpSettingsVariable, 'value');

					return Promise.all(res.rows.map(request => {
						return this.triggerEmailerEvent$v1({
							request,
							eventName: event,
							smtpSettings
						});
					}));
				} else {
					return Promise.resolve(`No request`);
				}
			});
	},
reminderNotPaid$v1:function () {
		let startTime = moment.tz(this.companyData.timeZone.name).subtract(20, 'm').format('X');
		let endTime = moment.tz(this.companyData.timeZone.name).subtract(10, 'm').format('X');
		let requests = [];
		let event = 'requestNotPaid';
		return this.services.emailer('getSendedEmails', {event})
			.then(res => {
				let filter = {
					createdAt: {
						op: 'BETWEEN',
						data: [startTime, endTime]
					},
					status: {
						op: '>=',
						data: 0
					},
				};
				if (res && !_.isEmpty(res.rows)) {
					let ids = [];
					res.rows.map(row => {
						if (row.requestId) return ids.push(row.requestId);
					});
					ids = _.uniq(ids);
					filter.id = {
						op: 'NOT IN',
						data: ids
					};
					filter.source = 'front';
				}
				return this.searchRequests$v1({filter});
			})
			.then(res => {
				if (res && !_.isEmpty(res.rows)) {
					requests = res.rows;
					let requestsIds = _.map(res.rows, 'id');
					return this.services.payment('searchReceipts', {filter: {entityId: requestsIds}});
				} else {
					return Promise.resolve(false);
				}
			})
			.then(async res => {
				if (res && !_.isEmpty(res.rows)) {
					let requestsIds = res.rows.map(payment => {
						return {id: payment.entityId};
					});
					requests = _.pullAllBy(requests, requestsIds, 'id');

				}

				if (!_.isEmpty(requests)) {
					let smtpSettingsVariable = await this.getVariable$v1({name: 'smtpSettings'});
					let smtpSettings = _.get(smtpSettingsVariable, 'value');

					return Promise.all(requests.map(request => {
						return this.triggerEmailerEvent$v1({
							request,
							eventName: event,
							smtpSettings
						});
					}));
				} else {
					return Promise.resolve(`No not paid request for ${moment.tz(this.companyData.timeZone.name).format('YYYY-MM-DD')}`);
				}
			});
	},
saveRequestToSolr$v1:function (request) {
		return this.services.solr('save', {request: [request]});
	},
fullTextSearch$v1:function (request) {
		return this.services.solr('search', request);
	},
checkPromoCode$v1:function ({promoCode, requestId}) {
		return this.getCalculatorSettings$v1({id: requestId})
			.then(({discountSettings}) => {
				let foundPromo = _.findIndex(discountSettings.promoCodes,
					{name: promoCode});
				if (foundPromo !== -1) {
					return {promoConfirmed: true};
				} else {
					return {promoConfirmed: false};
				}
			});
	},
setMessageReaded$v1:function ({messageId}) {
		return this.services.message('addRead', {messageId})
			.then(message => {
				this.services.gate('socketSendToRequest', {
					requestId: message.requestId,
					event: 'messageReadedByThem',
					data: {message}
				});
				this.services.gate('socketSendToUser', {
					userId: this.systemData.authorizedUser.id,
					event: 'messageReadedByYou',
					data: {message}
				});
			});
	},
getUserRequestUnreadMessages$v1:function (requestId) {
		return this.services.message('getUserUnread', {requestId});
	},
getUserAllUnreadMessages$v1:function () {
		let data = {
			fields: ['id'],
			pagination: {itemsOnPage: 999999999999},
		};
		return this.services.request('searchRequests', data)
			.then(requests => {
				if (requests.count) {
					return this.services.message('getUserUnread', {requestId: requests.rows.map(request => request.id)});
				} else {
					return {count: 0, rows: []};
				}
			});
	},
getUserPublicInfo$v1:function (filters) {
		return this.services.request('getUserPublicInfo', filters);
	},
getRequestsWithUnreadMessages$v1:function (data = {}) {
	let messagesRes;
	data.fields = ['requestId'];
	return this.services.message('getUserAllUnreadGroupedByRequest', data)
		.then(res => {
			messagesRes = res;
			let promise;
			if (!_.isEmpty(res)) {
				let ids = _.keys(res);
				let order = {
					'id': {
						type: 'IN',
						data: ids
					}
				};
				promise = this.services.request('searchRequests', {
					filter: {id: ids},
					order,
					pagination: {itemsOnPage: 9999999999}
				});
			} else {
				promise = Promise.resolve({
					rows: [],
					count: 0
				});
			}

			return promise;
		})
		.then(res => {
			res.rows.forEach(request => {
				request.unreadCount = messagesRes[request.id].length;
			});
			return res;
		});
},
validationRules$v1:function () {
		return {
			updateRequest: {
				'client.email': `string|email`,
				'manager.email': `string|email`,
			},

		};
	},
getMessagesGroupedByRequest$v1:function (data = {}) {
		let gettingRequests;

		if (this.systemData.authorizedUser.id === 1) {
			gettingRequests = Promise.resolve({count: 9999999999});
		} else {
			gettingRequests = this.services.request('searchRequests', {
				fields: ['id'],
				pagination: {itemsOnPage: 9999999999}
			});
		}

		return gettingRequests
			.then(res => {
				if (res.count) {
					if (res.rows) _.set(data, 'filter.requestId', res.rows.map(row => row.id));
					data.fields = ['requestId'];

					return this.services.message('getMessagesGroupedByRequest', data);
				} else {
					return Promise.resolve({
						count: 0,
						rows: []
					});
				}

			})
			.then(res => {
				if (res.count) {
					let ids = res.requestsIds;
					let order = {
						'id': {
							type: 'IN',
							data: ids
						}
					};
					return this.services.request('searchRequests', {
						filter: {id: ids},
						order
					})
						.then(requests => {
							requests.rows.map(request => {
								//request.unreadCount = res.rows[request.id].unreadCount;
							});
							return requests;
						});
				} else {
					return Promise.resolve({
						count: res.count,
						rows: []
					});
				}
			});
	},
getPaymentTotalByDate$v1:function (data) {
		return this.services.payment('getPaymentTotalByDate', data);
	},
getVariablesAnonimus$v1:function () {
	let request = [{name: 'companyInfo'}, {name: 'servicesInfo'}, {name: 'packagesInfo'}];
	return this.getManyVariables$v1(request)
},
makeOldVariableFormat$v1:function (variables) {
	let vars = _.clone(variables);
	_.forEach(vars, (value, name) => vars[name] = value.value);
	return vars;
},
getAreaForAddress$v1:function ({address}) {

		return this.getCalculatorSettings$v1()
			.then((calculatorSettings) => this.services.location('getAreaForAddress', {
				address,
				calculatorSettings
			}));
	},
createUser$v1:function (userInfo) {
		return this.services.request('createUser', userInfo);
	},
createRequestFromFrontSite$v1:function (request) {
		request.manager = 1;
		return this.doTask('saveRequest')(request);
	},
cronTasksTestSmtpSettings$v1:function () {
		let testEmail = 'sergsamolilov@gmail.com';
		return this.getVariable$v1({name: 'smtpSettings'})
			.then(res => {
				let smtpSettings = _.get(res, 'value');
				return this.services.emailer('sendDirectEmail', {
					destination: testEmail,
					body: {
						body: 'testBody',
						subject: 'testSubject'
					},
					source: 'cronTest',
					smtpSettings
				});
			})
			.then((res) => {
				let accepted = _.get(res, '[0].accepted', []);
				let acceptedIndex = accepted.indexOf(testEmail);
				if (acceptedIndex === -1) throw `Mail was not deliver for ${testEmail}`;

				return true;
			})
			.catch(err => {
				throw err;
			});

	},
sendPickupReminder5days$v1:function () {
		let startTime = moment.tz(this.companyData.timeZone.name).add(4, 'd').unix();
		let endTime = moment.tz(this.companyData.timeZone.name).add(5, 'd').unix();

		let event = 'pickupReminder5';
		let CONST = {};
		return this.doTask('getConstants')(['request'])
			.then(res => {
				CONST = res;
				return this.services.emailer('getSendedEmails', {event})
			})
			.then(res => {
				let filter = {
					pickupDate: {
						op: 'BETWEEN',
						data: [startTime, endTime]
					},
					status: CONST.request.STATUS_NUMBER.PICKUP
				};
				if (res && !_.isEmpty(res.rows)) {
					let ids = [];
					res.rows.map(row => {
						if (row.requestId) return ids.push(row.requestId);
					});
					ids = _.uniq(ids);
					filter.id = {
						op: 'NOT IN',
						data: ids
					};
				}
				return this.searchRequests$v1({filter});
			})
			.then(res => {
				if (res && !_.isEmpty(res.rows)) {
					return Promise.all(res.rows.map(request => {
						return this.triggerEmailerEvent$v1({
							request,
							eventName: event
						});
					}));
				} else {
					return Promise.resolve(`No request`);
				}
			});
	},
sendPickupReminder1day$v1:function () {
		let startTime = moment.tz(this.companyData.timeZone.name).unix();
		let endTime = moment.tz(this.companyData.timeZone.name).add(1, 'd').unix();

		let event = 'pickupReminder1';
		let CONST = {};
		return this.doTask('getConstants')(['request'])
			.then(res => {
				CONST = res;
				return this.services.emailer('getSendedEmails', {event})
			})
			.then(res => {
				let filter = {
					pickupDate: {
						op: 'BETWEEN',
						data: [startTime, endTime]
					},
					status: CONST.request.STATUS_NUMBER.PICKUP
				};
				if (res && !_.isEmpty(res.rows)) {
					let ids = [];
					res.rows.map(row => {
						if (row.requestId) return ids.push(row.requestId);
					});
					ids = _.uniq(ids);
					filter.id = {
						op: 'NOT IN',
						data: ids
					};
				}
				return this.searchRequests$v1({filter});
			})
			.then(res => {
				if (res && !_.isEmpty(res.rows)) {
					return Promise.all(res.rows.map(request => {
						return this.triggerEmailerEvent$v1({
							request,
							eventName: event
						});
					}));
				} else {
					return Promise.resolve(`No request`);
				}
			});
	},
emailWasRead$v1:function (data) {
		let mailId = +data.mailId;

		let mail;
		return this.services.emailer('getEmail', {id: mailId})
			.then(res => {
				if (!res.readed) {
					mail = res;
					this.services.log('getLog', {mailId})
						.then(log => {
							log.type = 'emailRead';
							log.title = 'Email Was Read by ' + mail.email;
							this.saveLog$v1(log);
						});

					return this.services.emailer('wasRead', mailId);
				}

				return false;
			});


	},
saveUserDirect$v1:function (user) {
		return this.services.request('saveUser', user);
	},
changeClientInRequest$v1:function ({requestId, clientId}) {
		return this.services.request('saveRequest', {
			value: {
				id: requestId,
				client: clientId
			}
		});
	},
transferAllRequestsToAnotherClient$v1:function ({clientIdFrom, clientIdTo}) {
		let conditions = {
			filter: {client: clientIdFrom},
			order: {id: 'DESC'},
			pagination: {
				pageNumber: 1,
				itemsOnPage: 999999
			}
		};
		return this.services.request('searchRequests', conditions)
			.then(res => {
				return Promise.all(
					res.rows.map(request =>
						this.changeClientInRequest$v1({
							requestId: request.id,
							clientId: clientIdTo
						}))
				);
			});
	},
cloneRequest$v1:function (id) {
		return this.services.request('cloneRequest', id)
			.then(request => {
				return this.calculateRequest$v1(request);
			})
			.then(request => {
				return this.services.request('saveRequest', {
					value: request
				});
			})
			.then(request => {
			   this.sendRequestToGoogleCalendar$v1(request);
				this.saveRequestToSolr$v1(request);
				this.saveLog$v1({
					type: 'requestCreated',
					title: 'Request Created',
					body: {
						type: 'json',
						value: request
					},
					entityType: 'request',
					entityId: request.id,
				});

				return request.id;
			})
	},
getParklotSettings$v1:function (request) {
		return this.services.parklot('getSettings');
	},
saveTruck$v1:function (data) {
		return this.services.parklot('saveTruck', data);
	},
removeTruck$v1:function (data) {
		return this.services.parklot('removeTruck', data);
	},
calculateParklotJob$v1:function (data) {
	return this.services.parklot('calculateParklotJob', data);
},
searchParklotJobsForRequests$v1:function (data) {
		return this.services.parklot('searchParklotJobsForRequests', data);
	},
getRequestsForParklot$v1:function (data) {
		return this.services.request('getRequestsNoPermissions', data);
	},
getUsersForDepartment$v1:function () {
		return this.services.request('getUsersForDepartment');
	},
getUsersRoles$v1:function () {
		return this.services.request('constantine')
			.then(res => {
				return res.USER_ROLES;
			});
	},
searchRequestForStatistics$v1:function (data) {
		let result = {};
		return this.services.request('searchRequests', data)
			.then(res => {
				result.requests = res.rows;
				return this.services.payment('getRequestReceipts', _.uniq(_.map(res.rows, 'id')));
			})
			.then(res => {
				result.invoices = res.rows;
				return result;
			});
	},
searchInvoicesForStatistics$v1:function (data) {
		let result = {};
		return this.services.payment('searchReceipts', data);
	},
geocodeAddressBatch$v1:function (requests) {
		return this.services.location('geocodeAddressBatch', requests);
	},
saveVisitToFrontsite$v1:function (data) {
		return this.services.statistics('saveVisit', data);
	},
getVisitsToFrontsite$v1:function (data) {
		return this.services.statistics('getVisits', data);
	},
sendRequestToGoogleCalendar$v1:function (newRequest, oldRequest = {}) {
	return this.getManyVariables$v1([{name: 'companyInfo'}, {name: 'googleCalendarSettings'}])
		.then(res => {
			return this.services.googleCalendar('sendRequestToGoogleCalendar', {
				newRequest,
				oldRequest,
				settings:res
			})
			.then(res => {
				this.saveLog$v1({
					type: 'addRequestGoogleCalendar',
					title: `Added request to google calendar`,
					body: {
						type: 'json',
						value: _.map(res, 'data')
					},
					entityType: 'request',
					entityId: newRequest.id,
				});
			})
				.catch(err => {
					this.saveLog$v1({
						type: 'addRequestGoogleCalendar',
						title: 'Error Added request to google calendar',
						body: {
							type: 'json',
							value: err
						},
						entityType: 'request',
						entityId: newRequest.id,
					});
				})
		})

	},
enableGoogleCalendars$v1:function (types) {
		return this.services.googleCalendar('enableCalendars', types);
	},
getEnabledGoogleCalendars$v1:function () {
		return this.services.googleCalendar('getEnabledCalendars');

	},
shareGoogleCalendar$v1:function (newUser = {}, oldUser = {}) {
		let diff = commonsLibs.objectsDiff(oldUser, newUser);
		let forDelete = [];
		let forAdd = [];
		if (newUser.googleCalendarStatuses && diff && (diff.googleCalendarStatuses || diff.googleEmail)) {

			if (diff.googleCalendarStatuses) {
				forDelete = _.difference(oldUser.googleCalendarStatuses, newUser.googleCalendarStatuses);
				forAdd = _.difference(newUser.googleCalendarStatuses, oldUser.googleCalendarStatuses);
			}

			let data = {
				userId: newUser.id,
				googleEmail: newUser.googleEmail,
				calendarTypes: forAdd
			};

			let deletePromise = Promise.resolve();
			if (forDelete.length) {
				deletePromise = this.services.googleCalendar('deleteCalendarSharingRules', {
					userId: newUser.id,
					calendarTypes: forDelete
				});
			}

			return deletePromise
				.then(() => {
					return this.services.googleCalendar('shareCalendars', data);
				});

		}
	},
syncGoogleEvents$v1:function (calendarId) {
		return this.services.googleCalendar('getGoogleUpdatedEvents', calendarId)
			.then(res => {
				return Promise.all(res.map(async request => {
					let update = false;
					let oldRequest = await this.getRequest$v1({id: request.id});
					_.forEach(request, (value, fieldName) => {
						if (oldRequest[fieldName] !== value) {
							oldRequest[fieldName] = value;
							update = true;
						}
					});

					if (update) {
						return this.doTask('saveRequest')(oldRequest);
					} else {
						return Promise.resolve(oldRequest);
					}
				}));

			});
	},
syncGoogleEvents$v2:function (calendarId) {
		return true;
	},
createUserFromFrontSite$v1:function (client) {
		return this.getUserPublicInfo$v1({email: client.email})
			.then(user => {
				if (!_.isEmptyPhp(user) && user.id) return user;
				client.roles = [2];
				client.status = 1;
				return this.createUser$v1(client);
			});
	},
restorePassword$v1:function (email) {
		let user = {};
		let emailDevSettings = {};
		return this.getVariable$v1({name: 'smtpSettings'})
			.then(res => {
				emailDevSettings = _.get(res, 'value', {});
				return this.getUserPublicInfo$v1({email});
			})
			.then(res => {
				if (!res.id) throw new Error('USER_DOES_NOT_EXIST');
				user = res;
				return this.services.request('refreshUserPassword', user);
			})
			.then(password => {
				user.newPassword = password;
				return this.getVariable$v1({name: 'smtpSettings'});
			})
			.then((res = {}) => {
				let smtpSettings = _.get(res, 'value');
				if (smtpSettings) {
					let company = {};
					_.set(company, 'companyInfo.companyEmail', user.email);
					return this.services.emailer('triggerEvent', {
						eventName: 'restorePassword',
						parserData: {
							request: {client: user},
							company
						},
						source: !_.isEmptyPhp(smtpSettings.from) ? smtpSettings.from : 'system',
						smtpSettings,
						emailDevSettings
					})
						.then(res => {
							return true;
						});
				} else {
					throw 'SMTP_SETTINGS_EMPTY';
				}
			});
	},
sendInvoiceToClient:function (data) {
		return this.triggerEmailerEvent$v1({
			requestId: data.requestId,
			eventName: 'invoiceForClient'
		});
	},
getLoginSettings$v1:function () {
		return this.getManyVariables$v1([{name: 'loginCoverUrl'}, {name: 'companyLogoUrl'}])
	},
cronTasksSetDeliveryRequestToPickup$v1:function () {
	let CONTS = {};
	return this.doTask('getConstants')(['request'])
		.then(res => {
			CONTS = res;
			let startTime = moment.tz(this.companyData.timeZone.name).subtract(1, 'days').unix();
			return this.searchRequests$v1({
				filter: {
					deliveryDate: {
						op: '<',
						data: startTime
					},
					status: CONTS.request.STATUS_NUMBER.DELIVERY
				}
			})
		})
		.then(requests => {
			return Promise.all(requests.rows.map(request => {
				request.status = CONTS.request.STATUS_NUMBER.PICKUP;
				return this.doTask('saveRequest')(request);
			}));
		});
	},
cronTasksSetPickupRequestToComplete$v1:function () {
		let startTime = moment.tz(this.companyData.timeZone.name).subtract(1, 'days').unix();
	let CONST = {};
	return this.doTask('getConstants')(['request'])
		.then(res => {
			CONST = res;
			return this.searchRequests$v1({
				filter: {
					pickupDate: {
						op: '<',
						data: startTime
					},
					status: CONST.request.STATUS_NUMBER.PICKUP
				}
			})
		})
		.then(requests => {
			return Promise.all(requests.rows.map(request => {
				request.status = CONST.request.STATUS_NUMBER.COMPLETE;
				return this.doTask('saveRequest')(request);
			}));
		});
	},
saveRequestNotes$v1:function (data) {

	let oldRequest = {};
	
	return this.getRequest$v1({id: data.id || 0})
	.then(_oldRequest => {
		oldRequest = _oldRequest;
		return this.services.request('saveRequest', {value:{id: data.id, notes: data.notes}});
	})
	.then(savedRequest => {
		setTimeout(() => {
			this.saveRequestEmail(savedRequest, oldRequest);
			this.saveRequestToSolr$v1(savedRequest);
			this.saveRequestLog(
				{id: savedRequest.id, notes: savedRequest.notes},
				{id: oldRequest.id, notes: oldRequest.notes});
			if (!additionalParams.fromGoogle) this.sendRequestToGoogleCalendar$v1(savedRequest, oldRequest);
		}, 0);

		return savedRequest;
	});
},
sendReceiptEmail$v1:function ({requestId, receiptId}) {
	return this.getRequest$v1({id:requestId})
		.then(request => {
			return this.triggerEmailerEvent$v1({
						request: request,
						eventName: 'receiptCreated',
						tokenParams:{'request.payments':{id:receiptId}}
					});
		})

},
clearSolrIndex$v1:function () {
		return this.services.solr('deleteAll');
},
reindexSolrRequests$v1:function (requestsIds) {
	return this.searchRequests$v1({filter:{id:requestsIds}})
	.then(res => {
		  return this.services.solr('save', {request: res.rows});
	}) 

},
getTimeZone$v1:function () {
	return Promise.resolve(this.companyData.timeZone);
},
indexPage:function () {
		let data = [{name: 'companyInfo'}, {name:'companyFaviconUrl'}];

	return this.getManyVariables$v1(data)
	.then(res => {
		return {companyName:_.get(res, 'companyInfo.companyName', null), companyFaviconUrl:_.get(res, 'companyFaviconUrl.url', null)};
	})
},
getAllInventory$v1:function() {
	return this.services.inventory("getInventory", {});
},
saveInventoryRoom$v1:function(room) {
	return this.services.inventory("saveRoom", room)
	.then(inventorySettingsChanges => {
		this.services.gate('socketSendToInventory', {
			event: 'inventorySettingsChanged',
			data: inventorySettingsChanges
		});
		return true;
	});
},
saveInventoryCategory$v1:function(category) {
	return this.services.inventory("saveCategory", category)
	.then(inventorySettingsChanges => {
		this.services.gate('socketSendToInventory', {
			event: 'inventorySettingsChanged',
			data: inventorySettingsChanges
		});
		return true;
	});
},
saveInventoryItem$v1:function(item) {
	return this.services.inventory("saveItem", item)
	.then(inventorySettingsChanges => {
		this.services.gate('socketSendToInventory', {
			event: 'inventorySettingsChanged',
			data: inventorySettingsChanges
		});
		return true;
	});
},
removeInventoryRoom$v1:function(roomId) {
	return this.services.inventory("removeRoom", roomId)
	.then(inventorySettingsChanges => {
		this.services.gate('socketSendToInventory', {
			event: 'inventorySettingsChanged',
			data: inventorySettingsChanges
		});
		return true;
	});
},
removeInventoryCategory$v1:function(categoryId) {
	return this.services.inventory("removeCategory", categoryId)
	.then(inventorySettingsChanges => {
		this.services.gate('socketSendToInventory', {
			event: 'inventorySettingsChanged',
			data: inventorySettingsChanges
		});
		return true;
	});
},
removeInventoryItem$v1:function(itemId) {
	return this.services.inventory("removeItem", itemId)
	.then(inventorySettingsChanges => {
		this.services.gate('socketSendToInventory', {
			event: 'inventorySettingsChanged',
			data: inventorySettingsChanges
		});
		return true;
	});
},
calculateInventoryRoom$v1:function(room) {
	return this.services.inventory("calculateRoom", room);
},
getInventoryRooms$v1:function() {
	return this.services.inventory("getInventoryRooms", {});
},
calculateInventoryWeightFromVolume$v1:function(data) {
	return this.services.inventory('calculateInventoryWeightFromVolume', data);
},
saveRequestInventoryRoom$v1:function(room) {
	let operationInfo = {
		updateFieldResponce: null,
		updatedRequestFields: null,
		
		status: 200,
		updateFieldError: null,
		updateRequestError: null,
	};
	
	return this.services.inventory("saveRequestInventoryRoom", room)
		.catch(err => {
			err.updateFieldError = true;
			err.notCatch = true;
			throw err;
		})
		.then((res) => {
			operationInfo.updateFieldResponce = res;
			return this.doTask('saveRequest')({id: room.requestId}, {updatedFields: ['inventory']});
		})
		.catch(err => {
			if (err.notCatch) throw err;			
			err.updateRequestError = true;
			err.notCatch = true;
		})
	
		.then((updatedRequest) => {
			operationInfo.updatedRequestFields = {
				inventory: updatedRequest.inventory,
			};
			
			return operationInfo;
		})
		.catch(err => {
			if (err.updateFieldError) {
				operationInfo.updateFieldError = err;
			} else {
				operationInfo.updateRequestError = err;
			}
			operationInfo.status = 500;
			return operationInfo;
		});
},
removeRequestInventoryRoom$v1:function(roomId) {
	let operationInfo = {
		updateFieldResponce: null,
		updatedRequestFields: null,
		
		status: 200,
		updateFieldError: null,
		updateRequestError: null,
	};

	return this.services.inventory("removeRequestInventoryRoom", roomId)
		.catch(err => {
			err.updateFieldError = true;
			err.notCatch = true;
			throw err;
		})
		.then((res) => {
			operationInfo.updateFieldResponce = res;
			this.doTask('saveRequest')({id: room.requestId}, {updatedFields: ['inventory']});
		})
		.catch(err => {
			if (err.notCatch) throw err;			
			err.updateRequestError = true;
			err.notCatch = true;
		})
	
		.then((updatedRequest) => {
			operationInfo.updatedRequestFields = {
				inventory: updatedRequest.inventory,
			};
			
			return operationInfo;
		})
		.catch(err => {
			if (err.updateFieldError) {
				operationInfo.updateFieldError = err;
			} else {
				operationInfo.updateRequestError = err;
			}
			operationInfo.status = 500;
			return operationInfo;
		});
},
getInventoryForRequest$v1:function(requestId) {
	return this.services.inventory('getInventoryForRequest', requestId);
},
filterRequestFieldsByPermission$moving_v1:function(requestFront) {
	return requestFront;
},
loadRequestParts$moving_v1:function (request = {}) {
		if (!request.id) {
			return Promise.resolve(request);
		}
		let promises = [];
		promises.push(this.services.payment('getRequestReceipts', request.id));
		promises.push(this.services.parklot('getRequestJobs', request));
		promises.push(this.getRequestInventoryField$moving_v1(request.id));

		return Promise.all(promises)
			.then(([paymentsField, parklotField, inventoryField]) => {
				request.payments = paymentsField;
				request.parklot = parklotField;
				request.inventory = inventoryField;
				return request;
			});
	},
loadRequestParts$v1:function (request = {}) {
		if (!request.id) {
			return Promise.resolve(request);
		}
		let promises = [];
		promises.push(this.services.payment('getRequestReceipts', request.id));
		promises.push(this.services.parklot('getRequestJobs', request));

		return Promise.all(promises)
			.then(([paymentsField, parklotField]) => {
				request.payments = paymentsField;
				request.parklot = parklotField;
				return request;
			});
	},
getDraftRequestFields$moving_v1:function (request) {
	return this.doTask('getConstants')(['request'])
		.then(CONST => ({
			deliveryDate: moment.tz(this.companyData.timeZone.name).add(2, 'days').startOf('day').unix(),
			deliveryTimeWindowStart: moment.tz(this.companyData.timeZone.name).add(2, 'days').startOf('day').add(8, 'hours').unix(),
			deliveryTimeWindowEnd: moment.tz(this.companyData.timeZone.name).add(2, 'days').startOf('day').add(12, 'hours').unix(),

			pickupDate: moment.tz(this.companyData.timeZone.name).add(6, 'days').startOf('day').unix(),
			pickupTimeWindowStart: moment.tz(this.companyData.timeZone.name).add(6, 'days').startOf('day').add(15, 'hours').unix(),
			pickupTimeWindowEnd: moment.tz(this.companyData.timeZone.name).add(6, 'days').startOf('day').add(18, 'hours').unix(),

			deliveryWorkTime: 60,
			pickupWorkTime: 60,
			deliveryCrewSize: 2,
			crewSize: 2,
			status: 0,
			type: 0,
			deliveryAddress: {},
			pickupAddress: {},
			packages: {},
			services: {},
			payments: {},
			discount: {},
			notes: {},
			isDraft: true,
			client: 1,
			manager: 1,
			source: 'dashboard',
			serviceType: CONST.request.REQUEST_TYPES.localMove
		}));
},
filterRequestFieldsByPermission$v1:function(requestFront) {
	return requestFront;
},
savingRequestSideEffectsBeforeCalc$v1:function(requestWithSafeUpdates) {
	return Promise.all([
		
	]);
},
savingRequestSideEffectsBeforeCalc$moving_v1:function(requestWithSafeUpdates) {
	return Promise.all([
		
	]);
},
savingRequestSideEffectsAfterCalc$v1:async function(calculatedRequest, requestFromDB) {

  let promises = [];

	let isJustBooked = !calculatedRequest.bookedDate && _.get(calculatedRequest, 'payments.rows', []).length === 1 && calculatedRequest.balance === 0;
	if (isJustBooked) calculatedRequest.bookedDate = moment().unix();

	if (!requestFromDB.truckWasAutoassigned) {
	 let enableAutoAssignTruck = await this.doTask('getVariable')({name: 'enableAutoAssignTruck'});
	 enableAutoAssignTruck = _.get(enableAutoAssignTruck, 'value.value');
	 
    let CONST = await this.doTask('getConstants')(['request']);
    let statusForAutoAssign = [
      CONST.request.STATUS_NUMBER.PAID,
      CONST.request.STATUS_NUMBER.DELIVERY,
      CONST.request.STATUS_NUMBER.PICKUP,
      CONST.request.STATUS_NUMBER.COMPLETE,
    ];

    let needAutoAssignTruck = statusForAutoAssign.indexOf(requestFromDB.status) === -1 &&
      statusForAutoAssign.indexOf(calculatedRequest.status) !== -1;

    if (needAutoAssignTruck && enableAutoAssignTruck) {
      promises.push(
        this.services.parklot('autoAssignTrucks', calculatedRequest)
          .then(res => {
            calculatedRequest.parklot = res.parklot;
            calculatedRequest.deliveryTime = res.deliveryTime;
            calculatedRequest.pickupTime = res.pickupTime;
            calculatedRequest.truckWasAutoassigned = 1;
          })
      );
    }
  }

	return Promise.all(promises);
},
savingRequestSideEffectsAfterCalc$moving_v1:function(calculatedRequest, requestFromDB) {
	return Promise.all([
			
	]);
},
savingRequestSideEffectsAfterSave$v1:function(savedRequest, oldRequest, additionalParams = {}) {

	let result = Promise.all([
		this.updateParklotJobsForRequest(savedRequest),
	]);
	
	result.then(([updatingParklotRes]) => {
		
		setTimeout(() => {
			this.doTask('saveRequestEmail')(savedRequest, oldRequest);
			this.saveRequestToSolr$v1(savedRequest);
			this.saveRequestLog(savedRequest, oldRequest);
			this.syncRequestToArrivy$v1(savedRequest, oldRequest);
			if (!additionalParams.fromGoogle) this.sendRequestToGoogleCalendar$v1(savedRequest, oldRequest);
			
			
			let changedFields = {};
			Object.keys(savedRequest).forEach(requestField => {
				let isFieldIgnored = requestField === 'savedVariables';
				let isFieldChanged = !isFieldIgnored && (
					(additionalParams.updatedFields || []).indexOf(requestField) !== -1
					|| !_.isEqual(savedRequest[requestField], oldRequest[requestField])
				);
				if (isFieldChanged) {
					changedFields[requestField] = savedRequest[requestField];
				}
			});
			
			if (Object.keys(changedFields).length) {
				this.services.gate('socketSendToRequest', {
					requestId: savedRequest.id,
					event: 'requestFieldsUpdated',
					data: Object.assign({}, {id: savedRequest.id}, changedFields),
				});
			}

			if (updatingParklotRes.isChanged && changedFields.parklot) {
				this.services.gate('socketSendToParklot', {
					event: 'parklotChanged',
					data: {
						newValue: changedFields.parklot,
						oldValue: oldRequest.parklot,
					},
				});
			}
			
			if (  changedFields.client ||
            changedFields.packages ||
            changedFields.pickupAddress ||
            changedFields.pickupCrewSize ||
            changedFields.deliveryAddress ||
            changedFields.deliveryCrewSize ||
            changedFields.crewSize ||
            changedFields.pickupDate ||
            changedFields.deliveryDate ||
            changedFields.deliveryTimeWindowStart ||
            changedFields.deliveryTimeWindowEnd ||
            changedFields.pickupTimeWindowStart ||
            changedFields.pickupTimeWindowEnd ||
            changedFields.location ||
            changedFields.notes) {
				this.services.gate('socketSendToParklot', {
					event: 'parklotRequestChanged',
					data: {
						id: savedRequest.id,
            client: changedFields.client,
            packages: changedFields.packages,
            pickupAddress: changedFields.pickupAddress,
            pickupCrewSize: changedFields.pickupCrewSize,
            deliveryAddress: changedFields.deliveryAddress,
            deliveryCrewSize: changedFields.deliveryCrewSize,
            crewSize: changedFields.crewSize,
            pickupDate: changedFields.pickupDate,
            deliveryDate: changedFields.deliveryDate,
            deliveryTimeWindowStart: changedFields.deliveryTimeWindowStart,
            deliveryTimeWindowEnd: changedFields.deliveryTimeWindowEnd,
            pickupTimeWindowStart: changedFields.pickupTimeWindowStart,
            pickupTimeWindowEnd: changedFields.pickupTimeWindowEnd,
            location: changedFields.location,
            notes: changedFields.notes,
					},
				});
			}
			
		}, 0);				
		
		return null;
	})
	
	return result;
},
savingRequestSideEffectsAfterSave$moving_v1:function(savedRequest, oldRequest, additionalParams = {}) {

	let result = Promise.all([
		this.updateParklotJobsForRequest(savedRequest),
	]);
	
	result.then(([updatingParklotRes]) => {
		
		setTimeout(() => {
			this.doTask('saveRequestEmail')(savedRequest, oldRequest);
			this.saveRequestToSolr$v1(savedRequest);
			this.saveRequestLog(savedRequest, oldRequest);
			if (!additionalParams.fromGoogle) this.sendRequestToGoogleCalendar$v1(savedRequest, oldRequest);
			
			
			let changedFields = {};
			Object.keys(savedRequest).forEach(requestField => {
				let isFieldIgnored = requestField === 'savedVariables';
				let isFieldChanged = !isFieldIgnored && (
					(additionalParams.updatedFields || []).indexOf(requestField) !== -1
					|| !_.isEqual(savedRequest[requestField], oldRequest[requestField])
				);
				if (isFieldChanged) {
					changedFields[requestField] = savedRequest[requestField];
				}
			});
			
			if (Object.keys(changedFields).length) {
				this.services.gate('socketSendToRequest', {
					requestId: savedRequest.id,
					event: 'requestFieldsUpdated',
					data: Object.assign({}, {id: savedRequest.id}, changedFields),
				});
			}

			if (updatingParklotRes.isChanged && changedFields.parklot) {
				this.services.gate('socketSendToParklot', {
					event: 'parklotChanged',
					data: {
						newValue: changedFields.parklot,
						oldValue: oldRequest.parklot,
					},
				});
			}
			
			if (		changedFields.crewSize
					|| changedFields.pickupAddress
					|| changedFields.deliveryAddress) {
				this.services.gate('socketSendToParklot', {
					event: 'parklotRequestChanged',
					data: {
						id: savedRequest.id,
						crewSize: changedFields.crewSize,
						pickupAddress: changedFields.pickupAddress,
						deliveryAddress: changedFields.deliveryAddress,
					},
				});
			}
			
		}, 0);				
		
		return null;
	})
	
	return result;
},
getRequestInventoryField$moving_v1:function (requestId) {
	return this.services.inventory('getRequestRooms', requestId)
},
sideCalculationsBeforeMain$v1:function ({request, calculatorSettings}) {
	let updatingParklotPromise = this.services.parklot('calculateRequestParklot', request)
		.then(parklotField => {		
			request.parklot = parklotField;
		});
		
	let updatingLocation = this.services.location('calculateDistanceService', {
		request,
		calculatorSettings
	})
	.then(locationField => {
		request.location = locationField;
	});
	
	let gettingTaxSettings = this.services.request('getRequestTaxes', request)
		.then((savedTaxes) => {
			request.savedTaxes = savedTaxes;
		});

		
	return Promise.all([
		updatingParklotPromise,
		updatingLocation,
		gettingTaxSettings,
	])
		.then(responces => {
			return request;
		});
},
createReceipt$moving_v1:function (data) {
	let operationInfo = {
		updateFieldResponce: null,
		updatedRequestFields: null,
				
		status: 200,
		updateFieldError: null,
		updateRequestError: null,
	};

	let logTitle = 'Receipt';
	let oldRequest;
	let newRequest;
	let newReceipt;

	return this.getRequest$v1({id: data.entityId})
		.then(res => {
			oldRequest = res;

			if (!data.refund) {
				let balance = oldRequest.balance ? +oldRequest.balance : 0;
				if (data.amount > balance) {
					throw new Error('Amount of payment is more than balance');
				}
			} else {
				logTitle = 'Refund';
			}
			return true;
		})
		.then(() => {
			//TODO move variable to payment service.
			return this.getManyVariables$v1([{"name":"authorizeNetKeys"}, {name: 'paymentSettings'}]);
		})
		.then(res => {
			if (res.authorizeNetKeys) data.connectionData = res.authorizeNetKeys;
			if (!_.get(res, 'paymentSettings.autoPaymentEnabled', false) && _.get(data, 'data.autopayment')) throw  'PAYMENT_AUTOPAYMENT_SETTINGS_DISABLED';
			return this.services.payment('createReceipt', data);
		})
		.catch(err => {
			delete data.cardNumber;
			delete data.cardCode;
			delete data.connectionData;
			delete data.expirationDate;

			this.saveLog$v1({
				type: 'receiptError',
				title: `${logTitle} Error`,
				body: {
					type: 'json',
					value: {
						receiptInfo: data,
						error: err.data || err.message || err
					}
				},
				entityType: 'request',
				entityId: data.entityId,
			});		
			err.updateFieldError = true;
			throw err;
		})
		.then(res => {
			operationInfo.updateFieldResponce = res;
			newReceipt = res;

			let receiptCreateEventName = _.get(newReceipt, 'data.autopayment') ? 'receiptSuccessAutoCharge' : 'receiptSuccess';
			
			this.saveLog$v1({
				type: receiptCreateEventName,
				title: `${logTitle} created ${_.get(newReceipt, 'data.autopayment') ? 'autocharge' : ''}`,
				body: {
					type: 'json',
					value: res
				},
				entityType: 'request',
				entityId: data.entityId,
			});
						
			return this.doTask('saveRequest')({id: data.entityId}, {updatedFields: ['payments']});
		})		
		.then(res => {
			newRequest = res;
			
			if (oldRequest.payments.total) {
				let receiptCreateEventName = _.get(newReceipt, 'data.autopayment') ? 'receiptSuccessAutoCharge' : 'receiptSuccess';

				this.triggerEmailerEvent$v1({
						request: newRequest,
						eventName: receiptCreateEventName,
						tokenParams: {'request.payments':{id: newReceipt.id, type:'Order updated'}}
					});
			}

			let requestWasJustPaid = newRequest.balance <= 0 && oldRequest.balance > 0 && !_.get(oldRequest, 'payments.rows', []).length;
			
			if (requestWasJustPaid && this.systemData.source !== 'dashboard') {
				this.triggerEmailerEvent$v1({
					request: res,
					eventName: 'requestPaid'
				});
			}

			operationInfo.updatedRequestFields = {
				payments: newRequest.payments,
			};
			
			if (newRequest.status !== oldRequest.status) {
				operationInfo.updatedRequestFields.status = newRequest.status;
			}
			
			return operationInfo;
		})
		.catch(err => {
			if (err.updateFieldError) {
				operationInfo.updateFieldError = err;	
			} else {			
				operationInfo.updateRequestError = err;	
			}
			operationInfo.status = 500;
			return operationInfo;
		});
},
saveRequestEmail$moving_v1:function (savedRequest, oldRequest) {
	let promises = [];
	let CONST = {};
	return this.doTask('getConstants')(['request'])
		.then(res => {
			CONST = res;
			if (_.isEmpty(oldRequest)) {
				return this.triggerEmailerEvent$v1({
					request: savedRequest,
					eventName: 'requestCreated'
				});
			} else {
				if (oldRequest.status !== CONST.request.STATUS_NUMBER.DELIVERY
					&& savedRequest.status === CONST.request.STATUS_NUMBER.DELIVERY) {
					promises.push(this.triggerEmailerEvent$v1({
						request: savedRequest,
						eventName: 'requestDeliveryStatus'
					}));
				}

				if (oldRequest.deliveryDate !== savedRequest.deliveryDate) {
					promises.push(this.services.emailer('removeSendedEmailLog', {
						requestId: savedRequest.id,
						event: 'deliveryReminder5'
					}));
				}

				if (oldRequest.pickupDate !== savedRequest.pickupDate) {
					promises.push(this.services.emailer('removeSendedEmailLog', {
						requestId: savedRequest.id,
						event: 'pickupReminder1'
					}));
					promises.push(this.services.emailer('removeSendedEmailLog', {
						requestId: savedRequest.id,
						event: 'pickupReminder5'
					}));
				}
			}

			return Promise.all(promises);
		})

},
sideCalculationsAfterMain$v1:async function (newRequest, oldRequest, calculatorSettings) {

	let CONST = await this.doTask('getConstants')(['request']);

	let requestWasJustPaid = newRequest.balance <= 0 && oldRequest.balance > 0;
	let needToChangeStatusAfterPayment = requestWasJustPaid
					&& (newRequest.status === CONST.request.STATUS_NUMBER.PENDING
							|| newRequest.status === CONST.request.STATUS_NUMBER.PAID)
					&&	this.systemData.source !== 'dashboard';
					
	if (needToChangeStatusAfterPayment) {
		newRequest.status = CONST.request.STATUS_NUMBER.DELIVERY;
	}
	
	return Promise.resolve([]);
},
sideCalculationsBeforeMain$moving_v1:function ({request, calculatorSettings}) {

	let updatingParklotPromise = this.services.parklot('calculateRequestParklot', request)
		.then(parklotField => {		
			request.parklot = parklotField;
		});
		
	let updatingLocation = this.services.location('calculateDistanceService', {
		request,
		calculatorSettings
	})
	.then(locationField => {
		request.location = locationField;
		return request;
	});

		
	return Promise.all([
		updatingParklotPromise,
		updatingLocation
	])
		.then(responces => {
			return request;
		});
},
calculateRequest$moving_v1:function (request) {

	let calculatorSettings;
	let iterationCounter = 0;
	
	let calculateFields = (request, calculatorSettings) => {
		iterationCounter += 1;
		let newRequest = _.cloneDeep(request);
		return this.doTask('sideCalculationsBeforeMain')({
					request: newRequest,
					calculatorSettings
				})
				.then(() => {
					return this.services.calculator('calculate', {
						request: newRequest,
						calculatorSettings
					})
				})
				.then((calculatedRequest) => {
					Object.assign(newRequest, calculatedRequest);
					return this.doTask('sideCalculationsAfterMain')(						
						newRequest,
						request,
						calculatorSettings
					)
				})				
				.then(() => {
					//let diff = commonsLibs.objectsDiff(newRequest, request);
					//let needAnotherIteration = !!Object.keys(diff).length;
					let needAnotherIteration = !_.isEqual(newRequest, request);
					if (needAnotherIteration) {
						if (iterationCounter > 10) {
							throw new Error('Max calculator iteration count exceeded');
						}
						return calculateFields(newRequest, calculatorSettings);
					}
					return newRequest;
				});
	};

	return this.getCalculatorSettings$v1(request)
		.then(res => {
		
			calculatorSettings = res;
			
			return calculateFields(request, calculatorSettings);
			
		});
},
sideCalculationsAfterMain$moving_v1:async function (newRequest, oldRequest, calculatorSettings) {

	let CONST = await this.doTask('getConstants')(['request']);

	let requestWasJustPaid = newRequest.balance <= 0 && oldRequest.balance > 0;
	let needToChangeStatusAfterPayment = requestWasJustPaid
					&& (newRequest.status === CONST.request.STATUS_NUMBER.PENDING
							|| newRequest.status === CONST.request.STATUS_NUMBER.PAID)
					&&	this.systemData.source !== 'dashboard';
					
	if (needToChangeStatusAfterPayment) {
		newRequest.status = CONST.request.STATUS_NUMBER.DELIVERY;
	}
	
	return Promise.resolve([]);
},
getInventoryMovesizeSettings$moving_v1:function () {
	return this.services.inventory('getInventoryMovesizeSettings', {});
},
calculateInventoryMovesize$moving_v1:function (data) {
	return this.services.inventory('calculateMovesize', data);
},
saveInventoryMovesize$moving_v1:function (data) {
	return this.services.inventory('saveMovesize', data)
	.then(inventorySettingsChanges => {
		this.services.gate('socketSendToInventory', {
			event: 'inventorySettingsChanged',
			data: inventorySettingsChanges
		});
		return true;
	});
},
removeInventoryMovesize$moving_v1:function(movesizeId) {
	return this.services.inventory('removeMovesize', movesizeId)
	.then(inventorySettingsChanges => {
		this.services.gate('socketSendToInventory', {
			event: 'inventorySettingsChanged',
			data: inventorySettingsChanges
		});
		return true;
	});
},
getConstants$v1:function (services) {

	if (!services || _.isEmpty(services)) services = [
		'googleCalendar', 'request', 'calculator', 'parklot', 'globals'
	];

	return Promise.all(services.map(serviceName => {
		if (serviceName !== 'globals') {
			return this.services[serviceName]('constantine');
		} else return {
			AVAILABLE_FLOORS: ['e', 1, 2, 3, 4, 5],
		};
	}))
		.then(resArray => {
			let result = {};
			resArray.forEach((value, index) => {result[services[index]] = value});
			return result;
		});
},
getPaymentCustomerProfile$v1:function(data) {
	return this.getVariable$v1({"name":"authorizeNetKeys"})
		.then(res => {
			if (!_.isEmpty(res.value)) data.connectionData = res.value;
			return this.services.payment('getCustomerProfile', data);
		})

},
saveTaxes$v1:function(data) {
	return this.services.request('saveTaxes', data);
},
getTaxes$v1:function(filter) {
	return this.services.request('getTaxes', filter);
},
getAllTaxes$v1:function(data) {
	return this.services.request('getAllTaxes', data);
},
deleteTaxes$v1:function(id) {
	return this.services.request('deleteTaxes', id);
},
syncRequestToArrivy$v1:async function(request, oldRequest, {constants, variables} = {}) {
	if (!constants) constants = await this.doTask('getConstants')(['request']);
	if (!variables)  variables = await this.getManyVariables$v1([{name:'arrivySettings'}, {name:'arrivyRequestStatutes'}]);


	let dataForArrivy = {
				request,
				arrivySettings: variables.arrivySettings || {},
				arrivyRequestStatutes:variables.arrivyRequestStatutes || {},
				constants
	};
	return this.services.arrivy('syncRequest', dataForArrivy)
		.then(res => {
			this.saveLog$v1({
				type: 'arrivyChanged',
				title: 'Request added to arrivy',
				entityType: 'request',
				entityId: request.id,
				body: {
					type: 'json',
					value: res,
				},
			});
		})
		.catch(err => {
			this.saveLog$v1({
				type: 'arrivyChanged',
				title: 'Error request added to arrivy',
				entityType: 'request',
				entityId: request.id,
				body: {
					type: 'json',
					value: {err, request},
				},
			});
			throw err;
		})

},
syncRequestsToArrivyMany$v1:function(requestsIds) {
	return this.doTask('searchRequests')({filter:{id:requestsIds}})
		.then(async requests => {
			let constants = await this.doTask('getConstants')(['request']);
			let variables = await this.getManyVariables$v1([{name:'arrivySettings'}, {name:'arrivyRequestStatutes'}]);

			return Promise.all(requests.rows.map(request => {
				return this.syncRequestToArrivy$v1(request, {}, {constants, variables});
			}))
		})
		.then(res => {
			return res;
		})
		.catch(err => {
			throw err
		})

},
removeGoogleCalendars$v1:function() {
		return this.services.googleCalendar('disableCalendars')
	},
searchParklotJobsByStartTime$v1:function(data) {
	return this.services.parklot('searchParklotJobsByStartTime', data);
},
saveParklotJob$v1:function(data) {
	return this.services.parklot('updateParklotJob', data)
		.then(res => {
			this.services.gate('socketSendToParklot', {
					event: 'parklotChanged',
					data: {
						newValue: [res.newValue],
						oldValue: [res.oldValue],
					},
				});
		});
},
saveManyParklotJobs$v1:function(data) {
	return this.services.parklot('updateManyParklotJobs', data)
		.then(res => {
			this.services.gate('socketSendToParklot', {
					event: 'parklotChanged',
					data: {
						newValue: res.newValue,
						oldValue: res.oldValue,
					},
				});

			res.newValue.forEach(newJob => {

				let oldJob = _.find(res.oldValue, {id: newJob.id});
				let trucksChanged = oldJob.trucks.length !== newJob.trucks.length ||
					newJob.trucks.some(newTruckId => oldJob.trucks.indexOf(newTruckId) === -1);

				if (trucksChanged) {
					this.doTask('getRequest')({id: newJob.entityId})
					.then(res => {
						let oldValue = _.cloneDeep(res.parklot);
						let index = _.findIndex(oldValue, {id: oldJob.id});
						if (index !== -1) {
							oldValue[index] = oldJob;
						}
						this.saveLog$v1({
							type: 'parklotChanged',
							title: 'Parklot Changed',
							entityType: 'request',
							entityId: res.id,
							body: {
								type: 'json',
								value: {
									newValue: res.parklot,
									oldValue,
								},
							},
						});
					});
				}
			});
		});
},
bookedRemindersDay$v1:function (day) {
	// Логика такая: у нас есть bookedDate. Мы отправляем ремайндеры после этого bookedDate. 1,3,5,7... дней.
	// Если статус delivery и bookedDate меньше либо равен нужной дате, то отправляем письмо.
	day = +day;
	let startTime = moment.tz(this.companyData.timeZone.name).subtract(day + 1, 'd').unix();
	let endTime = moment.tz(this.companyData.timeZone.name).subtract(day, 'd').unix();
	let event = `bookedReminderDay${day}`;

	let CONST = {};
	return this.doTask('getConstants')(['request'])
		.then(res => {
			CONST = res;
			return this.services.emailer('getSendedEmails', {event})
		})
		.then(res => {
			let filter = {
				bookedDate: {
					op: 'BETWEEN',
					data: [startTime, endTime]
				},
				status: CONST.request.STATUS_NUMBER.DELIVERY
			};
			if (res && !_.isEmpty(res.rows)) {
				let ids = [];
				res.rows.map(row => {
					if (row.requestId) return ids.push(row.requestId);
				});
				ids = _.uniq(ids);
				filter.id = {
					op: 'NOT IN',
					data: ids
				};
			}
			return this.searchRequests$v1({filter});
		})
		.then(async res => {
			if (res && !_.isEmpty(res.rows)) {
				let smtpSettingsVariable = await this.getVariable$v1({name: 'smtpSettings'});
				let smtpSettings = _.get(smtpSettingsVariable, 'value');

				return Promise.all(res.rows.map(request => {
					return this.triggerEmailerEvent$v1({
						request,
						eventName: event,
						smtpSettings
					});
				}));
			} else {
				return Promise.resolve(`No request`);
			}
		});
	},
deliveryReminderAfter1day$v1:function () {
		let startTime = moment.tz(this.companyData.timeZone.name).subtract(2, 'd').unix();
		let endTime = moment.tz(this.companyData.timeZone.name).subtract(1, 'd').unix();

		let event = 'deliveryReminderAfter1';
		let CONST = {};
		return this.doTask('getConstants')(['request'])
			.then(res => {
				CONST = res;
				return this.services.emailer('getSendedEmails', {event})
			})
			.then(res => {
				let filter = {
					deliveryDate: {
						op: 'BETWEEN',
						data: [startTime, endTime]
					},
					status:{op:'>=', data: CONST.request.STATUS_NUMBER.DELIVERY}
				};
				if (res && !_.isEmpty(res.rows)) {
					let ids = [];
					res.rows.map(row => {
						if (row.requestId) return ids.push(row.requestId);
					});
					ids = _.uniq(ids);
					filter.id = {
						op: 'NOT IN',
						data: ids
					};
				}
				return this.searchRequests$v1({filter});
			})
			.then(res => {
				if (res && !_.isEmpty(res.rows)) {
					return Promise.all(res.rows.map(request => {
						return this.triggerEmailerEvent$v1({
							request,
							eventName: event
						});
					}));
				} else {
					return Promise.resolve(`No request`);
				}
			});
	},
arrivyGetAllTemplatesStatuses$v1:function() {
	return this.getManyVariables$v1([{name: 'arrivySettings'}])
		.then((variables) => {
			return this.services.arrivy('getAllTemplatesStatuses', variables);
		});


},

getStatisticForPeriod:function(period) {
	let startTime, endTime, startTimeCurrentYear, endTimeCurrentYear;

	if (period === 'month') {
		startTime = moment.tz(this.companyData.timeZone.name).subtract(1, 'year').subtract(1, 'month').startOf(period).unix();
		endTime = moment.tz(this.companyData.timeZone.name).subtract(1, 'year').subtract(1, 'month').endOf(period).unix();

		startTimeCurrentYear = moment.tz(this.companyData.timeZone.name).subtract(1, 'month').startOf(period).unix();
		endTimeCurrentYear = moment.tz(this.companyData.timeZone.name).subtract(1, 'month').endOf(period).unix();
	}
	else {
		startTime = moment.tz(this.companyData.timeZone.name).subtract(1, 'year').startOf(period).unix();
		endTime = moment.tz(this.companyData.timeZone.name).subtract(1, 'year').endOf(period).unix();

		startTimeCurrentYear = moment.tz(this.companyData.timeZone.name).startOf(period).unix();
		endTimeCurrentYear = moment.tz(this.companyData.timeZone.name).endOf(period).unix();

	}


	let result = {};
	return this.services.request('searchRequests', {pagination:{itemsOnPage:99999}, fields:['id'], filter:{status:{op:'>=', data:1}, createdAt:{op:'BETWEEN', data:[startTime, endTime]}}})
		.then(res => {
			result.lastYearsRequests = res.count;
			return this.services.payment('getRequestReceipts', _.uniq(_.map(res.rows, 'id')));
		})
		.then(res => {
			result.lastYearReceipts = res;
			return this.services.request('searchRequests', {pagination:{itemsOnPage:99999},fields:['id'], filter:{status:{op:'>=', data:1}, createdAt:{op:'BETWEEN', data:[startTimeCurrentYear, endTimeCurrentYear]}}})

		})
		.then(res => {
			result.currentYearRequests = res.count;
			return this.services.payment('getRequestReceipts', _.uniq(_.map(res.rows, 'id')));
		})
		.then(res => {
			result.currentYearReceipts = res;
			return result
		})
},
sendStatisticEmailForPeriod$v1:function(period) {
	return this.getStatisticForPeriod(period)
		.then(statistic => {
			return this.triggerEmailerEvent$v1({request:{}, statistic}, {eventName: `${period}Statistic`});
		})
},
getAreaForAddressBatch$v1:async function({ addresses }) {
    let calculatorSettings = await this.getCalculatorSettings$v1();
    return Promise.all(addresses.map(address => {
      if (!address) return Promise.resolve(null);
      return this.services.location("getAreaForAddress", {address, calculatorSettings})
        .then(res => {
          if (!res || res === "" || _.isEmpty(res)) return null;
          return res;
        })
        .catch(err => null);
    }));
  },

};