module.exports = {
entitiesStructure$v1:function () { return [
  {
    name: "Message",
    fields: {
      authorId: {array: false, type: "integer"},
      requestId: {array: false, type: "integer"},
      parentId: {array: false, type: "integer"},
      type: {array: false, type: "integer"},
      body: {array: false, type: "text"}, 
      readedBy:{array: true, type: "integer"}
    }
  }
]},
getMessages$v1:function (data) {
  return this.companyData.entityInstance.getEntityMultiply('Message', data);
},
saveMessage$v1:function (data) {
	data.readedBy = [this.systemData.authorizedUser.id];
	return this.companyData.entityInstance.save('Message', data)
		.then(res => ({totalCount: 1, rows: [res]}));
},
addRead$v1:function ({messageId}) {
  return this.companyData.entityInstance.save('Message', 
  	{
  		readedBy: {op:'add', value:[this.systemData.authorizedUser.id]}
  	}
  	, messageId);
},
getUserUnread$v1:function (filter) {
	filter.readedBy = this.systemData.authorizedUser.id;
	let data = {
		filter,
		pagination: {itemsOnPage:999999999999},
		fields:['id', 'requestId']

	};
	return this.companyData.entityInstance.getEntityMultiply('Message', data)
		.then(messagesIds => {
			delete data.filter.readedBy;
			if (!_.isEmpty(messagesIds.rows)) data.filter.id = {'op': 'NOT IN', data:messagesIds.rows.map(message => message.id)};

			return this.companyData.entityInstance.getEntityMultiply('Message', data)
		})
		.then((messages = {}) => {
			return messages;
		})
},
getUserAllUnreadGroupedByRequest$v1:function (data) {
	let dataForReaded = {
		filter:{readedBy:this.systemData.authorizedUser.id} ,
		pagination: {itemsOnPage:999999999999},
		fields:['id'],
	};
	return this.companyData.entityInstance.getEntityMultiply('Message', dataForReaded)
		.then(messagesIds => {
			if (!_.isEmpty(messagesIds.rows)) _.set(data, 'filter.id', {'op': 'NOT IN', data:messagesIds.rows.map(message => message.id)});
			return this.companyData.entityInstance.getEntityMultiply('Message', data)
		})
		.then((res) => {
			let requestIds = {};
			res.rows.forEach(message => {
				if (!requestIds[message.requestId]) requestIds[message.requestId] = [];
				requestIds[message.requestId].push(message);
			});
			return requestIds;
		})
},
getMessagesGroupedByRequest$v1:function ({pagination, filter = {}}) {
	let entityName = 'Message';
	let requestIdFieldModel = this.companyData.entityInstance.getFieldModel(entityName, 'requestId');

	pagination = this.companyData.entityInstance.EntityTasks().buildPagination(pagination || {});
	let paginationString = `LIMIT ${pagination.offset}, ${pagination.limit}`;

	let filterString = '';
	if (filter.requestId && _.isArray(filter.requestId)) {
		filterString += ` WHERE \`value\` IN (${filter.requestId.join(',')})`
	}

	let count = 0;

	let countQuery = `SELECT max(\`entityId\`) \`entityId\`, value FROM \`${requestIdFieldModel.tableName}\` ${filterString} GROUP BY \`value\` ORDER BY \`entityId\` DESC`;

	return this.companyData.DB.queryInterface.sequelize.query(countQuery, {type: this.companyData.DB.queryInterface.sequelize.QueryTypes.SELECT})
		.then(res => {
			count = res.length || count;
			if (count) {
				let query = `SELECT max(\`entityId\`) \`entityId\`, value FROM \`${requestIdFieldModel.tableName}\` ${filterString} GROUP BY \`value\` ORDER BY \`entityId\` DESC ${paginationString}`;

				return this.companyData.DB.queryInterface.sequelize.query(query, {type: this.companyData.DB.queryInterface.sequelize.QueryTypes.SELECT});
			}
			else {
				return Promise.resolve([]);
			}
		})
		.then(res => {
			let userId = this.systemData.authorizedUser.id;
			let requestsIds = [];
			if (res && !_.isEmpty(res)) {
				requestsIds = res.map(row => row.value);
			}

			if (requestsIds.length) {
				return this.companyData.entityInstance.getEntityMultiply('Message', {pagination:{itemsOnPage:999999999999}, filter:{requestId: requestsIds, readedBy:{op:'NOT IN', data:userId}}})
					.then(res => {
						let requestsIds = _.map(res.rows, 'requestId');
						requestsIds = _.uniq(requestsIds);

						return {count, requestsIds};
					})
			}
			else {
				return {count, requestsIds:[]};
			}
		})
		.catch(err => {
			throw err;
		})
},

};