module.exports = {
autocompleteAddress$v1:function({ request, areas }) {

    return autocomplete.autocompleteAddress(request, areas);

  },
geocodeAddress$v1:function(data) {
    return autocomplete.geocodeAddress(data);
  },
calculateDistanceService$v1:function({ request, calculatorSettings }) {

    let areas = calculatorSettings.zonesSettings || [];

    let deliveryPromise = this.checkPointInArea$v1({
      address: request.deliveryAddress,
      areas
    });

    let pickupPromise = this.checkPointInArea$v1({
      address: request.pickupAddress,
      areas
    });

    return Promise.all([deliveryPromise, pickupPromise])
      .then(([deliveryRes, pickupRes]) => {
        return {
          delivery: deliveryRes,
          pickup: pickupRes
        };
      });
  },
getFullAddressRow$v1:function(address) {
    let result = "";
    if (address.address) result += address.address;
    if (address.city) result += ", " + address.city;
    if (address.stateName) result += ", " + address.stateName;
    if (address.postal) result += ", " + address.postal;
    if (address.apt) result += "Apt. " + address.apt;
    return result;
  },
checkPointInArea$v1:function({ address, areas }) {

    let addressString = this.getFullAddressRow$v1(address);
    if (!addressString.length) return Promise.resolve();

    return this.geocodeAddress$v1({
      address: addressString,
      components: {
        country: ["us"]
      }
    })
      .then((res) => {
        return _.get(res, "[0].geometry.location");
      })
      .then(latLng => {
        if (!latLng) return null;
        for (let areaIndex in areas) {
          if (autocomplete.containsLocation(areas[areaIndex].polygon,
            latLng)
          ) {
            return areas[areaIndex];
            break;
          }
        }
        return null;
      })
      .catch(err => null);
  },
getAreaForAddress$v1:function({ address, calculatorSettings }) {

    return this.checkPointInArea$v1({
      address: address,
      areas: calculatorSettings.zonesSettings || [],
    });

  },
geocodeAddressBatch$v1:function(data) {
    return Promise.all(
      data.map(address => autocomplete.geocodeAddress(address)
        .catch(err => null)
      )
    )
      .then(res => res.filter(res => res));
  },
calculateDistanceService$moving_v1:function({ request, calculatorSettings }) {

    let areas = _.get(calculatorSettings, "servicesInfo.systemServices.distanceCharge", []);

    let deliveryPromise = this.checkPointInArea$v1({
      address: request.deliveryAddress,
      areas
    });

    let pickupPromise = this.checkPointInArea$v1({
      address: request.pickupAddress,
      areas
    });

    return Promise.all([deliveryPromise, pickupPromise])
      .then(([deliveryRes, pickupRes]) => {
        return {
          delivery: deliveryRes,
          pickup: pickupRes
        };
      });
  },

};