module.exports = {
save$v1:function (data) {
	return this.companyData.solrInstance.save(data);
},
delete$v1:function (data) {
	return this.companyData.solrInstance.delete(data);
},
deleteAll$v1:function () {
	return this.companyData.solrInstance.deleteAll();
},
search$v1:function (data) {
	return this.companyData.solrInstance.search(data.text.trim());
},

};