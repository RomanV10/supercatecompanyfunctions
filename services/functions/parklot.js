module.exports = {
entitiesStructure$v1:function () {
	return [
		{
			name: 'Truck',
			fields: {
				name: FieldBuilder.makeStructure({ array: false, type: 'string' }),
				zone: FieldBuilder.makeStructure({ array: true, type: 'integer', defaultValue: [] }),
				isDefault: FieldBuilder.makeStructure({ array: false, type: 'integer', defaultValue: 0 }),
			},
		},
		{
			name: 'Job',
			fields: {
				status: FieldBuilder.makeStructure({ array: false, type: 'integer', defaultValue: 0 }),
				type: FieldBuilder.makeStructure({ array: false, type: 'integer', defaultValue: 0 }),
				startTime: FieldBuilder.makeStructure({ array: false, type: 'integer', defaultValue: 0 }),
				endTime: FieldBuilder.makeStructure({ array: false, type: 'integer', defaultValue: 0 }),
				entityType: FieldBuilder.makeStructure({ array: false, type: 'string', defaultValue: 'Request' }),
				entityId: FieldBuilder.makeStructure({ array: false, type: 'integer'}),
				trucks: FieldBuilder.makeStructure({ array: true, type: 'integer', defaultValue: [] }),
				listWeight: FieldBuilder.makeStructure({ array: false, type: 'integer', defaultValue: 0 }),
			},
		},
	]
},
getSettings$v1:function () {
	let promises = [];
	promises.push(this.getManyEntities$v1({name: 'Truck'}));
	return Promise.all(promises)
		.then(res => (
				{
					trucks: res[0].rows,
					constants: this.doTask('constants')(),
				}
			)
		);
},
getManyEntities$v1:function (data) {
	return this.companyData.entityInstance.getEntityMultiply(data.name, data.conditions);
},
saveTruck$v1:function (data) {
	return this.companyData.entityInstance.save('Truck', data, data.id);
},
removeTruck$v1:function (data) {
	return this.companyData.entityInstance.remove('Truck', data.id);
},
constants$v1:function () {
	return {
		JOB_TYPES: {
			DELIVERY: 0,
			PICKUP: 1,
		},
		JOB_STATUS: {
			PENDING: 0,
			CONFIRMED: 1,
		},
	};
},
getRequestJobs$v1:function (request) {
	if (!request.id) return Promise.resolve([]);

	return this.companyData.entityInstance.getEntityMultiply('Job', {
		filter: {
			entityId: request.id,
			entityType: 'Request'
		}
	})
		.then(res => this.doTask('createMissingRequestJobs')(res.rows, request))
		.then(res => {
			_.forEach(res, res => {
				if (!res.trucks) res.trucks = [];
				delete res.ELID;
				delete res.createdAt;
				delete res.updatedAt;
			});
			return res;
		});
},
updateJobsForRequest$v1:function (request) {
	if (!request.parklot) request.parklot = [];

	let result = {};

	return this.getRequestJobs$v1(request)
		.then(oldJobs => {
			result.oldValue = _.sortBy(oldJobs, ['id']);
			return this.doTask('removeUselessJobs')(oldJobs, request);
		})
		.then(() => {
			return Promise.resolve();
		})
		.then(() => {
			request.parklot.forEach(job => {
				job.trucks.sort((a, b) => a > b ? 1 : a < b ? -1 : 0);
        job.trucks = _.uniq(job.trucks);
			});
			return Promise.all(request.parklot.map(this.saveJob$v1));
		})
		.then(res => {
			result.newValue = _.sortBy(res, ['id']);
			result.isChanged = this.isJobsChanged$v1(result.oldValue, result.newValue);
			return result;
		});
},
saveJob$v1:function (data) {
	return this.companyData.entityInstance.save('Job', data, data.id);
},
calculateParklotJob$v1:function (data) {
	const CONST = this.doTask('constants')();
	let job = {};

	job.status = data.request.status >= 1
		? CONST.JOB_STATUS.CONFIRMED
		: CONST.JOB_STATUS.PENDING;

	if (data.jobType === CONST.JOB_TYPES.DELIVERY) {

		job.startTime = data.request.deliveryTime;
		job.endTime = data.request.deliveryTime + data.request.deliveryWorkTime * 60;

	} else if (data.jobType === CONST.JOB_TYPES.PICKUP) {

		job.startTime = data.request.pickupTime;
		job.endTime = data.request.pickupTime + data.request.pickupWorkTime * 60;

	}

	return job;
},
searchParklotJobsForRequests$v1:function (conditions = {}) {
	const CONST = this.doTask('constants')();
	conditions.entityType = 'Request';
	if (conditions.dateFrom && conditions.dateTo) {
		conditions.operation = {
			'or': [
				{
					'operation': {
						'and': [
							{
								'startTime': {
									'op': '>=',
									'data': conditions.dateFrom
								}
							},
							{
								'startTime': {
									'op': '<=',
									'data': conditions.dateTo
								}
							},
							{
								'status': {
									'op': '>=',
									'data': CONST.JOB_STATUS.CONFIRMED,
								}
							}
						]
					}
				},
				{
					'operation': {
						'and': [
							{
								'startTime': {
									'op': '<=',
									'data': conditions.dateFrom
								}
							},
							{
								'endTime': {
									'op': '>=',
									'data': conditions.dateFrom
								}
							},
							{
								'status': {
									'op': '>=',
									'data': CONST.JOB_STATUS.CONFIRMED,
								}
							}
						]
					}
				}
			]
		};
	}

	return this.companyData.entityInstance.getEntityMultiply('Job', {
		filter: conditions,
		pagination: {
			pageNumber: 1,
			itemsOnPage: 999999
		}
	});
},
createMissingRequestJobs$v1:function (jobs, request) {
	const CONST = this.doTask('constants')();
	let promises = [];

	if (!_.find(jobs, {type: CONST.JOB_TYPES.DELIVERY})) {
		let job = {
				type: CONST.JOB_TYPES.DELIVERY,
				entityType: 'Request',
				entityId: request.id,
				trucks: [],
			};
		promises.push(
			this.saveJob$v1(Object.assign(job, this.calculateParklotJob$v1({
				jobType: CONST.JOB_TYPES.DELIVERY, request
			})))
		);
	}

	if (!_.find(jobs, {type: CONST.JOB_TYPES.PICKUP})) {
		let job = {
				type: CONST.JOB_TYPES.PICKUP,
				entityType: 'Request',
				entityId: request.id,
				trucks: [],
			};
		promises.push(
			this.saveJob$v1(Object.assign(job, this.calculateParklotJob$v1({
				jobType: CONST.JOB_TYPES.PICKUP, request
			})))
		);
	}

	return Promise.all(promises)
		.then(newJobs => newJobs.concat(jobs));

},
isJobsChanged$v1:function (oldJobs, newJobs) {
	oldJobs = oldJobs.sort((a, b) => a.id > b.id ? 1 : a.id < b.id ? -1 : 0);
	newJobs = newJobs.sort((a, b) => a.id > b.id ? 1 : a.id < b.id ? -1 : 0);
	let idsChanged = oldJobs.length !== newJobs.length;
	if (idsChanged) return true;

	for (let i = 0; i < oldJobs.length && !idsChanged; i++) {
		idsChanged = idsChanged || oldJobs[i].id !== newJobs[i].id;

		if (!idsChanged) {

			if (!oldJobs[i].trucks) oldJobs[i].trucks = [];
			if (!newJobs[i].trucks) newJobs[i].trucks = [];
			let jobHaveNotTrucksEvenAndNow = !oldJobs[i].trucks.length && !newJobs[i].trucks.length;

			if (true || !jobHaveNotTrucksEvenAndNow) {
				let trucksWasChanged = oldJobs[i].trucks.length !== newJobs[i].trucks.length;
				idsChanged = idsChanged || trucksWasChanged;

				if (!idsChanged) {
					let fieldsChanged =
						oldJobs[i].status !== newJobs[i].status ||
						oldJobs[i].type !== newJobs[i].type ||
						oldJobs[i].startTime !== newJobs[i].startTime ||
						oldJobs[i].endTime !== newJobs[i].endTime ||
						oldJobs[i].entityType !== newJobs[i].entityType ||
						oldJobs[i].entityId !== newJobs[i].entityId;
					idsChanged = idsChanged || fieldsChanged;

					if (!idsChanged) {
						let oldTrucksSorted = oldJobs[i].trucks.sort((a, b) => a > b ? 1 : a < b ? -1 : 0);
						let newTrucksSorted = newJobs[i].trucks.sort((a, b) => a > b ? 1 : a < b ? -1 : 0);
						trucksWasChanged = oldTrucksSorted.some((truckId, index) =>
							truckId !== newTrucksSorted[index]
						);

						idsChanged = idsChanged || trucksWasChanged;
					}
				}
			}
		}
	}

	return idsChanged;

},
createMissingRequestJobs$moving_v1:function (jobs, request) {
	const CONST = this.doTask('constants')();
	let promises = [];

	let pickupRequired = request.serviceType === CONST.SERVICE_TYPES.localMove
	|| (request.serviceType === CONST.SERVICE_TYPES.movingWithStorage
		&& !request.movingWithStorageDirectionFrom);

	let deliveryRequired = request.serviceType === CONST.SERVICE_TYPES.movingWithStorage
		&& request.movingWithStorageDirectionFrom;

	if (deliveryRequired && !_.find(jobs, {type: CONST.JOB_TYPES.DELIVERY})) {
		let job = {
				type: CONST.JOB_TYPES.DELIVERY,
				entityType: 'Request',
				entityId: request.id,
				trucks: [],
			};
		promises.push(
			this.saveJob$v1(Object.assign(job, this.calculateParklotJob$v1({
				jobType: CONST.JOB_TYPES.DELIVERY, request
			})))
		);
	}

	if (pickupRequired && !_.find(jobs, {type: CONST.JOB_TYPES.PICKUP})) {
		let job = {
				type: CONST.JOB_TYPES.PICKUP,
				entityType: 'Request',
				entityId: request.id,
				trucks: [],
			};
		promises.push(
			this.saveJob$v1(Object.assign(job, this.calculateParklotJob$v1({
				jobType: CONST.JOB_TYPES.PICKUP, request
			})))
		);
	}

	return Promise.all(promises)
		.then(newJobs => newJobs.concat(jobs));

},
constants$moving_v1:function () {
	return {
		JOB_TYPES: {
			DELIVERY: 0,
			PICKUP: 1,
		},
		JOB_STATUS: {
			PENDING: 0,
			CONFIRMED: 1,
		},
		SERVICE_TYPES: {
			localMove: 0,
			movingWithStorage: 1,
		},
	};
},
removeUselessJobs$v1:function (oldJobs, request) {

	let removingUselessJobs = [];
	oldJobs.forEach(oldJob => {
		let jobIsUseless = !_.find(request.parklot, {id: oldJob.id});
		if (jobIsUseless) {
			removingUselessJobs.push(this.companyData.entityInstance.remove('Job', oldJob.id));
		}
	});

	return Promise.all(removingUselessJobs)
		.then(() => request.parklotJobs);
},
removeUselessJobs$moving_v1:function (oldJobs, request) {

	const CONST = this.doTask('constants')();

	let pickupRequired =  request.serviceType === CONST.SERVICE_TYPES.localMove
	|| ( request.serviceType === CONST.SERVICE_TYPES.movingWithStorage
		&& !request.movingWithStorageDirectionFrom);

	let deliveryRequired = request.serviceType === CONST.SERVICE_TYPES.movingWithStorage
		&& request.movingWithStorageDirectionFrom;

	let removingUselessJobs = [];
	oldJobs.forEach(oldJob => {
		let jobIsUseless = !_.find(request.parklot, {id: oldJob.id});
		if (jobIsUseless) {
			removingUselessJobs.push(this.companyData.entityInstance.remove('Job', oldJob.id));
		}
	});

	if (!deliveryRequired) {
		let deliveryOldJob = _.find(oldJobs, {type: CONST.JOB_TYPES.DELIVERY});
		if (deliveryOldJob) {
			removingUselessJobs.push(
				this.companyData.entityInstance.remove('Job', deliveryOldJob.id)
			);
		}
	}

	if (!pickupRequired) {
		let pickupOldJob = _.find(oldJobs, {type: CONST.JOB_TYPES.PICKUP});
		if (pickupOldJob) {
			removingUselessJobs.push(
				this.companyData.entityInstance.remove('Job', pickupOldJob.id)
			);
		}
	}

	return Promise.all(removingUselessJobs);
},
calculateParklotJob$moving_v1:function (data) {
	const CONST = this.doTask('constants')();
	let job = {};

	job.status = data.request.status >= 1
		? CONST.JOB_STATUS.CONFIRMED
		: CONST.JOB_STATUS.PENDING;

	let workTime = (data.request.laborTime || 0) + (data.request.drivingTime || 0) +
						(data.request.travelTime || 0);

	if (data.jobType === CONST.JOB_TYPES.DELIVERY) {

		job.startTime = data.request.deliveryTime;
		job.endTime = data.request.deliveryTime + workTime * 60;

	} else if (data.jobType === CONST.JOB_TYPES.PICKUP) {

		job.startTime = data.request.pickupTime;
		job.endTime = data.request.pickupTime + workTime * 60;

	}

	return job;
},
calculateRequestParklot$moving_v1:function(request) {
	if (!request.parklot) request.parklot = [];

	let result = {};

	this.doTask('filterRequestJobs')(request);
	request.parklot.forEach(job => {
		let calculatedJob = this.doTask('calculateParklotJob')({jobType: job.type, request});
		Object.assign(job, calculatedJob)
		//job.trucks.sort((a, b) => a > b ? 1 : a < b ? -1 : 0);
	});

	return request.parklot;
},
filterRequestJobs$moving_v1:function (request) {

	const CONST = this.doTask('constants')();

	let pickupRequired =  request.serviceType === CONST.SERVICE_TYPES.localMove
	|| request.serviceType === CONST.SERVICE_TYPES.movingWithStorage
		&& !request.movingWithStorageDirectionFrom;

	let deliveryRequired = request.serviceType === CONST.SERVICE_TYPES.movingWithStorage
		&& request.movingWithStorageDirectionFrom;

	let removingUselessJobs = [];

	if (!deliveryRequired) {
		let deliveryNewJob = _.findIndex(request.parklot, {type: CONST.JOB_TYPES.DELIVERY});
		if (deliveryNewJob !== -1) {
			request.parklot.splice(deliveryNewJob, 1);
		}
	}

	if (!pickupRequired) {
		let pickupNewJob = _.findIndex(request.parklot, {type: CONST.JOB_TYPES.PICKUP});
		if (pickupNewJob !== -1) {
			request.parklot.splice(pickupNewJob, 1);
		}

	}

	return request.parklot;
},
updateJobsForRequest$moving_v1:function (request) {
	if (!request.parklot) request.parklot = [];

	let result = {};

	return this.getRequestJobs$v1(request)
		.then(oldJobs => {
			result.oldValue = _.sortBy(oldJobs, ['id']);
			return this.doTask('removeUselessJobs')(oldJobs, request);
		})
		.then(() => {
			request.parklot.forEach(job => {
				job.trucks.sort((a, b) => a > b ? 1 : a < b ? -1 : 0);
			});
			return Promise.all(request.parklot.map(this.saveJob$v1));
		})
		.then(res => {
			result.newValue = _.sortBy(res, ['id']);
			result.isChanged = this.isJobsChanged$v1(result.oldValue, result.newValue);
			return result;
		});
},
filterRequestJobs$v1:function (request) {

	const CONST = this.doTask('constants')();

	let pickupRequired = true;

	let deliveryRequired = true;

	let removingUselessJobs = [];

	if (!deliveryRequired) {
		let deliveryNewJob = _.findIndex(request.parklot, {type: CONST.JOB_TYPES.DELIVERY});
		if (deliveryNewJob !== -1) {
			request.parklot.splice(deliveryNewJob, 1);
		}
	}

	if (!pickupRequired) {
		let pickupNewJob = _.findIndex(request.parklot, {type: CONST.JOB_TYPES.PICKUP});
		if (pickupNewJob !== -1) {
			request.parklot.splice(pickupNewJob, 1);
		}

	}

	return request.parklot;
},
searchParklotJobsByStartTime$v1:function (conditions = {}) {
	const CONST = this.doTask('constants')();
	conditions.entityType = 'Request';
	if (conditions.dateFrom && conditions.dateTo) {
		conditions.operation = {
			'and': [
				{
					'startTime': {
						'op': '>=',
						'data': conditions.dateFrom
					}
				},
				{
					'startTime': {
						'op': '<=',
						'data': conditions.dateTo
					}
				},
				{
					'status': {
						'op': '>=',
						'data': CONST.JOB_STATUS.CONFIRMED,
					}
				}
			]
		};
	}

	return this.companyData.entityInstance.getEntityMultiply('Job', {
		filter: conditions,
		pagination: {
			pageNumber: 1,
			itemsOnPage: 999999
		}
	});
},
updateParklotJob$v1:function(data) {
	let oldJob;
	return this.companyData.entityInstance.getEntity('Job', {id: data.id})
		.then(res => {
			oldJob = res;
			return this.saveJob$v1(data);
		})
		.then(res => {
			return {
				oldValue: oldJob,
				newValue: res,
			};
		})
},
updateManyParklotJobs$v1:function(data) {
	return Promise.all(data.map(this.updateParklotJob$v1))
		.then(res => {
			let oldValue = [];
			let newValue = [];
			res.forEach(oneJobRes => {
				oldValue.push(oneJobRes.oldValue);
				newValue.push(oneJobRes.newValue);
			});

			return {
				oldValue,
				newValue
			};
		});
},
findTruckAndTimeForJob$v1:function (date, zoneId) {
  let result = {
    trucks: [],
  };
  const SEC_IN_DAY = 86400;
  let trucks, defaultTruck, jobs;
  let dateTo = date + SEC_IN_DAY;
  return this.getManyEntities$v1({
    name: 'Truck',
    filter: {zone: zoneId},
    pagination: {
      pageNumber: 1,
      itemsOnPage: 999999
    }
  })
    .then(res => {
      trucks = res.rows.filter(truck => truck.zone.some(truckZoneId => +truckZoneId === +zoneId));
      defaultTruck = _.find(res.rows, truck => truck.isDefault);
      return this.doTask('searchParklotJobsForRequests')({dateFrom: date, dateTo: dateTo});
    })
    .then(res => {
      jobs = res.rows;
      let jobsByTrucks = trucks.map(truck => jobs.filter(job => job.trucks.indexOf(truck.id) !== -1));
      let jobsForDefaultTruck = defaultTruck
                                ? jobs.filter(job => job.trucks.indexOf(defaultTruck.id) !== -1)
                                : [];
      let targetTruckIndex = null;
      let targetTruckJobs = Infinity;
      jobsByTrucks.forEach((jobs, index) => {
        if (jobs.length < targetTruckJobs) {
          targetTruckIndex = index;
          targetTruckJobs = jobs.length;
        }
      });

      let selectedTruckJobs;

      if (targetTruckIndex === null && defaultTruck) {
        result.trucks = [defaultTruck.id];
        selectedTruckJobs = jobsForDefaultTruck;
      }

      if (targetTruckIndex !== null) {
        result.trucks = [trucks[targetTruckIndex].id];
        selectedTruckJobs = jobsByTrucks[targetTruckIndex];
      }

      if (result.trucks.length) {
        let startTime = moment.unix(date).tz(this.companyData.timeZone.name).startOf('day').add(8, 'hours');
        let today = moment.unix(date).tz(this.companyData.timeZone.name).startOf('day');
        while (startTime.isSame(today, 'day') && selectedTruckJobs.some(job => job.startTime === startTime.unix())) {
          startTime.add(15, 'minutes');
        }
        if (!startTime.isSame(today, 'day')) {
          startTime = moment(today).add(19, 'hours');
        }
        result.jobTime = startTime.unix();
      }
      return result;
    });
},
autoAssignTrucks$v1:function (request) {

  if (!request.parklot) request.parklot = [];

  let result = {};

  return Promise.resolve()
    .then(() => {
      const CONST = this.doTask('constants')();
      let promises = [];

      let pickupZone = _.get(request, 'location.pickup.id');
      let pickupDate = _.get(request, 'pickupDate');
      let pickupJob = _.find(request.parklot, {type: CONST.JOB_TYPES.PICKUP});
      let isPickupTruckSelected = !!pickupJob.trucks.length;
      if (pickupZone && pickupDate && !isPickupTruckSelected) {
        promises.push(
          this.findTruckAndTimeForJob$v1(pickupDate, pickupZone)
            .then(res => {
              pickupJob.trucks = res.trucks;
              if (res.jobTime) {
                request.pickupTime = res.jobTime;
                let calculatedJob = this.calculateParklotJob$v1({
                  request,
                  jobType: pickupJob.type
                });
                pickupJob.startTime = calculatedJob.startTime;
                pickupJob.endTime = calculatedJob.endTime;
              }
            })
        );
      }

      let deliveryZone = _.get(request, 'location.delivery.id');
      let deliveryDate = _.get(request, 'deliveryDate');
      let deliveryJob = _.find(request.parklot, {type: CONST.JOB_TYPES.DELIVERY});
      let isDeliveryTruckSelected = !!deliveryJob.trucks.length;
      if (deliveryZone && deliveryDate && !isDeliveryTruckSelected) {
        promises.push(
          this.findTruckAndTimeForJob$v1(deliveryDate, deliveryZone)
            .then(res => {
              deliveryJob.trucks = res.trucks;
              if (res.jobTime) {
                request.deliveryTime = res.jobTime;
                let calculatedJob = this.calculateParklotJob$v1({
                  request,
                  jobType: deliveryJob.type
                });
                deliveryJob.startTime = calculatedJob.startTime;
                deliveryJob.endTime = calculatedJob.endTime;
              }
            })
        );
      }

      return Promise.all(promises);
    })
    .then((res) => {
      result.parklot = request.parklot;
      result.pickupTime = request.pickupTime;
      result.deliveryTime = request.deliveryTime;
      return result;
    });
},

};