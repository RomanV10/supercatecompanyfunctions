module.exports = {
entitiesStructure$v1:function() {
	return [
		{
			name: "Movesize",
			fields: {
				name: FieldBuilder.makeStructure({array: false, type: "string"}),
				image: FieldBuilder.makeStructure({array: false, type: "string"}),
				removed: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue:0}),
				defaultRooms: FieldBuilder.makeStructure({array: true, type: "integer", defaultValue:[]}),
				suggestedRooms: FieldBuilder.makeStructure({array: true, type: "integer", defaultValue:[]}),
				crewSizeSettings: FieldBuilder.makeStructure({array: false, type: "json", defaultValue:{}}),
				dispersion: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 10}),
				trucksCount: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 1}),
				weight: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
				volume: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
				volumeWithDispersion: FieldBuilder.makeStructure({array: false, type: "json", defaultValue:{min: 0, max: 0}}),
				itemsCount: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),

			}
		},
		{
			name: "Room",
			fields: {
				name: FieldBuilder.makeStructure({array: false, type: "string"}),
				image: FieldBuilder.makeStructure({array: false, type: "string"}),
				categories: FieldBuilder.makeStructure({array: true, type: "integer", defaultValue:[]}),
				items: FieldBuilder.makeStructure({array: false, type: "json", defaultValue:{}}),
				itemsIds: FieldBuilder.makeStructure({array: true, type: "integer"}),
				removed: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue:0}),
				totalCount: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue:0}),
				totalBoxes: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue:0}),
				totalWeight: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue:0}),
				totalVolume: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue:0}),
				extraServices: FieldBuilder.makeStructure({array: false, type: "json", defaultValue:[]}),
				totalServicesCost: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue:0}),
				minCrewSize: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue:0}),
				additionalTime: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue:0}),
			}
		},
		{
			name: "RequestInventoryRoom",
			fields: {
				requestId: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue:0}),
				name: FieldBuilder.makeStructure({array: false, type: "string"}),
				items: FieldBuilder.makeStructure({array: false, type: "json", defaultValue:{}}),
				itemsIds: FieldBuilder.makeStructure({array: true, type: "integer"}),
				sourceRoom: FieldBuilder.makeStructure({array: false, type: 'entityReference', relatedEntity: "Room"}),

				totalCount: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue:0}),
				totalBoxes: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue:0}),
				totalWeight: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue:0}),
				totalVolume: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue:0}),
				extraServices: FieldBuilder.makeStructure({array: false, type: "json", defaultValue:[]}),
				totalServicesCost: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue:0}),
				minCrewSize: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue:0}),
				additionalTime: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue:0}),
				}
			},
		{
			name: "Item",
			fields: {
				isCustom: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue:0}),
				name: FieldBuilder.makeStructure({array: false, type: "string"}),
				image: FieldBuilder.makeStructure({array: false, type: "string"}),
				categories: FieldBuilder.makeStructure({array: true, type: "integer", defaultValue:[]}),
				removed: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue:0}),
				size: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue:0}),
				isBox: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue:0}),
				needService: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue:0}),
				extraService: FieldBuilder.makeStructure({array: false, type: "json", defaultValue:{}}),
			}
		},
		{
			name: "Category",
			fields: {
				name: FieldBuilder.makeStructure({array: false, type: "string"}),
				removed: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue:0}),
			}
		},
	]
},
saveRoom$v1:function(data) {
		permissions.inventory.isUserCanEditRoom(this.systemData.authorizedUser);
		let result = {
			rooms: {},
		};
		let oldRoom;

		//TODO merge with room from DB (if not all fields com from front calculator will fail)
		return this.getRoom$v1({id: data.id || 0})
		.then(_oldRoom => {
			oldRoom = _oldRoom;
			return this.calculateRoom$v1(data)
		})
		.then(calculatedRoom => {
			if (calculatedRoom.items) {
				calculatedRoom.itemsIds = Object.keys(calculatedRoom.items).map(a => +a);
			}
			
			return this.companyData.entityInstance.save('Room', calculatedRoom, data.id)
		})
		.then(savedRoom => {	
			delete savedRoom.ELID;	
			delete savedRoom.updatedAt;
			delete savedRoom.createdAt;
			let isChanged = Object.keys(savedRoom).some(key => !_.isEqual(oldRoom[key], savedRoom[key]));

			if (isChanged) {
				result.rooms[data.id || savedRoom.id] = savedRoom;
				return this.recalculateMovesizesAfterChangeRoom$v1(savedRoom.id);
			}
			return {};
		})
		.then(updatedMovesizes => {
			result.movesizes = updatedMovesizes;
			return result;
		});
	},
saveCategory$v1:function(data) {
		permissions.inventory.isUserCanEditCategory(this.systemData.authorizedUser);

		return this.companyData.entityInstance.save('Category', data, data.id)
		.then(savedCategory => {
			return {
				categories: {
					[data.id || savedCategory.id]: savedCategory,
				}
			};
		});
	},
saveItem$v1:function(data) {
		let savedItem;
		let itemsChanges = {};

		//TODO check for using this item in request
		permissions.inventory.isUserCanEditItem(this.systemData.authorizedUser);

		return this.companyData.entityInstance.save('Item', data, data.id)
			.then(_savedItem => {
				savedItem = _savedItem;
				itemsChanges[data.id || savedItem.id] = savedItem;
				return this.recalculateInventoryRoomsAfterChangeItem$v1(data.id);
			})
			.then(({rooms, movesizes}) => {
				
				return {
					items: itemsChanges,
					rooms,
					movesizes,
				};
			});
	},
removeRoom$v1:function(roomId) {
	permissions.inventory.isUserCanEditRoom(this.systemData.authorizedUser);

	return this.companyData.entityInstance.save('Room', {removed:1}, roomId)
	.then(removedRoom => {
		return {
			rooms: {
				[roomId]: null
			},
		};
	});
},
removeCategory$v1:function(categoryId) {
	permissions.inventory.isUserCanEditCategory(this.systemData.authorizedUser);

	return this.companyData.entityInstance.save('Category', {removed:1}, categoryId)
		.then(res => {
			return this.cleanRemovedCategoriesFromItemsAndRooms$v1();
		})
		.then(changedItemsAndRooms => {
			return {
				categories: {
					[categoryId]: null
				},
				items: changedItemsAndRooms.items,
				rooms: changedItemsAndRooms.rooms,
			};
		});
},
removeItem$v1:function(itemId) {

	permissions.inventory.isUserCanEditItem(this.systemData.authorizedUser);
	
	let result = {};

	return this.getItem$v1({id: itemId})
		.then(item => {	
			
			return this.companyData.entityInstance.save('Item', {removed:1}, itemId)
				.then(removedItem => {
					result.items = {
						[itemId]: null,
					};
					if (!removedItem.isCustom) {
						return this.cleanRemovedItemsFromRooms$v1();
					}
					return {};
				})
				.then(changedRooms => {
					result.rooms = changedRooms;
					return result;
				});	
										
		});
},
getInventory$v1:function() {
		return Promise.all([
			this.getInventoryRooms$v1({filter: {removed: 0}}),
			this.getInventoryItems$v1({filter: {removed: 0, isCustom: 0}}),
			this.getInventoryCategories$v1({filter: {removed: 0}}),
		])
			.then(([rooms, items, categories]) => {
				return {
					rooms: rooms,
					items: items,
					categories: categories
				};
			})

	},
calculateRoom$v1:function(room) {
	return this.companyData.entityInstance.getEntityMultiply('Item', {
			filter: {
				removed: 0,
				id: Object.keys(room.items).map(a => +a)
			},
			pagination: { itemsOnPage: 999999999 }
		})
		.then(res => {
			room.totalCount = 0;
			room.totalBoxes = 0;
			room.totalWeight = 0;
			room.totalVolume = 0;
			room.extraServices = [];
			room.totalServicesCost = 0;
			room.minCrewSize = 0;
			room.additionalTime = 0;
			
			res.rows.forEach(item => {
			
				let itemCount = room.items[item.id];
			
				if (item.isBox) room.totalBoxes += itemCount;
				room.totalCount += itemCount;
				
				room.totalVolume += +(item.size || 0) * itemCount;
				
				if (item.needService) {
				
					let extraService = _.cloneDeep(item.extraService);
					extraService.count = itemCount;
					extraService.itemId = item.id;
					extraService.totalPrice = extraService.price * extraService.count;
					extraService.totalExtraTime = extraService.extraTime * extraService.count;
					
					room.extraServices.push(extraService);
					
					if (extraService.minCrewSize
						 && extraService.minCrewSize > room.minCrewSize) {
						room.minCrewSize = extraService.minCrewSize
					}
					
					if (item.extraService.extraTime) {
						room.additionalTime += extraService.totalExtraTime;
					}
					
					room.totalServicesCost += extraService.totalPrice;
					
				}
			});
			
			room.totalWeight = this.calculateInventoryWeightFromVolume$v1(room.totalVolume);
			
			return room;
		});	
},
getInventoryRooms$v1:function({filter}) {
	return this.companyData.entityInstance.getEntityMultiply('Room', {
	filter,
	pagination:{itemsOnPage:999999999}})
			.then(res => res.rows);

},
cleanRemovedItemsFromRooms$v1:function() {
	return Promise.all([
		this.companyData.entityInstance.getEntityMultiply('Room', {
			filter: { removed: 0 },
			pagination: { itemsOnPage: 999999999 }
		}),
		this.companyData.entityInstance.getEntityMultiply('Item', {
			filter: { removed: 0 },
			pagination: { itemsOnPage: 999999999 }
		})
	])
		.then(([roomsRes, itemsRes]) => {
		
			let liveItemIds = {};
			itemsRes.rows.forEach(item => liveItemIds[item.id] = true);			
			
			let roomsForUpdating = [];
			
			roomsRes.rows.forEach(room => {
			
				let changed = false;
				
				Object.keys(room.items).forEach(itemId => {
					if (!liveItemIds[itemId]) {
						delete room.items[itemId];
						changed = true;
					}
				});
				
				if (changed) {
					roomsForUpdating.push(room);					
				}			
			});
			
			return Promise
				.all(roomsForUpdating.map(room => this.saveRoom$v1(room)))
				.then(updatedRooms => {
					let result = {};
					updatedRooms.forEach(({rooms}) => Object.assign(result, rooms));
					return result;
				});
	});

},
getItem$v1:function (conditions) {
	return this.companyData.entityInstance.getEntity('Item', conditions);
},
getRoom$v1:function (conditions) {
	return this.companyData.entityInstance.getEntity('Room', conditions);
},
getCategory$v1:function (conditions) {
	return this.companyData.entityInstance.getEntity('Category', conditions);
},
cleanRemovedCategoriesFromItemsAndRooms$v1:function() {
	return Promise.all([
		this.companyData.entityInstance.getEntityMultiply('Room', {
			filter: { removed: 0 },
			pagination:{itemsOnPage:999999999}
		}),
		this.companyData.entityInstance.getEntityMultiply('Item', {
			filter: { removed: 0 },
			pagination: { itemsOnPage: 999999999 }
		}),
		this.companyData.entityInstance.getEntityMultiply('Category', {
			filter:{removed:0},
			pagination:{itemsOnPage:999999999}
		}),
	])
		.then(([roomsRes, itemsRes, categoriesRes]) => {
		
			let categoriesIds = categoriesRes.rows.map(category => category.id);
			
			let promises = [];
			let roomsForUpdate = [];
			
			roomsRes.rows.forEach(room => {

				if (!room.categories) return;
						
				let filteresCategories = _.intersection(room.categories, categoriesIds);

				if (filteresCategories.length !== room.categories.length) {
					room.categories = filteresCategories;
					roomsForUpdate.push(room);
				}
			});
			
			let itemsForUpdate = [];
			
			itemsRes.rows.forEach(item => {
			
				if (!item.categories) return;
			
				let filteresCategories = _.intersection(item.categories, categoriesIds);

				if (filteresCategories.length !== item.categories.length) {
					item.categories = filteresCategories;
					itemsForUpdate.push(item);
				}
			});
			
			let savingRoomsPromise = Promise
				.all(roomsForUpdate.map(room => 
					this.companyData.entityInstance.save('Room', room, room.id)
				))
				.then(updatedRooms => {
					let result = {};
					updatedRooms.forEach((room, index) => result[roomsForUpdate[index].id] = room);
					return result;
				});
				
			let savingItemsPromise = Promise
				.all(itemsForUpdate.map(item => 
					this.companyData.entityInstance.save('Item', item, item.id)
				))
				.then(updatedRooms => {
					let result = {};
					updatedRooms.forEach((room, index) => result[itemsForUpdate[index].id] = room);
					return result;
				});
			
			return Promise.all([savingRoomsPromise, savingItemsPromise])
				.then(([rooms, items]) => ({rooms, items}));
	});
},
calculateInventoryWeightFromVolume$v1:function(volume) {
	return _.round(volume / 7, 1);
},
saveRequestInventoryRoom$v1:function(data) {
	if (data.items) {
		Object.keys(data.items).forEach(key => {
			if (!data.items[key]) delete data.items[key];
		});
		data.itemsIds = Object.keys(data.items).map(a => +a);		
	}

	return this.calculateRoom$v1(data)
		.then(savedRoom => {
			return this.companyData.entityInstance.save('RequestInventoryRoom', savedRoom, data.id)
		});
},
removeRequestInventoryRoom$v1:function(roomId) {
	permissions.inventory.isUserCanEditInventoryRoom(this.systemData.authorizedUser);

	return this.companyData.entityInstance.remove('RequestInventoryRoom', roomId);
},
getRequestRooms$v1:function(requestId) { 
	return this.companyData.entityInstance.getEntityMultiply('RequestInventoryRoom', {
			filter:{requestId},
			pagination:{itemsOnPage:999999999}}
		)
		.then(res => {
			return this.calculateRequestInventoryTotals$v1(res.rows);
		});
},
getInventoryForRequest$v1:function(requestId) {
	/**
	Ok	 * 1. get all actual inventory
	Ok	 * 2. get inventory for request
		 * 2. get removed items which exists in request
		 * 3. get removedRooms which exists in request
		 * 4. get removed categories which exists in removed Items and Rooms
		 */
	
	let actualInventory, requestRooms, removedRooms, removedItems, removedCategories, customItems;
		 
	return Promise.all([
			this.getInventory$v1(),
			this.getRequestRooms$v1(requestId)
		])
		.then(([actualInventoryRes, requestRoomsRes]) => {
		
			actualInventory = actualInventoryRes;
			requestRooms = requestRoomsRes.rooms;
		
			let requestItemsIds = {};
			let requestCategoriesIds = {};
			
			requestRooms.forEach(room => {
				_.keys(room.items).forEach(itemId => requestItemsIds[itemId] = true);
				_.get(room, 'sourceRoom.categories', []).forEach(categoryId => requestCategoriesIds[categoryId] = true);
			});			
			
			requestItemsIds = Object.keys(requestItemsIds).map(id => +id);
			requestCategoriesIds = Object.keys(requestCategoriesIds).map(id => +id);
			
			
			return Promise.all([
				this.getInventoryItems$v1({filter: {removed: 1, id: requestItemsIds}}),
				this.getInventoryItems$v1({filter: {isCustom: 1, id: requestItemsIds}}),				
				this.getInventoryCategories$v1({filter: {removed: 1, id: requestCategoriesIds}})
			]);			
		})
		.then(([removedItemsRes, customItemsRes, removedCategoriesRes]) => {
			removedItems = removedItemsRes;
			customItems = customItemsRes;			
			removedCategories = removedCategoriesRes;
			
			return {
				actualInventory,
				requestRooms,
				removedItems,
				customItems,
				removedCategories,
			};
		});
},
getInventoryItems$v1:function({filter}) {
	return this.companyData.entityInstance.getEntityMultiply('Item', {
		filter,
		pagination:{itemsOnPage:999999999}})
		.then(res => res.rows);
},
getInventoryCategories$v1:function({filter}) {
	return this.companyData.entityInstance.getEntityMultiply('Category', {
		filter,
		pagination:{itemsOnPage:999999999}})
		.then(res => res.rows);
},
calculateRequestInventoryTotals$v1:function (rooms) {
	let totalCount = 0;
	let totalBoxes = 0;
	let totalWeight = 0;
	let totalVolume = 0;
	let extraServices = [];
	let totalServicesCost = 0;
	let minCrewSize = 0;
	let additionalTime = 0;
	
	rooms.forEach(room => {
		totalCount += room.totalCount;
		totalBoxes += room.totalBoxes;
		totalVolume += room.totalVolume;
		
		
		totalServicesCost += room.totalServicesCost;		
		if (minCrewSize < room.minCrewSize) minCrewSize = room.totalCount;		
		additionalTime += room.additionalTime;
		
		room.extraServices.forEach(service => {
			let foundService = _.find(extraServices, {itemId: service.itemId});
			if (foundService) {
				foundService.count += service.count;
			} else {
				extraServices.push(service);
			}
		});

	});
	
	totalWeight = this.calculateInventoryWeightFromVolume$v1(totalVolume);
	
	return {
		rooms,
		totalCount,
		totalWeight,
		totalVolume,
		extraServices,
		totalServicesCost,
		minCrewSize,
		additionalTime
	};
},
getInventoryMovesizeSettings$v1:function () {
	return Promise.all([
		this.getInventoryRooms$v1({filter: {removed: 0}}),
		this.getInventoryMovesizes$v1({filter: {removed: 0}}),
	])
	.then(([rooms, movesizes]) => {
		return {
			rooms,
			movesizes
		};
	});
},
getInventoryMovesizes$v1:function({filter}) {
	return this.companyData.entityInstance.getEntityMultiply('Movesize', {
	filter,
	pagination:{itemsOnPage:999999999}})
			.then(res => {
				return Promise.all(res.rows);
	});
},
calculateMovesize$v1:function (movesize) {
	return this.getInventoryRooms$v1({filter: {id: movesize.defaultRooms}})
		.then(rooms => {
			movesize.itemsCount = rooms.reduce((acc, cur) => {return acc + cur.totalCount}, 0);
		
			movesize.volume = rooms.reduce((acc, cur) => {return acc + cur.totalVolume}, 0);
			movesize.weight = this.doTask('calculateInventoryWeightFromVolume')(movesize.volume);
			movesize.volumeWithDispersion = movesize.volumeWithDispersion || {};
			
			movesize.dispersion = +(movesize.dispersion || 0);
			if (movesize.dispersion > 100) movesize.dispersion = 100;
			if (movesize.dispersion < 0) movesize.dispersion = 0;
			
			movesize.volumeWithDispersion.min = _.round(movesize.volume - movesize.volume * (movesize.dispersion / 100));
			movesize.volumeWithDispersion.max = _.round(movesize.volume + movesize.volume * (movesize.dispersion / 100));
			
			return movesize;
		});
},
saveMovesize$v1:function(data) {
	permissions.inventory.isUserCanEditMoveSize(this.systemData.authorizedUser);

	return this.calculateMovesize$v1(data)
		.then(calculated => {
			return this.companyData.entityInstance.save('Movesize', calculated, data.id)
		})
		.then(saved => {
			return {
				movesizes: {
					[data.id]: saved,
				}
			};
		});

},
removeMovesize$v1:function(movesizeId) {
	
	permissions.inventory.isUserCanEditMoveSize(this.systemData.authorizedUser);

	return this.getInventoryMovesize$v1({id: movesizeId})
		.then(movesize => {			
			return this.companyData.entityInstance.save('Movesize', {removed:1}, movesizeId)
		})
		.then(saved => {
			return {
				movesizes: {
					[movesizeId]: null,
				}
			};
		});
},
getInventoryMovesize$v1:function (conditions) {
	return this.companyData.entityInstance.getEntity('Movesize', conditions);
},
recalculateMovesizesAfterChangeRoom$v1:function(roomId) {
	let oldMovesizes, newMovesizes;
	return this.getInventoryMovesizes$v1({filter: {
			removed: 0,
			defaultRooms: roomId
		}})
		.then(_oldMovesizes => {
			oldMovesizes = _oldMovesizes;
			return Promise.all(oldMovesizes.map(movesize => 
				this.saveMovesize$v1( _.cloneDeep(movesize) )));
		})
		.then(_newMovesizes => {
			
			let changes = {};
			_newMovesizes.forEach((newMovesize, index) => {
				let movesize = newMovesize.movesizes[Object.keys(newMovesize.movesizes)[0]];
				delete movesize.ELID;
				delete movesize.updatedAt;
				delete movesize.createdAt;
				delete oldMovesizes[index].ELID;
				delete oldMovesizes[index].updatedAt;
				delete oldMovesizes[index].createdAt;
				
				let isChanged = Object.keys(movesize).some(key => !_.isEqual(oldMovesizes[index][key], movesize[key]));
				
				if (isChanged) {
					changes[oldMovesizes[index].id] = movesize;
				}
			});
			return changes;
		})
},
recalculateInventoryRoomsAfterChangeItem$v1:function(itemId) {
	let oldRooms, newRooms;
	return this.getInventoryRooms$v1({filter: {
			removed: 0,
			itemsIds: itemId
		}})
		.then(_oldRooms => {
			oldRooms = _oldRooms;
			return Promise.all(oldRooms.map(room => this.saveRoom$v1(room)));
		})
		.then((changes) => {
			
			let roomsChanges = {};
			let movesizesChanges = {};
			changes.forEach(({rooms, movesizes}, index) => {
				Object.assign(roomsChanges, rooms);
				Object.assign(movesizesChanges, movesizes);
			});
			return {rooms: roomsChanges, movesizes: movesizesChanges};
		})
},

};