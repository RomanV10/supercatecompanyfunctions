module.exports = {
entitiesStructure$v1:function () {
  return [
    {
      name: "Template",
      fields: {
        body: {array: false, name: "body", type: "longtext"},
        name: {array: false, name: "name", type: "string"},
        subject: {array: false, name: "subject", type: "string"},
        destination: {array: false, name: "destination", type: "json"},
        event: {array: false, name: "event", type: "string"},
      }
    },
    {
      name: 'TemplateFolder',
      fields: {
        name: {array: false, name: "name", type: "string"},
        templates: {array: false, name: "templates", type: "json"},
      },
    },
    {
      name: 'Event',
      fields: {
        name: {array: false, type: "string"},
        description: {array: false, type: "string"},
        enabled: {array: false, type: "integer"},
        deleted: {array: false, type: "integer"},
      },
    },
      {
          name: 'WasSent',
          fields: {
              requestId: {array: false, type: "integer"},
              email: {array: false, type: "string"},
              event: {array: false, type: "string"},
			  readed: {array: false, type: "integer", defaultValue:0}
		  },
      },
  ];
},
eventsList$v1:function () {
  return [
    {
        key: 'requestCreated',
        name: 'Order Created',
	description: 'Order Created',	
    },
    {
        key: 'requestNotPaid',
        name: 'Order Not Paid',
	description: 'Order Created but not paid',
     },
     {
        key: 'requestPaid',
	name: 'Order is paid',
	description: 'Order Paid and confirmed',
     },     
     {
        key: 'deliveryReminder5',
	name: 'Delivery Reminder 5 Days',
	description: 'Reminder about delivery',
     },
     {
        key: 'deliveryReminder2',
	name: 'Delivery Reminder Tomorrow',
	description: 'Reminder about delivery',
     },
     {
        key: 'pickupReminder1',
	name: 'Pickup Reminder',
	description: 'Reminder about pickup',
     },
     {
        key: 'pickupReminder5',
	name: 'Pickup Reminder 5 Days',
	description: 'Reminder about pickup',
     },
	  {
		  key: 'managerAddMessage',
		  name: 'Admin/Manager add message',
		  description: 'New message was added by manager or admin',
	  },
	  {
		  key: 'clientAddMessage',
		  name: 'Client add message',
		  description: 'New message was added by client',
	  },
	  {
		  key: 'restorePassword',
		  name: 'User restore password',
		  description: 'User restore password',
	  },
	  {
			key: 'invoiceForClient',
			name: 'Sending invoice to client',
		 	description: 'Sending invoice to client',
	  },
	  {
			key: 'receiptCreated',
			name: 'Sending receipt to client when receipt created',
		 	description: 'Sending receipt to client when receipt created',
	  },
	  {
		  key: 'bookedReminderDay1',
		  name: 'Sending email after success booked one day',
		  description: 'Sending email after success booked one day',
	  },
	  {
		  key: 'bookedReminderDay3',
		  name: 'Sending email after success booked tree days',
		  description: 'Sending email after success booked tree days',
	  },
	  {
		  key: 'bookedReminderDay5',
		  name: 'Sending email after success booked five days',
		  description: 'Sending email after success booked five days',
	  },
	  {
		  key: 'bookedReminderDay7',
		  name: 'Sending email after success booked seven days',
		  description: 'Sending email after success booked seven days',
	  },
	  {
		  key: 'receiptSuccessAutoCharge',
		  name: 'Sending receipt to client when receipt created with autocharge',
		  description: 'Sending receipt to client when receipt created with autocharge',
	  },
	  {
		  key: 'deliveryReminderAfter1',
		  name: 'Sending email after one day off delivery date',
		  description: 'Sending email after one day off delivery date',
	  },
	  {
		  key: 'monthStatistic',
		  name: 'Sending email with previous month statistic',
		  description: 'Sending email with previous month statistic',
	  },
	  {
		  key: 'yearStatistic',
		  name: 'Sending email with previous year statistic',
		  description: 'Sending email with previous year statistic',
	  },
   ];
},
getTemplatesSettings$v1:function (data) {
    let promises = [];
    promises.push(this.getManyEntities$v1({name: 'Template'}));
    promises.push(this.getManyEntities$v1({name: 'TemplateFolder'}));
    //promises.push(this.getManyEntities$v1({name: 'Event'}));
    return Promise.all(promises)
        .then(res => (
                {
                    templates: res[0].rows,
                    folders: res[1].rows,
                    events: this.eventsList$v1(),
                    tokens: this.tokensList$v1(),
                }
            )
        );
},
getManyEntities$v1:function (data) {return this.companyData.entityInstance.getEntityMultiply(data.name, data.conditions);},
sendEventEmails$v1:function ({eventName, parserData, source, smtpSettings, tokenParams, emailDevSettings}) {
    return this.getManyEntities$v1({name: 'Template', conditions: {filter: {event: eventName}}})
        .then(templates => {
          return this.parseTemplates$v1({templates: templates.rows, parserData, tokenParams})
        })
        .then(parsedTemplates => {
          let promises = parsedTemplates.map(template => this.companyData.emailerInstance.send(template.destination, template, source, smtpSettings, emailDevSettings).then(mailerRes => ({mailerRes, template})));
            return Promise.all(promises);
        });
},
tokensList$v1:function () {
	return {
		'company.companyInfo.companyName': {
			'name': 'Company Name',
		},
		'company.companyInfo.companyPhone': {
			'name': 'Company Phone',
		},
		'company.companyInfo.companyEmail': {
			'name': 'Company Email',
		},
		'company.companyInfo.companySite': {
			'name': 'Company Site (url)',
			callback: ({parserData}) => `${_.get(parserData.company, 'companyInfo.companySite', 'no_company_site')}`,
		},
		'company.companyInfo.companyAutologin': {
			name: 'Autologin',
			callback: ({parserData, companyData}) => `https://${companyData.hosts[0]}/api/auth/hashLogin?hash=${parserData.request.client.hashLogin}&redirect=account&requestId=${parserData.request.id}`,
		},
		'company.companyInfo.companyAdminAutologin': {
			name: 'Manager autologin',
			callback: ({parserData, companyData}) => `https://${companyData.hosts[0]}/api/auth/hashLogin?hash=${parserData.request.manager.hashLogin}&redirect=dashboard`,
		},
		'company.companyLogoUrl': {
			name: 'Company Logo Url',
			callback: ({parserData, companyData}) => `https://${companyData.hosts[0]}${parserData.company.companyLogoUrl.url}`,
		},
		'request.deliveryAddress': {
			name: 'Delivery address',
			callback: ({parserData}) => {
				let result = '';
				if (parserData.request.deliveryAddress.address) result += parserData.request.deliveryAddress.address + ', ';
				if (parserData.request.deliveryAddress.city) result += parserData.request.deliveryAddress.city + ', ';
				if (parserData.request.deliveryAddress.state) result += parserData.request.deliveryAddress.state + ' ';
				if (parserData.request.deliveryAddress.postal) result += parserData.request.deliveryAddress.postal + ' ';
				return result;
			},
		},
		'request.deliveryAddress.apt': {
			name: 'Delivery apt',
			callback: ({parserData}) => parserData.request.deliveryAddress.apt || '',
		},
		'request.pickupAddress': {
			name: 'Pickup address',
			callback: ({parserData}) => {
				let result = '';
				if (parserData.request.pickupAddress.address) result += parserData.request.pickupAddress.address + ', ';
				if (parserData.request.pickupAddress.city) result += parserData.request.pickupAddress.city + ', ';
				if (parserData.request.pickupAddress.state) result += parserData.request.pickupAddress.state + ' ';
				if (parserData.request.pickupAddress.postal) result += parserData.request.pickupAddress.postal + ' ';
				return result;
			},
		},
		'request.pickupAddress.apt': {
			name: 'Pickup apt',
			callback: ({parserData}) => parserData.request.pickupAddress.apt || '',
		},

		'request.deliveryTimeWindow': {
			name: 'Delivery time window',
			callback: ({parserData}) => `${
				moment.unix(parserData.request.deliveryTimeWindowStart).tz(this.companyData.timeZone.name).format('hh:mm A')} - ${
				moment.unix(parserData.request.deliveryTimeWindowEnd).tz(this.companyData.timeZone.name).format('hh:mm A')}`,
		},
		'request.pickupTimeWindow': {
			name: 'Pickup time window',
			callback: ({parserData}) => `${
				moment.unix(parserData.request.pickupTimeWindowStart).tz(this.companyData.timeZone.name).format('hh:mm A')} - ${
				moment.unix(parserData.request.pickupTimeWindowEnd).tz(this.companyData.timeZone.name).format('hh:mm A')}`,
		},
		'request.packageOverview': {
			name: 'Package overview',
			callback: ({parserData}) => {
				return `<table class="Template__RequestOverview" style="border-collapse:collapse;border-spacing:0px;width:100%">
						<tbody>
						<tr>
						<td width="80%" align="left">
					 <div class="Template__RequestOverview__row">
						 ${parserData.request.packages.mainPackage
	? `${parserData.request.packages.mainPackage.name} ${parserData.request.packages.mainPackage.size} bins`
	: `no package`}
					 </div>
					 ${parserData.request.packages.mainPackage
					 ? `<div class="Template__RequestOverview__row">
						 ${parserData.request.packages.mainPackage.rollingCarts} Dollys
					 </div>
					 <div class="Template__RequestOverview__row">
						 ${parserData.request.packages.mainPackage.usagePeriod} Weeks of usage
					 </div>` : ''}
					 </td>		
					 <td width="20%" align="right">${this.currencyFormatter$v1(_.get(parserData.request.packages, 'mainPackage.rate', 0))}
					 
</td>	
					 </tr>
					 </tbody>
				 </table>`;
			},
		},
		'request.taxes': {
			name: 'Taxes overview',
			callback: ({parserData}) => {
				return `<table class="Template__RequestOverview" style="border-collapse:collapse;border-spacing:0px;width:100%">
						<tbody>
						<tr>
						<td width="80%" align="left">
						 <div class="Template__RequestOverview__row">
							 Taxes:
						 </div>
					 	</td>		
					 		<td width="20%" align="right">${this.currencyFormatter$v1(_.get(parserData, 'request.taxes.total', 0))}</td>	
					 </tr>
					 </tbody>
				 </table>`;
			},
		},
		'request.additionalOverview': {
			name: 'Additional Overview',
			callback: ({parserData}) => {
				return `<div class="Template__RequestOverview">				
					<table style="border-collapse: collapse;border-spacing: 0px;width: 100%;">
						<tbody>
						<tr class="Template__RequestOverview__row">
						<td class="Template__RequestOverview__column" width="80%" align="left">
							Extra Week
						</td>
						<td class="Template__RequestOverview__column" width="20%" align="right">
							${this.currencyFormatter$v1(_.get(parserData.request.packages, 'mainPackage.extraUsage', 0))}
						</td>
					</tr>
					${_.get(parserData.request, 'packages.additionalPackages', []).map((packageInfo, index) =>
					`<tr class="Template__RequestOverview__row">
						<td class="Template__RequestOverview__column" width="80%" align="left">
							${packageInfo.name} (${packageInfo.quantity})
						</td>
						<td class="Template__RequestOverview__column" width="20%" align="right">
							${this.currencyFormatter$v1(packageInfo.total)}
						</td>
					</tr>`).join('')}
						</tbody>
					</table>
				</div>`;
			},
		},
		'request.servicesOverview': {
			name: 'Services Overview',
			callback: ({parserData}) => {
				return `<table class="Template__RequestOverview" style="border-collapse: collapse;border-spacing: 0px;width: 100%;">
					<tbody>
					${_.get(parserData.request, 'services.autoServices', []).map((serviceInfo, index) =>
					!serviceInfo.disabled ?
						`<tr class="Template__RequestOverview__row">
						<td class="Template__RequestOverview__column" width="80%" align="left">
							${serviceInfo.visibleName || serviceInfo.name}
						</td>
						<td class="Template__RequestOverview__column" width="20%" align="right">
							${this.currencyFormatter$v1(serviceInfo.total)}
						</td>
					</tr>` : null
				).join('')}
					${_.get(parserData.request, 'services.customServices', []).map((serviceInfo, index) =>
					`<tr class="Template__RequestOverview__row">
						<td class="Template__RequestOverview__column"  width="80%" align="left">
							${serviceInfo.name} (${serviceInfo.quantity})
						</td>
						<td class="Template__RequestOverview__column" width="20%" align="right">
							${this.currencyFormatter$v1(serviceInfo.total)}
						</td>
					</tr>`
				).join('')}		
					</tbody>	
			</table>`;
			},
		},
		'request.receiptsDetails': {
			name: 'Receipts details',
			callback: ({parserData}) => {
				return `<div class="Template__Payments">
					${_.get(parserData.request, 'payments.rows', []).map(payment =>
						`<div class="Template__Payments__payment">
							<div class="Template__Payments__payment__info">
								<div class="Template__Payments__payment__info__row">
									Payment for Order #${payment.transactionId}
								</div>
								<div class="Template__Payments__payment__info__row">
									${moment.unix(payment.createdAt).tz(this.companyData.timeZone.name).format('MMM DD, YYYY HH:mmA')}
								</div>
							</div>
							<div class="Template__Payments__payment__value">
								${this.currencyFormatter$v1(payment.amount)}
							</div>
						</div>`).join('')}
				</div>`;
			},
		},		

		'request.client.email': {
			'name': 'Client Email'
		},
		'request.client.phone': {
			'name': 'Client Phone Number',
		},
		'request.manager.email': {
			'name': 'Manager full name'
		},
		'request.client.fullName': {
			'name': 'Client full name',
			callback:  ({parserData}) => `${parserData.request.client.firstName} ${parserData.request.client.lastName}`
		},
		'request.client.firstName': {
			'name': 'Client first name',
		},
		'request.client.newPassword': {
			'name': 'New password for restore',
		},
		'request.pickupDate': {
			'name': 'Pickup date',
			callback: ({parserData}) => moment.unix(parserData.request.pickupDate).tz(this.companyData.timeZone.name).format('MMMM D, YYYY')
		},
		'request.pickupDateMonth': {
			'name': 'Pickup date (month)',
			callback: ({parserData}) => moment.unix(parserData.request.pickupDate).tz(this.companyData.timeZone.name).format('MMM')
		},
		'request.pickupDateDay': {
			'name': 'Pickup date (Day)',
			callback: ({parserData}) => moment.unix(parserData.request.pickupDate).tz(this.companyData.timeZone.name).format('DD')
		},
		'request.deliveryDate': {
			'name': 'Delivery date',
			callback: ({parserData}) => moment.unix(parserData.request.deliveryDate).tz(this.companyData.timeZone.name).format('MMMM D, YYYY')
		},
		'request.deliveryDateMonth': {
			'name': 'Delivery date (month)',
			callback: ({parserData}) => moment.unix(parserData.request.deliveryDate).tz(this.companyData.timeZone.name).format('MMM')
		},
		'request.deliveryDateDay': {
			'name': 'Delivery date (Day)',
			callback: ({parserData}) => moment.unix(parserData.request.deliveryDate).tz(this.companyData.timeZone.name).format('DD')
		},
		'request.packages.mainPackage.basicUsage': {
			'name': 'Main Package Total',
			callback: ({parserData}) => _.get(parserData.request.packages, 'mainPackage.basicUsage', 0),
		},
		'request.packages.additionalTotal': {
			'name': 'Package Additionals',
			callback: ({parserData}) => parserData.request.packages.total
								- _.get(parserData.request.packages, 'mainPackage.basicUsage', 0).toFixed(2),
		},
		'request.services.total': {
			'name': 'Extra Services total',
		},
		'request.discount.total': {
			'name': 'Discount total',
		},
		'request.discountTable': {
			name: 'Discount Table',
			callback: ({parserData}) => {
				return `<table class="Template__RequestOverview" style="border-collapse:collapse;border-spacing:0px;width:100%">
						<tbody>
						<tr>
						<td width="80%" align="left">
					  Discount
					 </td>		
					 <td width="20%" align="right">${this.currencyFormatter$v1(parserData.request.discount.total)}
					 
</td>	
					 </tr>
					 </tbody>
				 </table>`;
			},
		},
		'request.grandTotal': {
			name: 'Grand Total Table',
			callback: ({parserData}) => {
				return `<table class="Template__RequestOverview" style="border-collapse:collapse;border-spacing:0px;width:100%">
						<tbody>
						<tr>
						<td width="80%" align="left">
					  Total
					 </td>		
					 <td width="20%" align="right">${this.currencyFormatter$v1(parserData.request.totalWithDiscount)}
					 
</td>	
					 </tr>
					 </tbody>
				 </table>`;
			},
		},
		'request.totalWithDiscount': {
			'name': 'Total with discount',
		},
		'request.payments': {
			'name': 'Request payments',
			'callback': ({parserData, tokenParams = {}}) => {
			  let receipts = _.get(parserData.request, 'payments.rows', []);
			  let type = null;

			   if (tokenParams['request.payments']) {
				  receipts = _.filter(receipts, {id:tokenParams['request.payments'].id});
				  type = tokenParams['request.payments'].type;
			 	}
				let total = _.sumBy(receipts, 'amount');

				return `<div>
				
						${receipts.map(receipt =>
						`<table width="100%">
						<tr>
						   <td width="40%" align="left">
								  ID:
								 </td>
								<td width="60%" align="left">
							  ${receipt.id}
							 </td>
						 </tr>
						 ${type
							? `<tr>
									<td align="left">
								  Type:
								 </td>
								<td align="left">
									   ${type}
									 </td>
									</tr>`
							: ''
						}
						<tr>
							<td align="left">
								  Date:
								 </td>
								<td  align="left">
							  ${moment.unix(receipt.createdAt).tz(this.companyData.timeZone.name).format('DD MMM, YYYY h:mm a')}
							 </td>
						</tr>
					    ${_.get(receipt, 'data.user') 
								? `<tr>
									<td align="left">
								  Name:
								 </td>
								<td width="20%" align="left">
									   ${receipt.data.user.firstName} ${receipt.data.user.lastName}
									 </td>
									</tr>`  
								: ''
						}
						 ${_.get(receipt, 'data.user.phone')
							? `<tr>
								<td align="left">
								  Phone:
								 </td>
								<td align="left">
								  ${receipt.data.user.phone}
								 </td>
								</tr>`
							: ''
						}
						${_.get(receipt, 'data.user.email')
							? `<tr>
								<td align="left">
								  Email:
								 </td>
								<td align="left">
								  ${receipt.data.user.email}
								 </td>
							</tr>`
							: ''
						}
							<tr>
								<td align="left">
								  Amount:
								 </td>
								<td align="left">
								  ${receipt.amount}$
								 <d>
							</tr>
						${_.get(receipt, 'data.cardInfo.cardType')
								? `<tr>
									<td align="left">
								  Card type:
								 </td>
								<td align="left">
									  ${receipt.data.cardInfo.cardType}
									 </td>
								</tr>`
								: ''
						}
						${_.get(receipt, 'data.cardInfo.numberSuffix')
							? `<tr>
								<td align="left">
								  Card number:
								 </td>
								<td align="left">
								  ****${receipt.data.cardInfo.numberSuffix}
								 </td>
							</tr>`
							: ''
						}				
						</table>
`
						).join('')}
				 </div>`;
			}
		},
		'request.id': {
			name: 'Request id',
		},
		'message.body': {
			'name': 'Message body',
		},
		'statistic.lastYearsRequests': {
			'name': 'Request count from last year',
		},
		'statistic.currentYearRequests': {
			'name': 'Request count from current year',
		},
		'statistic.lastYearReceipts.total': {
			'name': 'Request receipts from last year',
			callback:({parserData, params}) => {
				return this.currencyFormatter$v1(_.get(parserData, 'statistic.lastYearReceipts.total', 0))

			}
		},
		'statistic.currentYearReceipts.total': {
			'name': 'Request receipts from current year',
			callback:({parserData, params}) => {
				return this.currencyFormatter$v1(_.get(parserData, 'statistic.currentYearReceipts.total', 0))


			}
		},
		'statistic.requestsCountDiff': {
			'name': 'Difference between requests count',
			callback:({parserData, params}) => {
				let result = _.get(parserData, 'statistic.currentYearRequests', 0) - _.get(parserData, 'statistic.lastYearsRequests', 0);
				if (result > 0) {
					result = `+${result}`;
				} else if(result < 0) {
					result = `-${result}`;
				}

				return result;
			}
		},
		'statistic.receiptsTotalDiff': {
			'name': 'Difference between receipts total',
			callback:({parserData, params}) => {
				let result =  _.get(parserData, 'statistic.currentYearReceipts.total', 0) - _.get(parserData, 'statistic.lastYearReceipts.total', 0);
				if (result > 0) {
					result = `+${this.currencyFormatter$v1(result)}`;
				} else if(result < 0) {
					result = `-${this.currencyFormatter$v1(result)}`;
				}

				return result;

			}
		},

	};
},
tokensCallbacks$v1:function () {
	 return {
        'client.fullName': function (data) {
            return `${data.request.client.firstName} ${data.request.client.lastName}`;
        }
    }
},
saveEntity$v1:function (data) {
    return this.companyData.entityInstance.save(data.entityName, data.entityData, data.entityData.id);
},
parseAndSendTemplates$v1:function ({templates, parserData, source, tokenParams}) {
    let promises = [];
    templates.map(template => {
        template.body = this.companyData.tokenInstance.findAndReplace(template.body, parserData, tokenParams);
        template.subject = this.companyData.tokenInstance.findAndReplace(template.subject, parserData, tokenParams);
        template.destination = this.companyData.tokenInstance.findAndReplace(template.destination.join(','), parserData, tokenParams);
        template.requestId = _.get(parserData, 'request.id');
        promises.push(this.companyData.emailerInstance.send(template.destination, template, source));
    });
    return Promise.all(promises);
},
removeEntity$v1:function (data) {
    return this.companyData.entityInstance.remove(data.entityName, data.entityData.id);
},
parseTemplates$v1:function ({templates, parserData, tokenParams}) {
	return templates.map(template => ({
		body: this.companyData.tokenInstance.findAndReplace(template.body, parserData, tokenParams),
		subject: this.companyData.tokenInstance.findAndReplace(template.subject, parserData, tokenParams),
		destination: template.destination ? this.companyData.tokenInstance.findAndReplace(template.destination.join(','), parserData, tokenParams) : 'test@test.com',
        event:template.event,
        requestId: _.get(parserData, 'request.id')
    }));
},
sanitizeTemplate$v1:function (template) {
  let templateGOST = {};
  templateGOST.body = '' + template.body || '';
  templateGOST.destination = template.destination || [];
  templateGOST.subject = '' + template.subject || '';
  templateGOST.name = '' + template.name || '';
  templateGOST.event = '' + template.event || '';
  return templateGOST;
},
sendTemplates$v1:function ({templates, parserData, source = 'test source', destination, smtpSettings, tokenParams, emailDevSettings}) {
	let parsedDestination = this.companyData.tokenInstance.findAndReplace(destination.join(','), parserData);
	return this.normalizeTemplatesArray$v1(templates)
		.then(templates => this.parseTemplates$v1({templates, parserData, tokenParams}))
		.then(parsedTemplates => Promise.all(
			parsedTemplates.map(template => this.companyData.emailerInstance.send(parsedDestination, template, source, smtpSettings, emailDevSettings).then(mailerRes => ({mailerRes, template})))
		));
},
normalizeTemplatesArray$v1:function (templates) {
    let templatesFromFront = templates
        .filter(template => _.isObject(template))
        .map(this.sanitizeTemplate$v1);

    let templatesFromDB = templates
        .filter(template => _.isNumber(template));

    return this.getManyEntities$v1({name: 'Template', conditions: { filter: {id: templatesFromDB}}})
    .then(templatesFromDB => templatesFromDB.rows.concat(templatesFromFront));
},
getSendedEmails$v1:function (condition) {
        return this.getManyEntities$v1({name: 'WasSent', conditions: { filter: condition}});
    },
removeSendedEmailLog$v1:function (condition) {
    return this.companyData.entityInstance.removeBy('WasSent', condition);
},
currencyFormatter$v1:function (value) {
	if (isNaN(value)) value = 0;
	return `$${(+value || 0).toFixed(2)}`;
},
sendDirectEmail$v1:function ({destination, body, source, smtpSettings}) {
    return this.companyData.emailerInstance.send(destination, body, source, smtpSettings)
},
wasRead$v1:function (mailId) {
		return this.companyData.emailerInstance.wasRead(mailId);
	},
getEmail$v1:function (data) {
		return this.companyData.emailerInstance.getEmail(data);
	},

};