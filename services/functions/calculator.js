module.exports = {
calculateCrates$v1:function({ request, calculatorSettings, constantsSettings }) {
    delete request.total;
    delete request.totalWithDiscount;
    delete request.balance;

    this.calculatePackages$v1({
      request,
      calculatorSettings
    });
    this.calculateServices$v1({
      request,
      calculatorSettings
    });
    this.calculateTaxes$v1({
        request,
        calculatorSettings,
        constantsSettings
    });
    this.calculateTotal$v1({
      request,
      calculatorSettings
    });
    this.calculateDiscount$v1({
      request,
      calculatorSettings
    });
    this.calculatePayments$v1({
      request,
      calculatorSettings
    });
    this.calculateBalance$v1({
      request,
      calculatorSettings
    });
    return request;
  },
calculateServices$v1:function({ request, calculatorSettings }) {

    request.services = request.services || {};
    request.services.customServices = request.services.customServices || [];
    let oldAutoServices = request.services.autoServices;
    request.services.autoServices = [];

    request.services.total = request.services.customServices.reduce((sum, serviceInfo) => {
      serviceInfo.rate = serviceInfo.rate || 0,
        serviceInfo.quantity = serviceInfo.quantity || 0,
        serviceInfo.total = serviceInfo.rate * serviceInfo.quantity;
      serviceInfo.total = this.formatCost$v1(serviceInfo.total);
      sum += serviceInfo.total;
      sum = this.formatCost$v1(sum);
      return sum;
    }, 0);
    request.services.total = this.formatCost$v1(request.services.total);

    calculateAutoServices.call(this);

    request.services.total += request.services.autoServices.reduce((sum, serviceInfo) => {

      serviceInfo.rate = serviceInfo.rate || 0;
      serviceInfo.quantity = serviceInfo.quantity || 0;
      serviceInfo.total = serviceInfo.disabled ? 0 : (serviceInfo.rate * serviceInfo.quantity);
      serviceInfo.total = this.formatCost$v1(serviceInfo.total);
      sum += serviceInfo.total;
      sum = this.formatCost$v1(sum);
      return sum;
    }, 0);
    request.services.total = this.formatCost$v1(request.services.total);


    return request;

    function calculateAutoServices() {
      calculateFloorService.call(this);
      calculateDistanceService.call(this);
      request.services.autoServices.forEach(service => {
        let foundOld = _.find(oldAutoServices, { name: service.name });
        if (foundOld) service.disabled = !!foundOld.disabled;
      });
    }

    function calculateFloorService() {
      const CONST = this.doTask("constantine")();

      request.deliveryAddress = request.deliveryAddress || {};
      let deliveryFloor = request.deliveryAddress.floor || 0;
      let deliveryRate = +_.get(calculatorSettings, `servicesInfo.systemServices.floorService[${deliveryFloor}]`, 0);
      if (deliveryRate > 0) {

        request.services.autoServices.push({
          name: CONST.AUTO_SERVICES_NAMES.DELIVERY_FLOOR,
          visibleName: "Delivery walkup fee",
          rate: deliveryRate,
          quantity: 1
        });
      }
      request.pickupAddress = request.pickupAddress || {};
      let pickupFloor = request.pickupAddress.floor || 0;
      
      let pickupRate = +_.get(calculatorSettings, `servicesInfo.systemServices.floorService[${pickupFloor}]`, 0);
      if (pickupRate > 0) {
        request.services.autoServices.push({
          name: CONST.AUTO_SERVICES_NAMES.PICKUP_FLOOR,
          visibleName: "Pickup walkup fee",
          rate: pickupRate,
          quantity: 1
        });
      }
    }

  function calculateDistanceService() {
    const CONST = this.doTask("constantine")();

    request.deliveryAddress = request.deliveryAddress || {};
    request.pickupAddress = request.pickupAddress || {};

    let deliveryCost = _.get(request, "location.delivery.extraServiceCharge", 0);
    if (deliveryCost) {
      request.services.autoServices.push({
        name: CONST.AUTO_SERVICES_NAMES.DELIVERY_DISTANCE,
        visibleName: _.get(request, "location.delivery.extraServiceName", CONST.AUTO_SERVICES_NAMES.DELIVERY_DISTANCE),
        rate: deliveryCost,
        quantity: 1
      });
    }

    let pickupCost = _.get(request, "location.pickup.extraServiceCharge", 0);
    if (pickupCost) {
      request.services.autoServices.push({
        name: CONST.AUTO_SERVICES_NAMES.PICKUP_DISTANCE,
        visibleName: _.get(request, "location.pickup.extraServiceName", CONST.AUTO_SERVICES_NAMES.DELIVERY_DISTANCE),
        rate: pickupCost,
        quantity: 1
      });
    }
  }
  },
calculateDiscount$v1:function({ request, calculatorSettings }) {
    const VALUE_DISCOUNT = 0;
    const PERCENT_DISCOUNT = 1;

    request.discount = request.discount || {};
    request.discount.type = request.discount.type || VALUE_DISCOUNT;


    if (request.discount.promoCode) {
      const promoCodesSettings = _.get(calculatorSettings, "discountSettings.promoCodes", []);

      let foundPromo = _.find(promoCodesSettings,
        { name: request.discount.promoCode });
      if (foundPromo) {
        request.discount.type = foundPromo.type;
        request.discount.value = foundPromo.value;
        request.discount.percent = foundPromo.percent;
      }
    }

    if (request.discount.type === VALUE_DISCOUNT) {
      request.discount.total = request.discount.value || 0;
    } else if (request.discount.type === PERCENT_DISCOUNT) {
      request.discount.total = Math.round((request.discount.percent || 0) * request.total) / 100;
      request.discount.total = this.formatCost$v1(request.discount.total);
    }
    request.totalWithDiscount = request.total - request.discount.total;
    request.totalWithDiscount = this.formatCost$v1(request.totalWithDiscount);
  },
calculateDiscount$moving_v1:function({ request, calculatorSettings }) {
    const VALUE_DISCOUNT = 0;
    const PERCENT_DISCOUNT = 1;

    request.discount = request.discount || {};
    request.discount.type = request.discount.type || VALUE_DISCOUNT;

    if (this.systemData.source !== "dashboard") {
      request.discount.total = 0;
      request.discount.value = 0;
      request.discount.percent = 0;
    }

    if (request.discount.promoCode) {
      const promoCodesSettings = _.get(calculatorSettings, "discountSettings.promoCodes", []);

      let foundPromo = _.find(promoCodesSettings,
        { name: request.discount.promoCode });
      if (foundPromo) {
        request.discount.type = foundPromo.type;
        request.discount.value = foundPromo.value;
        request.discount.percent = foundPromo.percent;
      }
    }

    if (request.discount.type === VALUE_DISCOUNT) {
      request.discount.total = request.discount.value || 0;
    } else if (request.discount.type === PERCENT_DISCOUNT) {
      request.discount.total = Math.round((request.discount.percent || 0) * request.total) / 100;
      request.discount.total = this.formatCost$v1(request.discount.total);
    }
  },
calculateBalance$v1:function({ request, calculatorSettings }) {
    request.balance = request.totalWithDiscount - _.get(request, "payments.total", 0);
    request.balance = this.formatCost$v1(request.balance);
  },
calculatePayments$v1:function({ request, calculatorSettings }) {
	return;
    request.payments = request.payments || {};
    request.payments.rows = request.payments.rows || [];
    request.payments.total = request.payments.rows.reduce(
      (sum, row) => {
        let amount = +(row.amount || 0);
        let value = sum + amount - _.sumBy((row.refunds || []), "amount");
        return this.formatCost$v1(value);
      },
      0);
  },
calculateTotal$v1:function({ request, calculatorSettings }) {
    request.total = request.packages.total + request.services.total + request.taxes.total;
    request.total = this.formatCost$v1(request.total);
  },
calculateCustomServices$v1:function({ request, calculatorSettings }) {
    request.services = request.services || {};
    request.services.customServices = request.services.customServices || [];
    request.services.autoServices = request.services.autoServices || [];

    request.services.total = request.services.customServices.reduce((sum, serviceInfo) => {
      serviceInfo.rate = serviceInfo.rate || 0;
      serviceInfo.quantity = serviceInfo.quantity || 0;
      serviceInfo.total = serviceInfo.rate * serviceInfo.quantity;
      serviceInfo.total = this.formatCost$v1(serviceInfo.total);
      sum += serviceInfo.total;
      sum = this.formatCost$v1(sum);
      return sum;
    }, 0);
    request.services.total = this.formatCost$v1(request.services.total);

    request.services.total += request.services.autoServices.reduce((sum, serviceInfo) => {
      serviceInfo.rate = serviceInfo.rate || 0;
      serviceInfo.quantity = serviceInfo.quantity || 0;
      serviceInfo.total = serviceInfo.disabled ? 0 : (serviceInfo.rate * serviceInfo.quantity);
      serviceInfo.total = this.formatCost$v1(serviceInfo.total);
      sum += serviceInfo.total;
      sum = this.formatCost$v1(sum);
      return sum;
    }, 0);
    request.services.total = this.formatCost$v1(request.services.total);

    return request;
  },
calculatePaymentsWithDerivations$v1:function({ request, calculatorSettings }) {
    this.calculatePayments$v1({
      request,
      calculatorSettings
    });
    this.calculateBalance$v1({
      request,
      calculatorSettings
    });
    return request;
  },
formatCost$v1:function(value) {
    return +(value.toFixed(2));
  },
calculatePackages$v1:function({ request, calculatorSettings }) {
    const ONE_DAY = 86400;
    let startTime = (request.deliveryDate || Math.floor(Date.now() / 1000)); //unix time
    let endTime = request.pickupDate || Math.floor(Date.now() / 1000); //unix time
    let diffInWeeks = (endTime - startTime) / 604800;
    if (diffInWeeks - Math.floor(diffInWeeks)) {
      diffInWeeks = Math.floor(diffInWeeks) + 1;
    } else {
      diffInWeeks = Math.floor(diffInWeeks);
    }

    request.packages = request.packages || {};
    request.packages.additionalPackages = request.packages.additionalPackages || [];
    request.packages.total = request.packages.additionalPackages.reduce((sum, packageInfo) => {
      packageInfo.rate = packageInfo.rate || 0;
      packageInfo.quantity = packageInfo.quantity || 0;
      packageInfo.usagePeriod = (packageInfo.usagePeriod === 0)
                                ? 0 : (packageInfo.usagePeriod || 1);
      packageInfo.usagePeriods = (packageInfo.usagePeriod === 0) ? 1 : diffInWeeks;
      if (packageInfo.usagePeriods < 1) packageInfo.usagePeriods = 1;
      packageInfo.total = packageInfo.rate * packageInfo.quantity * packageInfo.usagePeriods;
      packageInfo.total = this.formatCost$v1(packageInfo.total);
      sum += packageInfo.total;
      sum = this.formatCost$v1(sum);
      return sum;
    }, 0);
    request.packages.total = this.formatCost$v1(request.packages.total);

    if (request.packages.mainPackage) {
      request.packages.mainPackage.rate = request.packages.mainPackage.rate || 0;
      request.packages.mainPackage.quantity = 1;
      request.packages.mainPackage.usagePeriod = request.packages.mainPackage.usagePeriod || 1;
      request.packages.mainPackage.extraUsageRate = request.packages.mainPackage.extraUsageRate || 0;
      request.packages.mainPackage.basicUsage = request.packages.mainPackage.rate * request.packages.mainPackage.quantity;
      request.packages.mainPackage.basicUsage = this.formatCost$v1(request.packages.mainPackage.basicUsage);

      request.packages.mainPackage.usagePeriods = diffInWeeks - request.packages.mainPackage.usagePeriod;
      if (request.packages.mainPackage.usagePeriods < 0) request.packages.mainPackage.usagePeriods = 0;
      request.packages.mainPackage.extraUsage = request.packages.mainPackage.usagePeriods * request.packages.mainPackage.extraUsageRate;
      request.packages.mainPackage.extraUsage = this.formatCost$v1(request.packages.mainPackage.extraUsage);

      request.packages.mainPackage.total = request.packages.mainPackage.basicUsage + request.packages.mainPackage.extraUsage;
      request.packages.mainPackage.total = this.formatCost$v1(request.packages.mainPackage.total);

      request.packages.total += request.packages.mainPackage.total;
      request.packages.total = this.formatCost$v1(request.packages.total);
    }
    return request;
  },
calculateTaxes$v1:function({ request, calculatorSettings, constantsSettings }) {
    request.taxes = {
    	rows: [],
    	total: 0,
    };
    
    if (_.get(request, 'calculatorFlags.disableTaxes')) return;
    
    this.calculatePackagesTaxes$v1({ request, calculatorSettings, constantsSettings })
    
    request.taxes.total = _.sumBy(request.taxes.rows, row => row.value);
      
},
calculatePackagesTaxes$v1:function({ request, calculatorSettings, constantsSettings }) {
    let neededState = _.get(request, 'deliveryAddress.state');
    let taxesForState = _.get(request, `savedTaxes.${neededState}`, []);
    let taxesForStateAndType = _.find(taxesForState, {type: constantsSettings.request.TAXES_TYPE.PACKAGES});
    let taxesValue = _.get(taxesForStateAndType, 'value.amount', 0);    
    let packagesTotal  = _.get(request, 'packages.total', 0);
    let taxesAmount = packagesTotal * (taxesValue / 100);
    let taxAmount = _.round(taxesAmount, 2);
    if (taxAmount) {
	    request.taxes.rows.push({
	    	type: constantsSettings.request.TAXES_TYPE.PACKAGES,
	    	value: taxAmount,
	    	state: neededState,
	    });
    }
},
calculateTransportationPrice$moving_v1:function({ request, calculatorSettings }) {
    this.setLaborTime$moving_v1({
      request,
      calculatorSettings
    });
    this.setRate$moving_v1({
      request,
      calculatorSettings
    });
    this.setDrivingTime$moving_v1({
      request,
      calculatorSettings
    });
    this.setTravelTime$moving_v1({
      request,
      calculatorSettings
    });

    request.transportationPrice = _.round(
      ((request.laborTime + request.drivingTime + request.travelTime) / 60) * request.rate, 2);
  },
calculateTotalPrice$moving_v1:function({ request, calculatorSettings }) {
    this.calculateServices$v1({
      request,
      calculatorSettings
    });
    this.setReservation$moving_v1({
      request,
      calculatorSettings
    });
    this.setFuel$moving_v1({
      request,
      calculatorSettings
    });
    this.setValuation$moving_v1({
      request,
      calculatorSettings
    });
    this.calculatePackages$v1({
      request,
      calculatorSettings
    });
    this.calculateTransportationPrice$moving_v1({
      request,
      calculatorSettings
    });
    request.total = request.transportationPrice + request.fuel + request.valuation + request.packages.total + request.services.total
      + _.get(request, "inventory.totalServicesCost", 0);
  },
calculateFinalPrice$moving_v1:function({ request, calculatorSettings }) {
    this.calculateTotalPrice$moving_v1({
      request,
      calculatorSettings
    });
    this.calculateDiscount$moving_v1({
      request,
      calculatorSettings
    });
    request.totalWithDiscount = request.total - request.discount.total;
  },
setTravelTime$moving_v1:function({ request, calculatorSettings }) {
    request.travelTime = request.travelTime || 0;
  },
setRate$moving_v1:function({ request, calculatorSettings }) {
    request.rate = request.rate || 0;
  },
setValuation$moving_v1:function({ request, calculatorSettings }) {
    request.valuation = request.valuation || 0;
  },
setReservation$moving_v1:function({ request, calculatorSettings }) {
    request.reservation = request.reservation || 0;
  },
setLaborTime$moving_v1:function({ request, calculatorSettings }) {
    this.setCBF$moving_v1({
      request,
      calculatorSettings
    });
    this.setMoversSpeed$moving_v1({
      request,
      calculatorSettings
    });
    request.laborTime = _.round(request.cbf / request.moversSpeed);
  },
setDrivingTime$moving_v1:function({ request, calculatorSettings }) {
    request.drivingTime = request.drivingTime || 0;
  },
setFuel$moving_v1:function({ request, calculatorSettings }) {
    request.fuel = request.fuel || 0;

  },
setDiscount$moving_v1:function(data) {
    request.discount = request.discount || 0;
  },
setMoversCount$moving_v1:function({ request }) {
    request.crewSize = request.crewSize || 2;
  },
setCBF$moving_v1:function({ request }) {
    request.cbf = _.get(request, "inventory.totalVolume", 0);
  },
calculateRequest$moving_v1:function({ request, calculatorSettings }) {
    this.calculateBalance$moving_v1({
      request,
      calculatorSettings
    });

    return request;
  },
calculateBalance$moving_v1:function({ request, calculatorSettings }) {
    this.calculatePayments$v1({
      request,
      calculatorSettings
    });
    this.calculateFinalPrice$moving_v1({
      request,
      calculatorSettings
    });

    request.balance = request.totalWithDiscount - _.get(request, "payments.total", 0);

    return request;
  },
setMoversSpeed$moving_v1:function({ request }) {
    this.setMoversCount$moving_v1({ request });
    request.moversSpeed = request.crewSize * 10;
  },
getConstants$v1:function() {
    return {
      AUTO_SERVICES_NAMES: {
        DELIVERY_FLOOR: "Delivery Floor Charge",
        PICKUP_FLOOR: "Pickup Floor Charge",
        DELIVERY_DISTANCE: "Delivery Distance Charge",
        PICKUP_DISTANCE: "Pickup Distance Charge"
      }
    };
  },

};