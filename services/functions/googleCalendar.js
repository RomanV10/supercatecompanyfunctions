module.exports = {
entitiesStructure$v1:function () {
	return [
		{
			name: "Calendars",
			fields: {
				type: FieldBuilder.makeStructure({array: false, type: "integer"}),
				remoteCalendarId: FieldBuilder.makeStructure({array: false, type: "string"}),
				syncToken: FieldBuilder.makeStructure({array: false, type: "string", defaultValue:null}),
				chanelResourceId: FieldBuilder.makeStructure({array: false, type: "string", defaultValue:null}),
			}
		},
		{
			name: "CalendarsSharingRules",
			fields: {
				calendarId: FieldBuilder.makeStructure({array: false, type: "integer"}),
				userId: FieldBuilder.makeStructure({array: false, type: "integer"}),
				remoteRuleId: FieldBuilder.makeStructure({array: false, type: "string"}),
			}
		},
		{	
			name: "CalendarsEvents",
			fields: {
				calendarId: FieldBuilder.makeStructure({array: false, type: "integer"}),
				entityId: FieldBuilder.makeStructure({array: false, type: "integer"}),
				remoteEventId: FieldBuilder.makeStructure({array: false, type: "string"}),
				updated: FieldBuilder.makeStructure({array: false, type: "integer"}),
                type: FieldBuilder.makeStructure({array: false, type: "integer"}),

            }
		},
	]
},
enableCalendars$v1:function (types) {
    return Promise.all(types.map(async value => {
    let createCalendarResponse = await this.createCalendar$v1(value);
    if (createCalendarResponse.remoteCalendarId) {
       this.createCalendarWatch$v1(createCalendarResponse.remoteCalendarId);
    }
       return createCalendarResponse;
    }));
	
},
createCalendar$v1:function ({type, name}) {
  return googleCalendar.createCalendar(name)
   .then(res => {
     let data = {
       type,
       remoteCalendarId: res.data.id
     };
      return this.companyData.entityInstance.save('Calendars',data)
   })
},
disableCalendars$v1:function () {
    return this.companyData.entityInstance.getEntityMultiply('Calendars', {pagination:{itemsOnPage:99999}, fields:['id']})
        .then(res => {
          if (res.count) {
              return Promise.all(res.rows.map(row => {
                  return this.companyData.entityInstance.remove('Calendars', row.id);
              }))
          }

          return Promise.resolve([]);
        })
},
shareCalendars$v1:function ({userId, googleEmail, calendarTypes}) {
     return this.companyData.entityInstance.getEntityMultiply('Calendars', {filter:{type:calendarTypes}, fields:['remoteCalendarId']})
      .then(res => {
        return Promise.all(res.rows.map(calendarDb => {
          return googleCalendar.shareCalendar(calendarDb.remoteCalendarId, googleEmail)
            .then(remoteRule => {
              let data = {
                calendarId:calendarDb.id,
                userId,
                remoteRuleId:remoteRule.data.id
              };

              return this.companyData.entityInstance.save('CalendarsSharingRules',data);
            })
        }))
      })
  },
getUserSharedCalendars$v1:function ({userId}) {
  return this.companyData.entityInstance.getEntityMultiply('CalendarsSharingRules', {filter:{userId}, fields:['calendarId']})
   .then(res => {
     if (res.count) {
       return this.companyData.entityInstance.getEntityMultiply('Calendars', {filter:{id:_.map(res.rows, 'calendarId')}})
     }
   })
},
createEvent$v1:function (request, event = {}) {
 let calendarType = request.status;
 let calendarId;
 let remoteEvent = {};
  return this.companyData.entityInstance.getEntity('Calendars', {type:calendarType}, ['remoteCalendarId'])
   .then(calendar => {
     if (_.isEmpty(calendar)) throw 'No calendar';
     calendarId = calendar.id;
     let {remoteCalendarId} = calendar;
     if (_.isEmpty(event)) event = this.prepareGoogleCalendarEvent$v1(request);
     return googleCalendar.createEvent(event, remoteCalendarId)
   })
   .then(res => {
       remoteEvent = res;
     let data = {
       entityId:request.id,
       calendarId,
       updated:moment().unix(),
       remoteEventId: remoteEvent.data.id,
       type:calendarType
     };
     
       this.saveNewSyncToken(calendarId, remoteEvent.data.syncToken);

      return this.companyData.entityInstance.save('CalendarsEvents',data)
   })
      .then(res => {
          return remoteEvent;
      })
},
patchEvent$v1:function (request, event = {}, type) {
 let remoteEventId = '';
 let calendarId;
 let calendarEventFilter = {entityId:request.id};
 if(!_.isUndefined(type)) calendarEventFilter.type = type;

    return this.companyData.entityInstance.getEntity('CalendarsEvents', calendarEventFilter)
    .then(res => {
      if (res.remoteEventId) {
       remoteEventId = res.remoteEventId;
       return this.companyData.entityInstance.getEntity('Calendars', {id:res.calendarId})
        .then(res => {
        	   if (_.isEmpty(event)) event = this.prepareGoogleCalendarEvent$v1(request);
		      calendarId = res.id;

		      return googleCalendar.patchEvent(event, res.remoteCalendarId, remoteEventId);
		    })
	    .then(remoteEvent => {
	       this.saveNewSyncToken(calendarId, remoteEvent.data.syncToken);
	       return remoteEvent;
	    }) 
      }
      else {
       return this.createEvent$v1(request, event);
      }
    })
   
},
moveEvent$v1:function (request) {
let remoteEventId = '';
let currentRemoveCalendarId = '';
let newCalendarId;
let eventId;
  return this.companyData.entityInstance.getEntity('CalendarsEvents', {entityId:request.id})
    .then(res => {
      eventId = res.id;
      remoteEventId = res.remoteEventId;
      return this.companyData.entityInstance.getEntity('Calendars', {id:res.calendarId});
    })
    .then(res => {
      currentRemoveCalendarId = res.remoteCalendarId;
      return this.companyData.entityInstance.getEntity('Calendars', {type:request.status});
    })
    .then(res => {
    if (res.remoteCalendarId) {
      newCalendarId = res.id;
    return googleCalendar.moveEvent(remoteEventId, currentRemoveCalendarId, res.remoteCalendarId);
    }
    else {
      throw `Now calendar for status ${request.status}`;
    }      
    })
    .then(remoteEvent => {
             this.saveNewSyncToken(newCalendarId, remoteEvent.data.syncToken);

      return this.companyData.entityInstance.save('CalendarsEvents', {calendarId:newCalendarId}, eventId);
    })
},
prepareDate$v1:function (date) {
    return {
    'dateTime': moment.unix(date).tz(this.companyData.timeZone.name).format(),
    'timeZone': this.companyData.timeZone.name,
  }
},
getEventColor$BinIt:function (request) {
    // undefined	Who knows	#039be5
    // 1	Lavender	#7986cb
    // 2	Sage	#33b679
    // 3	Grape	#8e24aa
    // 4	Flamingo	#e67c73
    // 5	Banana	#f6c026
    // 6	Tangerine	#f5511d
    // 7	Peacock	#039be5
    // 8	Graphite	#616161
    // 9	Blueberry	#3f51b5
    // 10	Basil	#0b8043
    // 11	Tomato	#d60000
    let colorId = '1';
    if (request.realStatus === -3) {
        colorId = '8';
    } else {
        if(request.status <= 2) {
            colorId = '11';
        } else {
            colorId = '9';

        }
    }

    return colorId;
},
getSummaryTime$v1:function (request) {
    let startTime = 0;
    let endTime = 0;
    switch (request.status) {
        case 0:
        case 1:
        case 2:
        default:
            startTime = request.deliveryTimeWindowStart;
            endTime = request.deliveryTimeWindowEnd;
            break;
        case 3:
        case 4:
            startTime = request.pickupTimeWindowStart;
            endTime = request.pickupTimeWindowEnd;
    }

    if (startTime && endTime) {
        let decimalTime = commonsLibs.timeToDecimal(moment.unix(endTime).tz(this.companyData.timeZone.name).format('hh:mm'));
        let time = Math.ceil(decimalTime);

        return `${moment.unix(startTime).tz(this.companyData.timeZone.name).format('h')} - ${time}`;
    }else {
        return '';
    }
},
prepareGoogleCalendarEvent$v1:function (request) {
	let {startDate, endDate} = this.getNeededDates$v1(request);
	return {
		'summary': this.getSummary$v1(request),
		'location': this.getShortLocation$v1(request),
		'description': this.getDescription$v1(request),
		'start': this.prepareDate$v1(startDate),
		'end': this.prepareDate$v1(endDate),
		//'attendees': this.getUserEmails$v1(request),
	}
},
getUserEmails$v1:function (email) {
  let attendees = [];
  if (!_.isEmpty(email)) {
    attendees.push({ email: email })
  }

  return attendees;
},
getSummaryRequestType$v1:function (request) {
    let result = '';
    switch (request.status) {
        case 0:
        case 1:
        case 2:
        default:
            result = 'DEL';
            break;
        case 3:
        case 4:
            result = 'PU';
    }

    return `SUPER ${result}`;
},
getSummary$v1:function (request, settings) {
    let other = '';
    if (_.get(request, 'packages.additionalPackages[0]')) {
        other = '+ OTHER';
    }

    let result = '';

    switch (request.status) {
        case 0:
        case 1:
        case 2:
        default:
            result = `${this.getSummaryTime$v1(request)} ${this.getSummaryRequestType$v1(request)} - ${_.get(request, 'deliveryAddress.city', '')} (${_.get(request, 'packages.mainPackage.size', '')} crates, ${_.get(request, 'packages.mainPackage.rollingCarts', '')} carts ${other}) ${request.client.firstName} ${request.client.lastName}  ${_.get(settings, 'companyInfo.companyName')} #${request.id}`;
            break;
        case 3:
        case 4:
            result = `${this.getSummaryTime$v1(request)} ${this.getSummaryRequestType$v1(request)} - ${_.get(request, 'pickupAddress.city', '')} (${_.get(request, 'packages.mainPackage.size', '')} crates, ${_.get(request, 'packages.mainPackage.rollingCarts', '')} carts ${other}) ${request.client.firstName} ${request.client.lastName}  ${_.get(settings, 'companyInfo.companyName')} #${request.id}`;
            break;
    }

    return result;
},
getSummary$BinIt:function (request, settings) {
    let other = '';
    if (_.get(request, 'packages.additionalPackages[0]')) {
        other = '+ OTHER';
    }

    let result = '';

    switch (request.status) {
        case 0:
        case 1:
        case 2:
        default:
            result = `${this.getSummaryTime$v1(request)} - ${request.client.firstName} ${request.client.lastName} - ${_.get(request, 'deliveryAddress.city', '')} (${_.get(request, 'packages.mainPackage.size', '')} crates, ${_.get(request, 'packages.mainPackage.rollingCarts', '')} carts ${other})`;
            break;
        case 3:
        case 4:
            result = `${this.getSummaryTime$v1(request)} - ${request.client.firstName} ${request.client.lastName} - ${_.get(request, 'pickupAddress.city', '')} (${_.get(request, 'packages.mainPackage.size', '')} crates, ${_.get(request, 'packages.mainPackage.rollingCarts', '')} carts ${other})`;
            break;
    }

    return result;
},
getShortLocation$v1:function (request) {
    let location = '';
    switch (request.status) {
        case 0:
        case 1:
        case 2:
        default:
            location = `${request.deliveryAddress.address || 'address '}, ${request.deliveryAddress.city || 'City'}, ${request.deliveryAddress.state || 'state'}, ${request.deliveryAddress.postal || 'postal'}`;
            break;
        case 3:
        case 4:
            location = `${request.pickupAddress.address || 'address '}, ${request.pickupAddress.city || 'City'}, ${request.pickupAddress.state || 'state'}, ${request.pickupAddress.postal || 'postal'}`;
            break;
    }
    return location;
},
getEstimatedTime$v1:function (request, status) {
    let startTime = 0;
    let endTime = 0;
    let time = '';

    switch (status) {
        case 0:
        case 1:
        case 2:
        default:
            startTime = request.deliveryTimeWindowStart;
            endTime = request.deliveryTimeWindowEnd;
            break;
        case 3:
        case 4:
            startTime = request.pickupTimeWindowStart;
            endTime = request.pickupTimeWindowEnd;
            break;
    }

    if (startTime && endTime) {
        time  = `${moment.unix(startTime).tz(this.companyData.timeZone.name).format('h:mm')} - ${moment.unix(endTime).tz(this.companyData.timeZone.name).format('h:mm')}`;
    }
    return time;
},
getLongLocation$v1:function (request) {
  let movingFrom = `
    ${this.wrapLabel('Drop off:')} ${_.get(request, 'deliveryAddress.address', '')}, ${_.get(request, 'deliveryAddress.city', '')}, ${_.get(request, 'deliveryAddress.state', '')} 
    ${_.get(request, 'deliveryAddress.postal', '')}, ${this.wrapLabel('Apt:')} ${_.get(request, 'deliveryAddress.apt', '')}
    ${this.wrapLabel('Estimated Time:')}  ${this.getEstimatedTime$v1(request, 0)}
  `
  let movingTo = `
    ${this.wrapLabel('Pickup:')} ${_.get(request, 'pickupAddress.address', '')}, ${_.get(request, 'pickupAddress.city', '')}, ${_.get(request, 'pickupAddress.state', '')} 
    ${_.get(request, 'pickupAddress.postal', '')}, ${this.wrapLabel('Apt:')} ${_.get(request, 'pickupAddress.apt', '')}
    ${this.wrapLabel('Estimated Time:')} ${this.getEstimatedTime$v1(request, 3)}
	`
	return `${movingFrom}${movingTo}`;
},
getDescription$v1:function (request) {
  return `
    ${request.client.firstName} ${request.client.lastName}
    ${request.client.phone || ''}
    <a href="${request.client.email || ''}">${request.client.email || ''}</a>
    ${this.wrapLabel('Package:')} ${_.get(request, 'packages.mainPackage.name', '')} ${_.get(request, 'packages.mainPackage.size', '')} crates included. 
    ${_.get(request, 'packages.mainPackage.rollingCarts', '')} rolling carts.${_.get(request, 'packages.mainPackage.usagePeriod', '')} weeks of usage. \n
    ${this.getAdditionalPackages(request)}
    ${this.getExtraServices(request)}
    ${this.getTaxes(request)}   
    ${this.getLongLocation$v1(request)}
    ${this.wrapLabel('Receipt Details:')}
    ${this.wrapLabel('Order:')} #${request.id}
    ${this.wrapLabel('Package:')} ${_.get(request, 'packages.mainPackage.name', '')} ${_.get(request, 'packages.mainPackage.size', '')} crates: \$${_.get(request, 'packages.mainPackage.total', '')}
    ${this.wrapLabel('Total:')} \$${_.get(request, 'totalWithDiscount', 0)}
  `
},
getAdditionalPackages:function (request) {
        let result = '';
        if (_.get(request, 'packages.additionalPackages')) {
            result += `${this.wrapLabel('Supplies:')} \n`;
            request.packages.additionalPackages.forEach(getAdditionalPackage => {
                result += `    ${getAdditionalPackage.name} - (${getAdditionalPackage.quantity}) \n`;
            });
        }

        return result;
    },
getExtraServices:function (request) {
    let result = '';

    if (_.get(request, 'services.autoServices')) {
        let label = `${this.wrapLabel('Services:')} \n`;
        request.services.autoServices.forEach(service => {
            if (!service.disabled) result += `    ${service.name} ${service.total} \n`;
        });

        if (result !== '') result = label + result;
    }

    if (_.get(request, 'services.customServices')) {
        request.services.customServices.forEach((service, index) => {
            result +=  `    ${service.name} $${service.total} \n`;
        });
    }

    return result;
    },
getTaxes:function (request) {
    if (_.get(request, 'taxes.total')) return `${this.wrapLabel('Tax:')} $${request.taxes.total}`;
    return '';

},
getEnabledCalendars$v1:function () {
   return this.companyData.entityInstance.getEntityMultiply('Calendars');
},
deleteCalendarSharingRules$v1:function ({userId, calendarTypes}) {
   let calendarWithRemoteRulesIds = {};
    return this.companyData.entityInstance.getEntityMultiply('CalendarsSharingRules', {filter:{userId}, fields:['calendarId', 'remoteRuleId']})
    .then(res => {
      let promise = Promise.resolve();
        if (res.count) {
          let calendarsIds = [];
          res.rows.forEach(row => {
            if (!calendarWithRemoteRulesIds[row.calendarId]) calendarWithRemoteRulesIds[row.calendarId] = {remoteRulesIds:[]};
            calendarWithRemoteRulesIds[row.calendarId].remoteRulesIds.push(row.remoteRuleId);
            calendarsIds.push(row.calendarId);
          });

          if (calendarsIds.length) {
            promise = this.companyData.entityInstance.getEntityMultiply('Calendars', {filter:{id:calendarsIds, type:calendarTypes}})
              .then(res => {
                 let promise = Promise.resolve();

                 if (res.count) {
                   let calendarWithRulesForDelete = [];

                   res.rows.forEach(row => {
                      if (calendarWithRemoteRulesIds[row.id]) {
                        calendarWithRemoteRulesIds[row.id].remoteRulesIds.forEach(remoteRuleId => {
                          calendarWithRulesForDelete.push({remoteCalendarId:row.remoteCalendarId, remoteRuleId});
                        })
                      }
                   });

                   if (calendarWithRulesForDelete.length) {
                     console.log(calendarWithRulesForDelete);
                     promise = Promise.all(calendarWithRulesForDelete.map(value => {
                          return this.deleteCalendarSharingOneRule$v1(value.remoteCalendarId, value.remoteRuleId);
                      }));
                   }
                 }

                 return promise;
              })
          }
        }

        return promise;
    });
},
deleteCalendarSharingOneRule$v1:function (remoteCalendarId, remoteRuleId) {
return googleCalendar.deleteCalendarSharingRule(remoteCalendarId, remoteRuleId)
    .then(res => {
      return this.companyData.entityInstance.removeBy('CalendarsSharingRules', {remoteRuleId});
    })
    .catch(err => {
      throw err;
    })
},
requestFieldsForCalendar$v1:function () {
return [
    'deliveryWorkTime',
    'pickupWorkTime', 
    'deliveryCrewSize', 
    'deliveryCrewSize', 
    'pickupCrewSize', 
    'referrer', 
    'type', 
    'packages', 
    'services', 
    'discount', 
    'total',
    'deliveryAddress',
    'pickupAddress',
    'pickupTimeWindowStart',
    'pickupTimeWindowEnd',
    'status',
    'deliveryTimeWindowStart',
    'deliveryTimeWindowEnd',
    'deliveryWorkTime'
  ];
},
getNeededDates$v1:function (request) {
  let startDate;
  let endDate;
	  switch (request.status) {
      case 0:
      case 1:
      case 2:
      default:
        startDate = request.deliveryTimeWindowStart;
        endDate = request.deliveryTimeWindowEnd;
        break;
      case 3:
      case 4:
        startDate = request.pickupTimeWindowStart;
        endDate = request.pickupTimeWindowEnd;

  }
    if (request.realStatus === -3) {
        startDate = moment.unix(startDate).tz(this.companyData.timeZone.name).startOf('day').add(3, 'hours').unix();
    }
  return {startDate, endDate};
},
getNeededDates$BintIt:function (request) {
    let startDate;
    let endDate;
    switch (request.status) {
        case 0:
        case 1:
        case 2:
        default:
            startDate = request.deliveryTimeWindowStart;
            endDate = request.deliveryTimeWindowEnd;
            break;
        case 3:
        case 4:
            startDate = request.pickupTimeWindowStart;
            endDate = request.pickupTimeWindowEnd;

    }
    return {startDate, endDate};
},
wrapLabel:function (label) {
	return `<strong>${label}</strong>`
},
getGoogleUpdatedEvents$v1:function (calendarId) {
    return this.getGoogleUpdatedEventsCore(calendarId)
},
getGoogleUpdatedEventsCore:function (calendarId, useEndDate = true) {
  let syncToken;
	  let remoteEvents = [''];
	  let calendatType;

    return this.companyData.entityInstance.getEntity('Calendars', {id:calendarId})
      .then(calendar => {
        calendatType = calendar.type;
         return googleCalendar.getCalendarEventsList(calendar.remoteCalendarId, {syncToken:calendar.syncToken});
      })
      .then(res => {
        syncToken = res.data.nextSyncToken;
        remoteEvents = res.data.items;
        let remoteEventsIds = _.map(remoteEvents, 'id');
        return this.companyData.entityInstance.getEntityMultiply('CalendarsEvents', {filter:{remoteEventId:remoteEventsIds}});
      })
        .then(events => {
          console.log(events);
          if (events.count) {
            return events.rows.map(event => {
              let remoteEvent = _.find(remoteEvents, {id:event.remoteEventId});
              let dataForSave = {id:event.entityId};

              let startDay = moment(remoteEvent.start.dateTime).tz(this.companyData.timeZone.name).startOf('day').unix();
              let start = moment(remoteEvent.start.dateTime).unix();
              let end = moment(remoteEvent.end.dateTime).unix();

              switch (calendatType) {
                case 0:
                case 1:
                case 2:
                default:
                  dataForSave.deliveryDate = startDay;
                  dataForSave.deliveryTimeWindowStart = start;
                  dataForSave.deliveryTimeWindowEnd = end;
                  break;
                case 3:
                case 4:
                  dataForSave.pickupDate = startDay;
                  dataForSave.pickupTimeWindowStart = end;
                  dataForSave.pickupTimeWindowEnd = end;
                  break;
              }

               return dataForSave;
            });
          }

          this.saveNewSyncToken(calendarId, syncToken);
          return [];
        })
},
saveNewSyncToken:function (calendarId, syncToken) {
    return this.companyData.entityInstance.save('Calendars', {syncToken}, calendarId);
},
createCalendarWatch$v1:function (remoteCalendarId) {
let calendarId;
let idForWatch = remoteCalendarId.split('@')[0];
 return this.companyData.entityInstance.getEntity('Calendars', {remoteCalendarId}, ['chanelResourceId'])
 .then(res => {
  calendarId = res.id;
   return googleCalendar.stopWatchingCalendarEvents(idForWatch, res.chanelResourceId);
 })
 .then(res => {
     const fs = require('fs');
     let jsonData = fs.readFileSync(`${rootDir}/companiesData.json`, 'utf8');
     let companiesData = JSON.parse(jsonData);
     let host =  _.get(companiesData, `${this.companyData.companyName}.hosts.0`, '');

     let url = `https://${host}/api/googleCalendarWatch/${this.companyData.companyName}/${calendarId}`;
     return googleCalendar.watchCalendarEvents(idForWatch, remoteCalendarId, url);
 })
 .then(res => {
    return this.companyData.entityInstance.save('Calendars', {chanelResourceId:res.data.resourceId}, calendarId);

 })

},
removeRequestFromCalendar:function (request) {
    let remoteEvents = [];
    return this.companyData.entityInstance.getEntityMultiply('CalendarsEvents', {filter:{entityId:request.id}})
        .then(res => {
            remoteEvents = res.rows;
            // eventId = res.id;
            // remoteEventId = res.remoteEventId;
            return this.companyData.entityInstance.getEntityMultiply('Calendars', {filter:{id:_.map(remoteEvents, 'calendarId')}});
        })
        .then(res => {
            return Promise.all(res.rows.map(row => {
                let remoteEvent = _.find(remoteEvents, {calendarId:row.id});
                return googleCalendar.deleteEvent(remoteEvent.remoteEventId, row.remoteCalendarId);
            }));
        })
        .then(res => {
            return Promise.all(remoteEvents.map(row => {
                return this.companyData.entityInstance.remove('CalendarsEvents', row.id);
            }));
        })

},
sendRequestToGoogleCalendar$v1:function ({newRequest, oldRequest = {}, settings = {}}) {
    if (!_.isEmpty(oldRequest)) {
        if (newRequest.status !== oldRequest.status && newRequest.status < 0) {
            return this.removeRequestFromCalendar(newRequest);
        } else {
            let requestFieldsForCalendar = this.requestFieldsForCalendar$v1();
            let diff = commonsLibs.objectsDiff(newRequest, oldRequest);
            let diffKeys = Object.keys(diff);
            let neededFields = _.intersection(diffKeys, requestFieldsForCalendar);
            if (neededFields.length) {
                return this.patchEvent$v1(newRequest)
                    .then(res => {
                        let movePromise = Promise.resolve(res);
                        if (diff.status) {
                            movePromise = this.moveEvent$v1(newRequest);
                        }

                        return movePromise;
                    })

            }
            return Promise.resolve();
        }

    } else {
        return this.createEvent$v1(newRequest);
    }
},
sendRequestToGoogleCalendar$vSuperCrate:function ({newRequest, oldRequest = {}, settings = {}}) {
    //Если request canceled, то ставим цвет красный и начало времени на 3 утра.
    // Если статус минусовой и на canceled - удаляем
    let consts = this.doTask('constantine')();
    let companyStyle = _.get(settings, 'googleCalendarSettings.style', consts.COMPANY_STYLES.SUPERCRATE);
    let requestFieldsForCalendar = this.requestFieldsForCalendar$v1();
    let diff = commonsLibs.objectsDiff(newRequest, oldRequest);
    let diffKeys = Object.keys(diff);
    let neededFields = _.intersection(diffKeys, requestFieldsForCalendar);

    if (neededFields.length) {
        if (newRequest.status > 1 || newRequest.status === -3) {
            newRequest.realStatus = newRequest.status;
            let deliveryRequest = _.clone(newRequest);
            deliveryRequest.status = 2;

            let pickupRequest = _.clone(newRequest);
            pickupRequest.status = 3;

            let requestForCalendars = [deliveryRequest, pickupRequest];

            return Promise.all(requestForCalendars.map(request => {
                let event = {};
                switch (companyStyle) {
                    case consts.COMPANY_STYLES.BINIT:
                        event = this.prepareGoogleCalendarEvent$vBinIt(request, settings);
                        break;
                    case consts.COMPANY_STYLES.SUPERCRATE:
                    default:
                        event = this.prepareGoogleCalendarEvent$vSuperCrate(request, settings);
                        break;
                }


                return this.patchEvent$vSuperCrate(request, event, request.status);
            }));
        } else {
            return this.removeRequestFromCalendar(newRequest);
        }

    }

    return Promise.resolve();
	},
prepareGoogleCalendarEvent$vSuperCrate:function (request, settings) {
    let {startDate, endDate} = this.getNeededDates$v1(request);
    let event = {};
    if (request.realStatus === -3) {
        event.colorId = '11';
    } else {
        event.colorId = '1';
    }

    event.summary= this.getSummary$v1(request, settings);
    event.location= this.getShortLocation$v1(request);
    event.description= this.getDescription$v1(request);
    event.start= this.prepareDate$v1(startDate);
    event.end= this.prepareDate$v1(startDate + 30*60);

    return event;
    },
prepareGoogleCalendarEvent$vBinIt:function(request, settings) {
    let {startDate, endDate} = this.getNeededDates$BintIt(request);
    let event = {};

    event.colorId = this.getEventColor$BinIt(request);
    event.summary= this.getSummary$BinIt(request, settings);
    event.location= this.getShortLocation$v1(request);
    event.description= this.getDescription$v1(request);
    event.start= this.prepareDate$v1(startDate);
    event.end= this.prepareDate$v1(endDate);

    return event;
},
patchEvent$vSuperCrate:function (request, event = {}, type) {
    let remoteEvent = {};
    let calendarId;
    let calendarEventFilter = {entityId:request.id};
    if(!_.isUndefined(type)) calendarEventFilter.type = type;

    return this.companyData.entityInstance.getEntity('CalendarsEvents', calendarEventFilter)
        .then(res => {
            if (res.remoteEventId) {
                remoteEvent = res;
                return this.companyData.entityInstance.getEntity('Calendars', {id:remoteEvent.calendarId})
                    .then(res => {
                        if(res.id) {
                            calendarId = res.id;
                            return googleCalendar.patchEvent(event, res.remoteCalendarId, remoteEvent.remoteEventId)
                                .then(remoteEvent => {
                                    this.saveNewSyncToken(calendarId, remoteEvent.data.syncToken);
                                    return remoteEvent;
                                })
                        }
                        else {
                            return this.companyData.entityInstance.remove('CalendarsEvents', remoteEvent.id)
                                .then(() => {
                                    return this.createEvent$v1(request, event);
                                })
                        }
                    })
            }
            else {
                return this.createEvent$v1(request, event);
            }
        })
    },
getGoogleUpdatedEvents$vSuperCrate:function (calendarId) {
        return this.getGoogleUpdatedEventsCore(calendarId)
    },
constantineLexel$v1:function () {
    return {
        AVAILABLE_STATUSES: [
            'PENDING', 'PAID', 'DELIVERY', 'PICKUP', 'COMPLETE',
        ],
        COMPANY_STYLES: {SUPERCRATE:0, BINIT:1}
    }
},

};