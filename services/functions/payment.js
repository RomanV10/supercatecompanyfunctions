module.exports = {
entitiesStructure$v1:function () {
	return [
		{
			name: "Receipt",
			fields: {
				entityId: FieldBuilder.makeStructure({array: false, type: "integer"}),
				userId: FieldBuilder.makeStructure({array: false, type: "integer"}),
				type: FieldBuilder.makeStructure({array: false, type: "integer"}),
				amount: FieldBuilder.makeStructure({array: false, type: "decimal"}),
				transactionId: FieldBuilder.makeStructure({array: false, type: "string"}),
				paymentProvider: FieldBuilder.makeStructure({array: false, type: "integer"}),
				email: FieldBuilder.makeStructure({array: false, type: "string"}),
				invoiceId: FieldBuilder.makeStructure({array: false, type: "integer"}),
				data: FieldBuilder.makeStructure({array: false, type: "json"}),
				refunds: FieldBuilder.makeStructure({array: true, type: "integer", defaultValue:[]}),
				refund: FieldBuilder.makeStructure({array: false, type: "boolean", defaultValue:false}),
				receiptId: FieldBuilder.makeStructure({array: false, type: "integer"}),

			}
		},
		{
			name: "AuthorizeNetProfile",
			fields: {
				userId: FieldBuilder.makeStructure({array: false, type: "integer"}),
				customerProfileId: FieldBuilder.makeStructure({array: false, type: "string"}),
				customerPaymentProfileId: FieldBuilder.makeStructure({array: true, type: "string"}),
			}
		}
	]
},
createReceipt$v1:function (data) {
  if (!_.get(data, 'user.id')) throw 'PAYMENT_CREATE_RECEIPT_NO_USER_ID';
  return this.companyData.paymentInstance.createReceipt(data);
},
getRequestReceipts$v1:function (entityId) {
	return this.companyData.entityInstance.getEntityMultiply('Receipt', {
			filter: {
				entityId,
				refund: false,
			},
			pagination: {itemsOnPage: 999999}
		})
		.then(res => {
			return this.getRefundsForManyReceipts(res);
		})
		.then(res => {
			return this.doTask('calculateRequestPayments')(res.rows);
		})
},
searchReceipts$v1:function (data) {
	if (!data.filter) data.filter = {};
	data.filter.refund = false;
	return this.companyData.entityInstance.getEntityMultiply('Receipt', data)
		.then(res => {
			return this.getRefundsForManyReceipts(res);
		})
},
getPaymentTotalByDate$v1:function (data) {
	let filter = {
		createdAt: {
			op: 'BETWEEN',
			data
		}
	};
	return this.companyData.entityInstance.getEntityMultiply('Receipt', {
		filter,
		fields: ['amount', 'entityId']
	})
		.then(res => {
			if (!res.rows) return {totalAmount: 0, entitiesCount: 0};
			return {
				totalAmount: _.sumBy(res.rows, 'amount'),
				entitiesCount: _.uniqBy(res.rows, row => row.entityId).length,
			};
		});
},
getRefundsForManyReceipts:function (receipts) {
		return Promise.all(receipts.rows.map(receipt => {
			if (receipt.refunds && receipt.refunds.length) {
				return this.companyData.entityInstance.getEntityMultiply('Receipt', {filter: {id:receipt.refunds}})
					.then(refunds => {
						receipt.refunds = refunds.rows;
					})
			}
		}))
			.then(() => {
				return Promise.resolve(receipts);
			})
	},
getCustomerProfile$v1:function (data) {
	return this.companyData.paymentInstance.getCustomerProfile(data);
},
calculateRequestPayments$v1:function (payments) {
	let result = {};
	result.rows = payments || [];
	result.total = result.rows.reduce(
		(sum, row) => {
			let amount = +(row.amount || 0);
			let value = sum + amount - _.sumBy((row.refunds || []), "amount");
			return +(value.toFixed(2));
		}, 0);
	return result;	
},

};