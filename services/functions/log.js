module.exports = {
entitiesStructure$v1:function () { return [
  {
    name: "Log",
    fields: {
      type: {array: false, type: "string"},
      title: {array: false, type: "string"},
      body: {array: false, type: "json"},
      difference: {array: false, type: "json"},
      source: {array: false, type: "string"}, 
      author: {array: false, type: "string"},      
      authorId: {array: false, type: "integer"},
      entityId: {array: false, type: "integer"},
      entityType: {array: false, type: "string"},
      mailId: {array: false, type: "integer"},
      useragent: {array: false, type: "json"},

    }
  }
]},
getLogs$v1:function (data) {
  return this.companyData.entityInstance.getEntityMultiply('Log', data)
},
getLog$v1:function (data) {
    return this.companyData.entityInstance.getEntity('Log', data)
},
saveLog$v1:function (data) {
 if (data.forDiff && !_.isEmpty(data.forDiff)) {
    data.difference = commonsLibs.objectsDiff(data.forDiff.oldObj || {}, data.forDiff.newObj || {});
  }
  data.authorId = this.systemData.authorizedUser.id;
  data.author = this.systemData.authorizedUser.firstName + ' ' + this.systemData.authorizedUser.lastName;
  data.useragent = this.systemData.useragent;

   return this.companyData.entityInstance.save('Log', data);
},

};