module.exports = {
createRequest$v1:function ( data) {
        if (!data.source) data.source = this.systemData.source;
        return this.companyData.entityInstance.Request.save(data)
            .then(request => {
                return this.getRequest$v1({id:request.id});
            })
    },
getRequest$v1:function ( data, fields, usePermissions = true ) {
        if (usePermissions) permissions.request.addGetRequestFilter(this.systemData.authorizedUser, data);

        let request;
        return this.companyData.entityInstance.Request.get({filter:data, fields})
            .then(res => {
                if (!res) throw 'You don\'t have access to this request';
                request = res;
                if (res.savedVariables) return this.getRequestWithVars(request);
                return request;
            });
    },
getRequestsAll$v1:function ( data) {return this.companyData.entityInstance.Request.getMultiply({filter:{id:data.ids}});},
entitiesStructure$v1:function () {
        return [
            {
                name: "Request",
                fields: {
                    client: FieldBuilder.makeStructure({array: false, type: 'entityReference', relatedEntity: "User"}),
                    manager: FieldBuilder.makeStructure({array: false, type: 'entityReference', relatedEntity: "User"}),
                    deliveryDate: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    deliveryTime: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    pickupDate: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    pickupTime: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    deliveryTimeWindowStart: FieldBuilder.makeStructure({
                        array: false,
                        type: "integer",
                        defaultValue: 0
                    }),
                    deliveryTimeWindowEnd: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    pickupTimeWindowStart: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    pickupTimeWindowEnd: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    deliveryWorkTime: FieldBuilder.makeStructure({array: false, type: "decimal", defaultValue: 60}),
                    pickupWorkTime: FieldBuilder.makeStructure({array: false, type: "decimal", defaultValue: 60}),
                    deliveryCrewSize: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    pickupCrewSize: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    status: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    source: FieldBuilder.makeStructure({array: false, type: "string", defaultValue: 'dashboard'}),
                    referrer: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    type: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    deliveryAddress: FieldBuilder.makeStructure({
                        array: false, type: "json", defaultValue: {
                            address: "",
                            apt: "",
                            city: "",
                            floor: "",
                            postal: "",
                            state: "",
                        }
                    }),
                    pickupAddress: FieldBuilder.makeStructure({
                        array: false, type: "json", defaultValue: {
                            address: "",
                            apt: "",
                            city: "",
                            floor: "",
                            postal: "",
                            state: "",
                        }
                    }),
                    packages: FieldBuilder.makeStructure({array: false, type: "json", defaultValue: {}}),
                    services: FieldBuilder.makeStructure({array: false, type: "json", defaultValue: {}}),
                    payments: FieldBuilder.makeStructure({array: false, type: "json", defaultValue: {}}),
                    discount: FieldBuilder.makeStructure({array: false, type: "json", defaultValue: {}}),
                    total: FieldBuilder.makeStructure({array: false, type: "decimal", defaultValue: 0}),
                    totalWithDiscount: FieldBuilder.makeStructure({array: false, type: "decimal", defaultValue: 0}),
                    balance: FieldBuilder.makeStructure({array: false, type: "decimal", defaultValue: 0}),
                    notes: FieldBuilder.makeStructure({array: false, type: "json", defaultValue: {}}),
                    savedVariables: FieldBuilder.makeStructure({array: true, type: "string",}),
                    savedTaxes: FieldBuilder.makeStructure({array: false, type: "json", defaultValue: {}}),
                    taxes: FieldBuilder.makeStructure({array: false, type: "json", defaultValue: {}}),
                    location: FieldBuilder.makeStructure({array: false, type: "json", defaultValue: {}}),
                    calculatorFlags: FieldBuilder.makeStructure({array: false, type: "json", defaultValue: {}}),
                    bookedDate: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    truckWasAutoassigned: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                }
            },
            {
                name: 'User',
                fields: {
                    firstName: FieldBuilder.makeStructure({array: false, type: 'string'}),
                    lastName: FieldBuilder.makeStructure({array: false, type: 'string'}),
                    email: FieldBuilder.makeStructure({array: false, type: 'string'}),
                    password: FieldBuilder.makeStructure({array: false, type: 'string'}),
                    phone: FieldBuilder.makeStructure({array: false, type: 'string'}),
                    phoneAdditional: FieldBuilder.makeStructure({array: false, type: 'string'}),
                    companyName: FieldBuilder.makeStructure({array: false, type: 'string'}),
                    companyNameCustom: FieldBuilder.makeStructure({array: false, type: 'string'}),
                    roles: FieldBuilder.makeStructure({array: true, type: 'integer'}),
                    hashLogin: FieldBuilder.makeStructure({array: false, type: 'string'}),
                    status: FieldBuilder.makeStructure({array: false, type: 'integer', defaultValue: 0}),
                    avatar: FieldBuilder.makeStructure({array: false, type: 'string'}),
                    googleEmail: FieldBuilder.makeStructure({array: false, type: 'string'}),
                    googleCalendarStatuses: FieldBuilder.makeStructure({array: false, type: 'json', defaultValue:[]}),

                },
            },
            {
                name: 'Variable',
                fields: {
                    name: FieldBuilder.makeStructure({array: false, type: 'string'}),
                    value: FieldBuilder.makeStructure({array: false, type: 'json'}),
                    version: FieldBuilder.makeStructure({
                        array: false,
                        type: 'integer',
                        defaultValue: 0,
                        index: true
                    }),
                },
            },
            {
                name: 'Taxes',
                fields: {
                    state: FieldBuilder.makeStructure({array: false, type: 'string'}),
                    type: FieldBuilder.makeStructure({array: false, type: 'integer'}),
                    value: FieldBuilder.makeStructure({array: false, type: 'json'}),
                },
            }
        ]
    },
getUser$v1:function ( data) {
        return permissions.request.isUserCanOpenUser(this.systemData.authorizedUser, data)
            .then(()   => {
                return this.companyData.entityInstance.User.get({filter:data});
            })
            .then(user => {
                if (!_.isEmpty(user)) {
                    user.companyName = this.systemData.companyName;

                    if (this.systemData.authorizedUser.id !== -1) delete user.password;
                }

                return user;
            })
    },
createUser$v1:function ( data) {
        if (commonsLibs.Permissions.isAdmin(this.systemData.authorizedUser)) {
            if (!data.roles || _.isEmpty(data.roles)) {
                data.roles = [2];
            }
        }
        else {
            data.roles = [2];
        }

        let microtime = Date.now();
        data.hashLogin = sha256(`${microtime}${data.email}`);

        return this.companyData.entityInstance.User.save(data);
    },
searchRequests$v1:function ( data) {
        permissions.request.addSearchRequestsFilter(this.systemData.authorizedUser, data);
        return this.companyData.entityInstance.Request.getMultiply(data);
    },
getEntity$v1:function (data) {
        return this.companyData.entityInstance[data.name].get({filter:data.conditions});
    },
saveEntity$v1:function ( data) {
        return this.companyData.entityInstance[data.name].save(data.value, data.value.id);
    },
getManyEntities$v1:function ( data) {return this.companyData.entityInstance[data.name].getMultiply(data.conditions);},
saveVariable$v1:function (variableInfo) {

        const requestStoredVariables = this.doTask('getVariablesNamesStoredInRequest')()
        let lastVersion;
        let lastVersionId;

        if (requestStoredVariables.indexOf(variableInfo.name) !== -1) {
            return this.getVariable$v1({name: variableInfo.name})
                .then(res => {
                    lastVersion = res.version;
                    lastVersionId = res.id;
                    return this.getRequest$v1({
                        'savedVariables': `${variableInfo.name}_${res.version}`
                    }, ['id'], false);
                })
                .then(res => {
                    if (res.id) {
                        variableInfo.version = lastVersion + 1;
                        return this.companyData.entityInstance.Variable.save(variableInfo);
                    }

                    delete variableInfo.version;
                    return this.companyData.entityInstance.Variable.save(variableInfo, lastVersionId);
                });
        }

        return this.companyData.entityInstance.Variable.get({filter:{name: variableInfo.name}})
            .then(variableInBase => {
                delete variableInfo.version;
                return this.companyData.entityInstance.Variable.save(variableInfo, _.get(variableInBase, 'id'));

            });
    },
getFrontComponents$v1:function () { return {
        RootComponent: 'vTesting',
        Navigation: 'v1',
        RequestTable: 'v1',
        Dashboard: 'v1',
        RequestPage: 'v1',
        Settings: 'v1',
        ModalManager: 'v1',
        EditRequest: 'v1',
        TopPanel: 'v1',
        StoreRequests: 'v1',
        StoreCompanySettings: 'v1',
        ScreenInfoStore: 'v1',
        DeviceQualifier: 'v1',
        EmailStore: 'v1',
    }
    },
saveTaxes$v1:function(taxes) {
    if (!taxes.id) {
        let {state, type} = taxes;
        return this.companyData.entityInstance.Taxes.get({filter:{state, type}})
            .then(res => {
                return this.companyData.entityInstance.Taxes.save(taxes, res.id);
            })
    } else {
        return this.companyData.entityInstance.Taxes.save(taxes, taxes.id);
    }
},
saveRequest$v1:function (data) {
        let request;

        return permissions.request.isUserCanUpdateRequest(this.systemData.authorizedUser, data, this.companyData)
            .then(res => {
                if (data.value.id) delete data.value.savedVariables;
                if (!data.value.id) {
                  const requestStoredVariables = this.doTask('getVariablesNamesStoredInRequest')()
                    return this.getManyVariables$v1(requestStoredVariables.map(name => ({name})))
                        .then(res => {
                            data.value.savedVariables = _.map(res, varInfo => `${varInfo.name}_${varInfo.version}`);
                            return this.companyData.entityInstance.Request.save(data.value);
                        });
                }
                return this.companyData.entityInstance.Request.save(data.value, data.value.id);
            })
            .then(res => {
                request = res;
                return this.getRequestWithVars(request)
            });
    },
getUserPublicInfo$v1:function (data) {
        return this.companyData.entityInstance.User.get({filter:data, fields:['id', 'firstName', 'lastName' ,'hashLogin', 'email']})
            .then(user => {
                if (user) user.companyName = this.systemData.companyName;
                return user;
            })
    },
validationRules$v1:function () {return {
        saveRequest: {
            'client.email': `string|email`,
            'manager.email': `string|email`,
        },
    }
    },
saveUser$v1:function (data) {
        let permissionCheck = Promise.resolve();
        if (data.id) {
            permissionCheck = permissions.request.isUserCanOpenUser(this.systemData.authorizedUser, data);
        }
        return permissionCheck
            .then(res => {
                return this.companyData.entityInstance.User.save(data, data.id);
            })
            .then(user => {
                delete user.password;
                return user;
            })
    },
cloneRequest$v1:function (id) {
        return this.companyData.entityInstance.Request.get({filter:{id}})
            .then(request => {
                if (!request) throw 'No request with this id';
                let clientId = _.get(request, 'client.id');
                let managerId = _.get(request, 'manager.id');

                request.status = 0;
                request.client = clientId;
                request.manager = managerId;

                delete request.payments;

                return this.createRequest$v1(request);
            });
    },
getRequestsNoPermissions$v1:function (data) {
        return this.companyData.entityInstance.Request.getMultiply(data);
    },
getUsers$v1:function (queryData) {
        return this.companyData.entityInstance.User.getMultiply(queryData)
            .then(res => {
                res.rows.forEach(user => delete user.password);
                return res;
            })
    },
getUsersForDepartment$v1:function () {
        const USER_ROLES = this.doTask('constantine')().USER_ROLES;

        return this.getUsers$v1({pagination:{itemsOnPage: 999999}, filter:{roles:{op:'NOT', data:USER_ROLES.CLIENT}}});
    },
saveRequestsMultiply$v1:function (requests) {
        return Promise.all(requests.map(request => {
            return this.companyData.entityInstance.Request.save(request, request.id);
        }));

    },
refreshUserPassword$v1:function (user) {
        const generatePassword = require('password-generator');
        let newPassword = generatePassword(8, false);

        const bcrypt = require('bcryptjs');
        let passwordHash = bcrypt.hashSync(newPassword);
        return this.companyData.entityInstance.User.save({password: passwordHash}, user.id)
            .then(res => {
                return newPassword;
            })
    },
getVariable$v1:function (request) {
        // {name: string, version: number}
        if (_.isUndefined(request.version) || _.isNull(request.version)) delete request.version;

        return this.getEntity$v1({
            name: 'Variable',
            conditions: request,
        })
            .then((variable) => {
                return _.isEmpty(variable)
                    ? {name: request.name, value: null, version: null}
                    : variable;
            })
            .catch(err => {
                return {name: request.name, value: null, version: null};
            })
    },
getManyVariables$v1:function (request) {
        return Promise.all(request.map(varInfo => {
            return this.getVariable$v1(varInfo);
        }))
            .then(variables => {

                let result = {};

                _.forEach((variables), ({name, value, version}) => {
                    result[name] = {name, value, version};
                });

                return result;
            });
    },
getRequestVariables$v1:function (request) {

        return this.getRequest$v1( {id: request.id }, ['savedVariables'] )
            .then(request => request.savedVariables)

    },
getRequestVar:function (savedVariables) {
        let variables = savedVariables.map(variableKey => {
            let versionIndex = variableKey.lastIndexOf('_');
            return {
                name: variableKey.slice(0, versionIndex),
                version: +variableKey.slice(versionIndex + 1),
            };
        });

        return this.getManyVariables$v1(variables);
    },
getRequestWithVars:function (request) {
        return this.getRequestVar(request.savedVariables)
            .then(vars => {
                request.savedVariables = this.makeOldVariableFormat$v1(vars);
                return request;
            });
    },
makeOldVariableFormat$v1:function (variables) {
        let vars = _.clone(variables);
        _.forEach(vars, (value, name) => vars[name] = value.value);
        return vars;
    },
entitiesStructure$moving_v1:function () {
        return [
            {
                name: "Request",
                fields: {
                    serviceType: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    client: FieldBuilder.makeStructure({array: false, type: 'entityReference', relatedEntity: "User"}),
                    manager: FieldBuilder.makeStructure({array: false, type: 'entityReference', relatedEntity: "User"}),
                    deliveryDate: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    pickupDate: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    deliveryTimeWindowStart: FieldBuilder.makeStructure({
                        array: false,
                        type: "integer",
                        defaultValue: 0
                    }),
                    deliveryTimeWindowEnd: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    pickupTimeWindowStart: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    pickupTimeWindowEnd: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    crewSize: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    status: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    source: FieldBuilder.makeStructure({array: false, type: "string", defaultValue: 'dashboard'}),
                    referrer: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    type: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    deliveryAddress: FieldBuilder.makeStructure({
                        array: false, type: "json", defaultValue: {
                            address: "",
                            apt: "",
                            city: "",
                            floor: "",
                            postal: "",
                            state: "",
                        }
                    }),
                    pickupAddress: FieldBuilder.makeStructure({
                        array: false, type: "json", defaultValue: {
                            address: "",
                            apt: "",
                            city: "",
                            floor: "",
                            postal: "",
                            state: "",
                        }
                    }),
                    packages: FieldBuilder.makeStructure({array: false, type: "json", defaultValue: {}}),
                    services: FieldBuilder.makeStructure({array: false, type: "json", defaultValue: {}}),
                    payments: FieldBuilder.makeStructure({array: false, type: "json", defaultValue: {}}),
                    discount: FieldBuilder.makeStructure({array: false, type: "json", defaultValue: {}}),
                    total: FieldBuilder.makeStructure({array: false, type: "decimal", defaultValue: 0}),
                    totalWithDiscount: FieldBuilder.makeStructure({array: false, type: "decimal", defaultValue: 0}),
                    balance: FieldBuilder.makeStructure({array: false, type: "decimal", defaultValue: 0}),
                    notes: FieldBuilder.makeStructure({array: false, type: "json", defaultValue: {}}),
                    savedVariables: FieldBuilder.makeStructure({array: true, type: "string",}),
                    rate: FieldBuilder.makeStructure({array: false, type: "decimal", defaultValue: 0}),
                    laborTime: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    drivingTime: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    travelTime: FieldBuilder.makeStructure({array: false, type: "integer", defaultValue: 0}),
                    reservationPrice: FieldBuilder.makeStructure({array: false, type: "decimal", defaultValue: 0}),
                    fuelPrice: FieldBuilder.makeStructure({array: false, type: "decimal", defaultValue: 0}),
                    valuation:  FieldBuilder.makeStructure({array: false, type: "decimal", defaultValue: 0}),
                    transportationPrice:  FieldBuilder.makeStructure({array: false, type: "decimal", defaultValue: 0}),

                }
            },
            {
                name: 'User',
                fields: {
                    firstName: FieldBuilder.makeStructure({array: false, type: 'string'}),
                    lastName: FieldBuilder.makeStructure({array: false, type: 'string'}),
                    email: FieldBuilder.makeStructure({array: false, type: 'string'}),
                    password: FieldBuilder.makeStructure({array: false, type: 'string'}),
                    phone: FieldBuilder.makeStructure({array: false, type: 'string'}),
                    phoneAdditional: FieldBuilder.makeStructure({array: false, type: 'string'}),
                    companyName: FieldBuilder.makeStructure({array: false, type: 'string'}),
                    companyNameCustom: FieldBuilder.makeStructure({array: false, type: 'string'}),
                    roles: FieldBuilder.makeStructure({array: true, type: 'integer'}),
                    hashLogin: FieldBuilder.makeStructure({array: false, type: 'string'}),
                    status: FieldBuilder.makeStructure({array: false, type: 'integer', defaultValue: 0}),
                    avatar: FieldBuilder.makeStructure({array: false, type: 'string'}),
                    googleEmail: FieldBuilder.makeStructure({array: false, type: 'string'}),
                    googleCalendarStatuses: FieldBuilder.makeStructure({array: false, type: 'json', defaultValue:[]}),

                },
            },
            {
                name: 'Variable',
                fields: {
                    name: FieldBuilder.makeStructure({array: false, type: 'string'}),
                    value: FieldBuilder.makeStructure({array: false, type: 'json'}),
                    version: FieldBuilder.makeStructure({
                        array: false,
                        type: 'integer',
                        defaultValue: 0,
                        index: true
                    }),
                },
            }
        ]
    },
constantineLexel$v1:function () {
        return {
            USER_ROLES: {
                MANAGER:1, CLIENT:2, SALES:3, DRIVER:4, DEVELOPER: 5,
            },
            TAXES_TYPE: {
                PACKAGES:0,
            },
            STATUS_NUMBER: {
                PENDING: 0,
                PAID: 1,
                DELIVERY: 2,
                PICKUP: 3,
                COMPLETE: 4,
                DRAFT: -1,
                SPAM: -2,
                CANCELLED: -3,
                NOT_AVAILABLE: -4,
                DEAD_LEAD: -5,
            },
            STATUS_NAMES: {
                0: 'Pending',
                1: 'Payment',
                2: 'Delivery',
                3: 'Pickup',
                4: 'Complete',
                '-1': 'Draft',
                '-2': 'Spam',
                '-3': 'Cancelled',
                '-4': 'Not available',
                '-5': 'Dead lead',
            },
            WEIGHT_UNIT: 'Crates',
            REFERRERS: [
                'Website',
                'Good friend',
                'Тётка on the street',
                'Taxist',
            ],
        }
    },
constantine$moving_v1:function() {
        return {
            USER_ROLES: {
                MANAGER:1, CLIENT:2, SALES:3, DRIVER:4, DEVELOPER: 5,
            },
            TAXES_TYPE: {
                PACKAGES:0,
            },
            STATUS_NUMBER: {
                PENDING: 0,
                PAID: 1,
                DELIVERY: 2,
                PICKUP: 3,
                COMPLETE: 4,
                DRAFT: -1,
                SPAM: -2,
                CANCELLED: -3,
                NOT_AVAILABLE: -4,
                DEAD_LEAD: -5,
            },
            REQUEST_TYPES: {
                localMove: 0
            },
            STATUS_NAMES: {
                0: 'Pending',
                1: 'Payment',
                2: 'Delivery',
                3: 'Pickup',
                4: 'Complete',
                '-1': 'Draft',
                '-2': 'Spam',
                '-3': 'Cancelled',
                '-4': 'Not available',
                '-5': 'Dead lead',
            },
            WEIGHT_UNIT: 'Crates',
            REFERRERS: [
                'Website',
                'Good friend',
                'Тётка on the street',
                'Taxist',
            ],
        };

    },
getVariables$v1:function(variablesNames) {
        return this.getManyEntities$v1({
            name: 'Variable',
            conditions: {filter:{name:variablesNames}},
        })
            .then((variables) => {
                return variables.rows
            })
    },
saveVariables$v1:function(variables) {
        return Promise.all(variables.map(variable => {
            return this.saveVariable$v1(variable);
        }))
    },
getRequestTaxes$v1:function(request) {
    let statesForSearch = [];
    //TODO use intersection for this
    [_.get(request, 'deliveryAddress.state'), _.get(request, 'pickupAddress.state')].forEach(state => {
        if (state && !_.get(request, `savedTaxes.${state}`)) {
            statesForSearch.push(state);
        }
    });

    if (statesForSearch.length) {
        return this.companyData.entityInstance.Taxes.getMultiply({filter:{state:statesForSearch}})
            .then(res => {
                let states = {};
                res.rows.forEach(row => {
                    if (!states[row.state]) states[row.state] = [];
                    states[row.state].push(row);
                });

                                request.savedTaxes = Object.assign(request.savedTaxes || {}, states);


                return request.savedTaxes;
            });
    }

    return Promise.resolve(request.savedTaxes)
},
getTaxes$v1:function(filter) {
        return this.companyData.entityInstance.Taxes.get({filter})
    },
getAllTaxes$v1:function(data) {
        return this.companyData.entityInstance.Taxes.getMultiply(data)
    },
deleteTaxes$v1:function(id) {
        return this.companyData.entityInstance.Taxes.remove(id)
;    },
getVariablesNamesStoredInRequest$v1:function () {
    return [
      'packagesInfo',
      'servicesInfo',
      'discountSettings',
      'zonesSettings'
    ];
  },

};