module.exports = {
sendToSocket$v1:function ({roomName, event, data}) {
  roomName = `${this.systemData.companyName}_${roomName}`;
  io.to(`${roomName}`).emit(event, data);
},
socketSendToUser$v1:function ({userId, event, data}) {
  let roomName = `user#${userId}`;
  this.doTask('sendToSocket')({roomName, event, data})
},
socketSendToRequest$v1:function ({requestId, event, data}) {
  let roomName = `request#${requestId}`;
  this.doTask('sendToSocket')({roomName, event, data})
},
allowedTasks$v1:function () {
  return [
		'getVariablesAnonimus',
		'autocompleteAddress',
		'geocodeAddress',
		'getAreaForAddress',
		'createUser',
		'calculateRequest',
		'saveVisitToFrontsite',
		'createUserFromFrontSite',
	   'checkPromoCode',
	   'restorePassword',
	   'getLoginSettings',
	   'getTimeZone',
	   'getManyVariables',
	   'getConstants',
	]; 
},
allowedVariables$v1:function () {
  return ['getVariable', 'createRequest'];
 
},
socketSendToParklot$v1:function ({event, data}) {
  let roomName = `parklot`;
  this.doTask('sendToSocket')({roomName, event, data})
},
sendToSocket$moving_v1:function ({roomName, event, data}) {
	roomName = `${this.systemData.companyName}_${roomName}`;
	let dataForSending = {
		data,
		roomName
	};
	io.to(`${roomName}`).emit(event, dataForSending);
},
socketSendToInventory$moving_v1:function ({event, data}) {
  let roomName = `inventorySettings`;
  this.doTask('sendToSocket')({roomName, event, data})
},

};