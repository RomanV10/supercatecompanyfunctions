module.exports = {
cronTasks$v1:function () {return [
		{name:'reminder5Days' ,schedule: '0 */5 * * *', callback:'sendDeliveryReminder5Days'},
   	{name:'pickupReminder5Days' ,schedule: '0 */5 * * *', callback:'sendPickupReminder5days'},
   	{name:'pickupReminder1Day' ,schedule: '0 */3 * * *', callback:'sendPickupReminder1day'},
   	{name:'sendDeliveryReminder1Day' ,schedule: '0 */3 * * *', callback:'sendDeliveryReminder1Day'},
   	{name:'reminderNotPaid' ,schedule: '*/10 * * * *', callback:'reminderNotPaid'},
	{name:'cronTasksSetDeliveryRequestToPickup' ,schedule: '0 */1 * * *', callback:'cronTasksSetDeliveryRequestToPickup'},
	{name:'cronTasksSetPickupRequestToComplete' ,schedule: '0 */1 * * *', callback:'cronTasksSetPickupRequestToComplete'},
	{name:'bookedRemindersDay1' ,schedule: '*/15 */4 * * *', callback:'cronTaskBookedRemindersDay', params:1},
	{name:'bookedRemindersDay3' ,schedule: '*/25 */4 * * *', callback:'cronTaskBookedRemindersDay', params:3},
	{name:'bookedRemindersDay5' ,schedule: '*/35 */4 * * *', callback:'cronTaskBookedRemindersDay', params:5},
	{name:'bookedRemindersDay7' ,schedule: '*/40 */4 * * *', callback:'cronTaskBookedRemindersDay', params:7},
	{name:'deliveryReminderAfter1day' ,schedule: '*/5 */5 * * *', callback:'cronTaskDeliveryRemindersAfterOneDay'},
{name:'sendStatisticEmailForPeriod' ,schedule: '0 5 1 */1 *', callback:'sendStatisticEmailForPeriod', params:'month'},
{name:'sendStatisticEmailForPeriod' ,schedule: '0 5 1 1 */1', callback:'sendStatisticEmailForPeriod', params:'year'},
	// {name:'testSMTPSettings' ,schedule: '*/5 * * * *', callback:'cronTasksTestSmtpSettings'},

	];
	},

};